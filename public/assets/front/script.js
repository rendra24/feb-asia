const burgerButton = document.querySelector('#burger-btn');
const burgerMenu = document.querySelector('#burger-menu');
const burgerClose = document.querySelector('#burger-close');

burgerButton.addEventListener('click', () => {
    burgerMenu.classList.toggle('burger-active');
    document.body.classList.toggle('overflow-hidden')
});

burgerClose.addEventListener('click', () => {
    burgerMenu.classList.toggle('burger-active')
    document.body.classList.toggle('overflow-hidden');
});