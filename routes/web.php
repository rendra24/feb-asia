<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
 Route::get('/admin', function(){
 	return "view('back.login')";
 });
Route::group(['namespace' => 'Front'], function() {

	Route::get('/', 'HomeController@index')->name('home');

    Route::get('/ftd', 'HomeController@ftd')->name('fakultas-ftd');

	Route::get('/kalender', 'KalenderController@index')->name('kalender');
	Route::get('/kalender/events', 'KalenderController@getEvents')->name('kalender-event');
	Route::get('/kalender/events/month', 'KalenderController@eventsMonth')->name('kalender-month');

	Route::get('/prodi/{prodi}', 'HomeController@prodi')->name('prodi');


	Route::get('/pengumuman', 'PengumumanController@index')->name('pengumuman');

	Route::get('/pengumuman/{id}/{slug}', 'PengumumanController@show')->name('pengumuman-detail');

	Route::get('/berita', 'BeritaController@index')->name('berita');
	Route::get('/berita/{id}/{slug}', 'BeritaController@detail')->name('berita-detail');

	Route::get('/kegiatan', 'KegiatanController@index')->name('kegiatan');
	Route::get('/kalender', 'KalenderController@index')->name('kalender');
	Route::get('/perkuliahan', 'PerkuliahanController@index')->name('perkuliahan');
	Route::get('/perkuliahan/search', 'PerkuliahanController@search')->name('perkuliahan-search');

	
	Route::get('/jam-bimbingan', 'JamBimbinganController@index')->name('jam-bimbingan');
	Route::get('/jam-bimbingan/search', 'JamBimbinganController@search')->name('jam-bimbingan-search');

	Route::get('/ujian/{jenis}', 'UjianController@index')->name('ujian');
	Route::get('/ujian/{jenis}/search', 'UjianController@search')->name('ujian-search');

	Route::group(['prefix' => 'tautan'], function() {
		Route::get('lpmi', 'LinkController@lpmi')->name('lpmi');
		Route::get('hackathon', 'LinkController@hackathon')->name('hack');
		Route::get('mikrotik', 'LinkController@mikrotik')->name('mikrotik');
		//Route::get('alumni', 'LinkController@alumni')->name('alumni');
		Route::group(['prefix' => 'alumni'], function() {
			Route::get('home', 'AlumniController@index');
            Route::get('struktur', 'AlumniController@strukturorganisasi');
            Route::get('/', 'AlumniController@index');
            Route::get('tokoh', 'AlumniController@tokoh');
            Route::get('galeri', 'AlumniController@galeri');
            Route::get('sumbangan', 'AlumniController@sumbangan');
            Route::get('pustaka', 'AlumniController@pustaka');

		});
	});

	Route::get('/visi-misi', 'VisiMisiController@index')->name('visi-misi');
	Route::get('/sertifikasi', 'SertifikasiController@index')->name('sertifikasi-front');


	Route::get('/profil-dosen', 'ProfilDosenController@index')->name('profil-dosen');

	Route::get('/unduhan', 'UnduhanController@index')->name('unduhan');
	Route::get('/unduhan/{id}/download', 'UnduhanController@download')->name('unduhan-download');

	Route::get('/login-admin', 'UserController@login')->name('login');
	Route::post('/login-admin', 'UserController@doLogin')->name('doLogin');


});

 Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'auth'], function() {

 	Route::get('/', 'AdminController@index')->name('admin-index');
 	Route::get('/password', 'AdminController@password')->name('admin-password');
 	Route::post('/password', 'AdminController@passwordUpdate')->name('admin-password-update');
 	Route::get('/list', 'AdminController@listAdmin')->name('admin-list');
 	Route::post('/save', 'AdminController@listAdminSave')->name('admin-save');
 	Route::post('/update', 'AdminController@listAdminUpdate')->name('admin-update');
 	Route::post('/delete', 'AdminController@listAdminDelete')->name('admin-delete');

 	Route::get('/logout', 'AdminController@logout')->name('logout');

 	Route::group(['middleware' => 'role:berita'], function(){
 		Route::get('/berita', 'BeritaController@index')->name('berita-index');
	 	Route::get('/berita/add', 'BeritaController@add')->name('berita-add');
	 	Route::post('/berita/add', 'BeritaController@save')->name('berita-save');
	 	Route::post('/berita/publish', 'BeritaController@publish')->name('berita-publish');
	 	Route::get('/berita/edit/{id}', 'BeritaController@edit')->name('berita-edit');
	 	Route::post('/berita/edit/{id}', 'BeritaController@update')->name('berita-update');
	 	Route::post('/berita/delete', 'BeritaController@delete')->name('berita-delete');
 	});
	 Route::group(['middleware' => 'role:prestasi'], function(){
		Route::get('/prestasi', 'PrestasiController@index')->name('prestasi-index');
		Route::get('/prestasi/add', 'PrestasiController@add')->name('prestasi-add');
		Route::post('/prestasi/add', 'PrestasiController@save')->name('prestasi-save');
		Route::post('/prestasi/publish', 'PrestasiController@publish')->name('prestasi-publish');
		Route::get('/prestasi/edit/{id}', 'PrestasiController@edit')->name('prestasi-edit');
		Route::post('/prestasi/edit/{id}', 'PrestasiController@update')->name('prestasi-update');
		Route::post('/prestasi/delete', 'PrestasiController@delete')->name('prestasi-delete');
	});
	Route::group(['middleware' => 'role:prodi'], function(){
		Route::get('/prodi', 'ProdiController@index')->name('prodi-index');
		Route::get('/prodi/add', 'ProdiController@add')->name('prodi-add');
		Route::post('/prodi/add', 'ProdiController@save')->name('prodi-save');
		Route::post('/prodi/publish', 'ProdiController@publish')->name('prodi-publish');
		Route::get('/prodi/edit/{id}', 'ProdiController@edit')->name('prodi-edit');
		Route::post('/prodi/edit/{id}', 'ProdiController@update')->name('prodi-update');
		Route::post('/prodi/delete', 'ProdiController@delete')->name('prodi-delete');
	});
 	Route::group(['middleware' => 'role:profil-dosen'], function(){
 		Route::get('/profil-dosen', 'DosenController@index')->name('profil-dosen-index');
	 	Route::post('/profil-dosen/save', 'DosenController@save')->name('profil-dosen-save');
	 	Route::post('/profil-dosen/update', 'DosenController@update')->name('profil-dosen-update');
	 	Route::post('/profil-dosen/delete', 'DosenController@delete')->name('profil-dosen-delete');
 	});
	 	
	Route::group(['middleware' => 'role:perkuliahan'], function(){
	 	Route::get('/perkuliahan', 'PerkuliahanController@index')->name('perkuliahan-index');
	 	Route::post('/perkuliahan', 'PerkuliahanController@save')->name('perkuliahan-save');
	 	Route::post('/perkuliahan/update', 'PerkuliahanController@update')->name('perkuliahan-update');
	 	Route::post('/perkuliahan/delete', 'PerkuliahanController@delete')->name('perkuliahan-delete');
		
	 	Route::post('perkuliahan/detail/delete', 'PerkuliahanDetailController@delete')->name('perkuliahan-detail-delete');
	 	Route::get('perkuliahan/detail/{id}', 'PerkuliahanDetailController@index')->name('perkuliahan-detail-index');
	 	Route::post('perkuliahan/detail/{id}/save', 'PerkuliahanDetailController@save')->name('perkuliahan-detail-save');
	 	Route::post('perkuliahan/detail/{id}/update', 'PerkuliahanDetailController@update')->name('perkuliahan-detail-update');
	 	Route::get('/perkuliahan/download', 'PerkuliahanController@download')->name('perkuliahan-download');
	 });

 	Route::group(['middleware' => 'role:ujian'], function(){
	 	Route::post('ujian/detail/delete', 'UjianDetailController@delete')->name('ujian-detail-delete');
	 	Route::post('ujian/detail/{id}/save', 'UjianDetailController@save')->name('ujian-detail-save');
	 	Route::post('ujian/detail/{id}/update', 'UjianDetailController@update')->name('ujian-detail-update');
	 	Route::get('ujian/detail/{id}/{jenis}', 'UjianDetailController@index')->name('ujian-detail-index');

	 	Route::post('/ujian/update', 'UjianController@update')->name('ujian-update');
	 	Route::get('/ujian/download', 'UjianController@download')->name('ujian-download');
	 	Route::post('/ujian/delete', 'UjianController@delete')->name('ujian-delete');
	 	Route::get('/ujian/{jenis}', 'UjianController@index')->name('ujian-index');
	 	Route::post('/ujian/{jenis}', 'UjianController@save')->name('ujian-save');
	 });


 	Route::get('/visi-misi/{prodi}', 'VisiMisiController@index')->name('visi-misi-index');
 	Route::get('/visi-misi/{prodi}/edit', 'VisiMisiController@edit')->name('visi-misi-edit');
 	Route::post('/visi-misi/{prodi}/edit', 'VisiMisiController@update')->name('visi-misi-update');

	Route::get('/sertifikasi', 'SertifikasiController@index')->name('sertifikasi');
	Route::get('/sertifikasi-edit/{id}', 'SertifikasiController@edit')->name('sertifikasi-edit');
	Route::post('/sertifikasi-edit', 'SertifikasiController@update')->name('sertifikasi-update');

	 
 	Route::get('/profil/{prodi}', 'ProfilController@index')->name('profil-index');
 	Route::get('/profil/{prodi}/edit', 'ProfilController@edit')->name('profil-edit');
 	Route::post('/profil/{prodi}/edit', 'ProfilController@update')->name('profil-update');

 	Route::group(['middleware' => 'role:link'], function(){
	 	Route::get('/link', 'LinkController@index')->name('link-index');
	 	Route::post('/link', 'LinkController@save')->name('link-save');
	 	Route::post('/link/update', 'LinkController@update')->name('link-update');
	 	Route::post('/link/delete', 'LinkController@delete')->name('link-delete');
	 });
 	Route::group(['middleware' => 'role:unduhan'], function(){
	 	Route::get('/unduhan', 'UnduhanController@index')->name('unduhan-index');
	 	Route::post('/unduhan', 'UnduhanController@save')->name('unduhan-save');
	 	Route::post('/unduhan/update', 'UnduhanController@update')->name('unduhan-update');
	 	Route::post('/unduhan/delete', 'UnduhanController@delete')->name('unduhan-delete');
	 });
 	Route::group(['middleware' => 'role:pengumuman'], function(){
	 	Route::get('/pengumuman', 'PengumumanController@index')->name('pengumuman-index');
	 	Route::get('/pengumuman/add', 'PengumumanController@add')->name('pengumuman-add');
	 	Route::post('/pengumuman/add', 'PengumumanController@save')->name('pengumuman-save');
	 	Route::get('/pengumuman/{id}/edit', 'PengumumanController@edit')->name('pengumuman-edit');
	 	Route::post('/pengumuman/{id}/edit', 'PengumumanController@update')->name('pengumuman-update');
	 	Route::post('/pengumuman/delete', 'PengumumanController@delete')->name('pengumuman-delete');
	 });
 	Route::group(['middleware' => 'role:kalender'], function(){
	 	Route::get('/kalender', 'KalenderController@index')->name('kalender-index');
	 	Route::post('/kalender', 'KalenderController@save')->name('kalender-save');
	 	Route::post('/kalender/update', 'KalenderController@update')->name('kalender-update');
	 	Route::post('/kalender/delete', 'KalenderController@delete')->name('kalender-delete');
	 });
 	Route::group(['middleware' => 'role:jam-bimbingan'], function(){
	 	Route::get('/jam-bimbingan', 'JamBimbinganController@index')->name('jam-bimbingan-index');
	 	Route::post('/jam-bimbingan', 'JamBimbinganController@save')->name('jam-bimbingan-save');
	 	Route::post('/jam-bimbingan/update', 'JamBimbinganController@update')->name('jam-bimbingan-update');
	 	Route::post('/jam-bimbingan/delete', 'JamBimbinganController@delete')->name('jam-bimbingan-delete');
		
	 	Route::post('jam-bimbingan/detail/delete', 'JamBimbinganDetailController@delete')->name('jam-bimbingan-detail-delete');
	 	Route::get('jam-bimbingan/detail/{id}', 'JamBimbinganDetailController@index')->name('jam-bimbingan-detail-index');
	 	Route::post('jam-bimbingan/detail/{id}/save', 'JamBimbinganDetailController@save')->name('jam-bimbingan-detail-save');
	 	Route::post('jam-bimbingan/detail/{id}/update', 'JamBimbinganDetailController@update')->name('jam-bimbingan-detail-update');
	 	Route::get('/jam-bimbingan/download', 'JamBimbinganController@download')->name('jam-bimbingan-download');
	 });

 });