<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUjianTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ujian', function (Blueprint $table) {
            $table->increments('id');
            $table->string('semester');
            $table->string('tahun_ajaran');
            $table->integer('jenis_ujian_id')->unsigned();
            $table->timestamps();

        });
        Schema::table('ujian', function (Blueprint $table) {
            $table->foreign("jenis_ujian_id")->references("id")->on("jenis_ujian");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
