<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerkuliahanDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perkuliahan_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->string('hari');
            $table->string('jam');
            $table->string('ruangan');
            $table->string('kelas');
            $table->string('mata_kuliah');
            $table->string('dosen_pengampu');
            $table->integer('perkuliahan_id')->unsigned();
            $table->timestamps();

        });
        Schema::table('perkuliahan_detail', function (Blueprint $table) {
            $table->foreign("perkuliahan_id")->references("id")->on("perkuliahan");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perkuliahan_detail');
    }
}
