<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUjianDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ujian_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->string('hari');
            $table->string('tanggal');
            $table->string('jam');
            $table->string('ruang');
            $table->string('nim');
            $table->string('nama_mahasiswa');
            $table->string('judul');
            $table->string('prodi');
            $table->integer('ujian_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('ujian_detail', function (Blueprint $table) {
            $table->foreign("ujian_id")->references("id")->on("ujian");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ujian_detail');
    }
}
