<?php

use App\JenisUjian;
use Illuminate\Database\Seeder;

class JenisUjianSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    		JenisUjian::create(['name' => 'pkl', 'description' => 'Praktek Kerja Lapangan']);
    		JenisUjian::create(['name' => 'ta', 'description' => 'Tugas Akhir']);
    }
}
