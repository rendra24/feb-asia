<?php

use App\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    		Role::create(['name' => 'berita', 'description' => 'Berita']);
    		Role::create(['name' => 'kalender', 'description' => 'Kalender Akademik']);
    		Role::create(['name' => 'perkuliahan', 'description' => 'Jadwal Perkuliahan']);
    		Role::create(['name' => 'Ujian', 'description' => 'Jadwal Ujian']);
    		Role::create(['name' => 'profil-stmik', 'description' => 'Profil STMIK ASIA']);
    		Role::create(['name' => 'profil-ti', 'description' => 'Profil Teknik Informatika']);
    		Role::create(['name' => 'profil-sk', 'description' => 'Profil Sistem Komputer']);
    		Role::create(['name' => 'profil-dkv', 'description' => 'Profil Desain Komunikasi Visual']);
    		Role::create(['name' => 'visi-misi-stmik', 'description' => 'Visi Misi STMIK ASIA']);
    		Role::create(['name' => 'visi-misi-ti', 'description' => 'Visi Misi Teknik Informatika']);
    		Role::create(['name' => 'visi-misi-sk', 'description' => 'Visi Misi Sistem Komputer']);
    		Role::create(['name' => 'visi-misi-dkv', 'description' => 'Visi Misi Desain Komunikasi Visual']);
    		Role::create(['name' => 'link', 'description' => 'Link Website']);
    		Role::create(['name' => 'profil-dosen', 'description' => 'Profil Dosen']);
    		Role::create(['name' => 'pengumuman', 'description' => 'Pengumuman']);
    		Role::create(['name' => 'unduhan', 'description' => 'unduhan']);
    }
}
