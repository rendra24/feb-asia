<?php

use App\Prodi;
use Illuminate\Database\Seeder;

class ProdiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		Prodi::create(['name' => 'stmik', 'description' => 'STMIK ASIA']);
		Prodi::create(['name' => 'ti', 'description' => 'Teknik Informatika']);
		Prodi::create(['name' => 'sk', 'description' => 'Sistem Komputer']);
		Prodi::create(['name' => 'dkv', 'description' => 'Desain Komunikasi Visual']);
    }
}
