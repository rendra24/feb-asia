<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ProgramStudi extends Model
{
    protected $table = "program_studi";
    protected $fillable = ['title', 'slug', 'content', 'user_id', 'status', 'image', 'publish'];
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function getPublishingAttribute()
    {
        return Carbon::parse($this->publish)->formatLocalized('%A, %d %B %Y');
    }
    public function getDateAttribute()
    {
        return Carbon::parse($this->publish)->formatLocalized('%A, %d %B %Y');
    } 
    public function getCreatedAttribute()
    {
        return Carbon::parse($this->created_at)->formatLocalized('%A, %d %B %Y');
    }
    public function getBodyAttribute()
    {
        $data = strip_tags(html_entity_decode($this->content));
        return str_limit($data, 200);
    }
}