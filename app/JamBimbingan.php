<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JamBimbingan extends Model
{
	protected $table = 'jam_bimbingan';

	protected $fillable = ['tahun_ajaran', 'semester'];

	public function details()
	{
		return $this->hasMany('App\JamBimbinganDetail');
	}

}
