<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VisiMisi extends Model
{
	protected $table = "visi_misi";

	protected $fillable = ['content', 'prodi_id'];

	public function prodi()
	{
		return $this->hasOne('App\Prodi');
	}
}
