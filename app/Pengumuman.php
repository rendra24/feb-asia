<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Pengumuman extends Model
{
    protected $table = "pengumuman";
    protected $fillable = ['title', 'slug', 'content','user_id'];
         public function user()
        {
            return $this->belongsTo('App\User');
        }
        public function getCreatedAttribute()
        {
            return Carbon::parse($this->created_at)->formatLocalized('%A, %d %B %Y');
        }
        public function getBodyAttribute()
        {
            $data = strip_tags(html_entity_decode($this->content));
            return str_limit($data, 250);
        }
        public function getIsiAttribute()
        {
            $data = strip_tags(html_entity_decode($this->content));
            return str_limit($data, 240);
        }
}
