<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JamBimbinganDetail extends Model
{
	protected $table = 'jam_bimbingan_detail';
	protected $fillable = ['nama', 'hari', 'jam', 'jam_bimbingan_id'];
	
	public function jamBimbingan()
    	{
    		return $this->belongsTo('App\JamBimbingan');
    	}
}
