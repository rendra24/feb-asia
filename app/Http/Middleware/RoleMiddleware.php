<?php

namespace App\Http\Middleware;

use Closure;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $roleName)
    {
            if (auth()->check()){
                    if(auth()->user()->super){
                        return $next($request);
                    }
                    if(auth()->user()->hasRole($roleName)){
                        return $next($request);
                    }
                    return redirect()->route('admin-index');
            } 
            
            return redirect()->route('home');
    }
}
