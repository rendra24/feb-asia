<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\JenisUjian;
use App\Ujian;
use App\UjianDetail;
use Excel;
use Illuminate\Http\Request;

class UjianController extends Controller
{
	public function index($jenis)
	{

		$jenis = JenisUjian::where('name', '=', $jenis)->first();
		$data['jenis'] = $jenis;
		$data['ujian'] = Ujian::where('jenis_ujian_id','=', $jenis->id)->orderBy('id', 'desc')->paginate(10);
		return view('back.ujian.index', $data);
	}

	public function save($jenis, Request $request)
	{
		$this->validate($request, [
			'semester' => 'required',
			'tahun_ajaran' => 'required',
		],$this->messages());
		$id_jenis = JenisUjian::where('name', '=', $jenis)->first();

		$data['semester'] =  $request->input('semester');
		$data['tahun_ajaran'] = $request->input('tahun_ajaran');
		$data['jenis_ujian_id'] = $id_jenis->id;
		$ujian = Ujian::create($data);
		if($request->hasFile('file-csv')){
			$path = $request->file('file-csv')->getRealPath();
			$this->importExcel($ujian->id,$path);
		}

		return response()->json(['message'=>'success'], 201);
	}

	public function update(Request $request)
	{
		$this->validate($request, [
			'semester' => 'required',
			'tahun_ajaran' => 'required',
		],$this->messages());
		$id = $request->input('id');
		$data['semester'] =  $request->input('semester');
		$data['tahun_ajaran'] = $request->input('tahun_ajaran');
		Ujian::find($id)->update($data);
		if($request->hasFile('file-csv')){
			$path = $request->file('file-csv')->getRealPath();
			$this->importExcel($id,$path);
		}
		return response()->json(['message'=>'success'], 200);
	}

	public function importExcel($id, $path)
	{
		$data = Excel::load($path, function($reader) {})->get();
		if(!empty($data) && $data->count()){
			foreach ($data->toArray() as $key => $value) {

				if(strlen(str_replace(" ", "", $value['hari']))>0 && strlen(str_replace(" ", "", $value['jam']))>0 && strlen(str_replace(" ", "", $value['ruang']))>0 && strlen(str_replace(" ", "", $value['tanggal']))>0 && strlen(str_replace(" ", "", $value['nim']))>0 && strlen(str_replace(" ", "", $value['nama_mahasiswa']))>0 && strlen(str_replace(" ", "", $value['judul']))>0 ){	

					$insert['hari']= $value['hari'];
					$insert['tanggal'] =str_replace("'", "", $value['tanggal']);
					$insert['jam'] =$value['jam'];
					$insert['ruang']= $value['ruang'];
					$insert['nim']= $value['nim'];
					$insert['nama_mahasiswa'] =$value['nama_mahasiswa'];
					$insert['judul']= $value['judul'];
					$insert['ujian_id'] =$id;
					if(!empty($insert)){
						UjianDetail::create($insert);
					}
				}
			}
		}
	}

	public function delete(Request $request)
	{
		$id = $request->input("id");
		UjianDetail::where("ujian_id","=", $id)->delete();
		Ujian::find($id)->delete();
		return response()->json(['message'=>'success'], 200);
	}

	public function messages()
	{
	    return [
	        'semester.required' => 'Semester Harus Di Isi',
	        'tahun_ajaran.required' => 'Tahun Ajaran Harus Di Isi',
	    ];
	}
	public function download()
	{
		$myFile = public_path("/assets/static/Excel/Ujian.xlsx");
	    	$headers = ['Content-Type: application/xlsx'];
	    	$newName = 'Format-Ujian.xlsx';

	    	return response()->download($myFile, $newName, $headers);
	}
}
