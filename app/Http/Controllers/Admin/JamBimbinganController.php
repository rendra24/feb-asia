<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\JamBimbingan;
use App\JamBimbinganDetail;
use Excel;
use Illuminate\Http\Request;

class JamBimbinganController extends Controller
{
	public function index()
	{
		$data['jamBimbingan'] = JamBimbingan::orderBy("id","desc")->paginate(10); 
		return view('back.jamBimbingan.index', $data);
	}

	public function save(Request $request)
	{
		$data =  $request->only('semester', 'tahun_ajaran');
		$JamBimbingan = JamBimbingan::create($data);
		if($request->hasFile('file-csv')){
			$path = $request->file('file-csv')->getRealPath();
			$this->importExcel($JamBimbingan->id,$path);
		}

		return response()->json(['message'=>'success'], 201);
	}

	public function update(Request $request)
	{
		$id = $request->input('id');
		$data =  $request->only('semester', 'tahun_ajaran');
		JamBimbingan::find($id)->update($data);
		if($request->hasFile('file-csv')){
			$path = $request->file('file-csv')->getRealPath();
			$this->importExcel($id,$path);
		}
		return response()->json(['message'=>'success'], 200);
	}

	public function importExcel($id, $path)
	{
		$data = Excel::load($path, function($reader) {})->get();
		if(!empty($data) && $data->count()){
			JamBimbinganDetail::where('jam_bimbingan_id','=',$id)->delete();

			foreach ($data->toArray() as $key => $value) {
				if(strlen(str_replace(" ", "", $value['hari']))>0 && strlen(str_replace(" ", "", $value['jam']))>0 && strlen(str_replace(" ", "", $value['nama_dosen']))>0){		
					$insert['hari']= $value['hari'];
					$insert['jam'] =$value['jam'];
					$insert['nama']= $value['nama_dosen'];
					$insert['jam_bimbingan_id'] =$id;
					if(!empty($insert)){
						JamBimbinganDetail::create($insert);
					}
				}
			}

		}
	}

	public function delete(Request $request)
	{
		$id = $request->input("id");
		JamBimbinganDetail::where("jam_bimbingan_id","=", $id)->delete();
		JamBimbingan::find($id)->delete();
		return response()->json(['message'=>'success'], 200);
	}

	public function download()
	{
		$myFile = public_path("/assets/static/Excel/JamBimbingan.xlsx");
	    	$headers = ['Content-Type: application/xlsx'];
	    	$newName = 'Format-Jam-Bimbingan.xlsx';

	    	return response()->download($myFile, $newName, $headers);
	}
}
