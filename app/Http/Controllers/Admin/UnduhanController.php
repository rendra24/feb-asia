<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Unduhan;
use Illuminate\Http\Request;

class UnduhanController extends Controller
{
	public function index()
	{
		$data['unduhan'] = Unduhan::orderBy('id', 'desc')->paginate(10);
		return view('back.unduhan.index', $data);
	}
	public function save(Request $request)
	{
		$this->validate($request, [
			'title' => 'required',
			'file' => 'required',
		],$this->messages());
		$data['title'] =  $request->input('title');
		if($request->hasFile('file')){
			$data['file'] = $this->upload($request);
		}
		$Unduhan = Unduhan::create($data);

		return response()->json(['message'=>'success'], 201);
	}
	public function update(Request $request)
	{
		$this->validate($request, [
			'title' => 'required',
			'file' => 'required',
		],$this->messages());
		$data['title'] =  $request->input('title');
		if($request->hasFile('file')){
			$data['file'] = $this->upload($request);
		}
		$id = $request->input('id');
		Unduhan::find($id)->update($data);
		return $data;
		return response()->json(['message'=>'success'], 200);
	}
	public function upload($request)
	{
		$uniqueFileName = uniqid(). '.' . $request->file('file')->getClientOriginalExtension();

        	$request->file('file')->move(public_path('assets/unduhan/') , $uniqueFileName);
        	return $uniqueFileName;
	}

	public function delete(Request $request)
	{
		$id = $request->input("id");
		Unduhan::find($id)->delete();
		return response()->json(['message'=>'success'], 200);
	}
	public function messages()
	{
	    return [
	        'title.required' => 'Title Unduhan Masih Kosong, Harus Di Isi',
	        'file.required' => 'file Unduhan Masih Kosong, Harus Di Isi',
	    ];
	}
}
