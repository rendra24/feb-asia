<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Perkuliahan;
use App\PerkuliahanDetail;
use Excel;
use Illuminate\Http\Request;

class PerkuliahanController extends Controller
{
	public function index()
	{
		$data['perkuliahan'] = Perkuliahan::orderBy("id","desc")->paginate(10); 
		return view('back.perkuliahan.index', $data);
	}

	public function save(Request $request)
	{
		$this->validate($request, [
			'semester' => 'required',
			'tahun_ajaran' => 'required',
		],$this->messages());
		$data['semester'] =  $request->input('semester');
		$data['tahun_ajaran'] = $request->input('tahun_ajaran');
		$perkuliahan = Perkuliahan::create($data);
		if($request->hasFile('file-csv')){
			$path = $request->file('file-csv')->getRealPath();
			$this->importExcel($perkuliahan->id,$path);
		}

		return response()->json(['message'=>'success'], 201);
	}

	public function update(Request $request)
	{
		$this->validate($request, [
			'semester' => 'required',
			'tahun_ajaran' => 'required',
		],$this->messages());
		$id = $request->input('id');
		$data['semester'] =  $request->input('semester');
		$data['tahun_ajaran'] = $request->input('tahun_ajaran');
		Perkuliahan::find($id)->update($data);
		if($request->hasFile('file-csv')){
			$path = $request->file('file-csv')->getRealPath();
		// 	$data = Excel::load($path, function($reader) {})->get();
		// return $data;
			$this->importExcel($id,$path);
		}
		return response()->json(['message'=>'success'], 200);
	}

	public function importExcel($id, $path)
	{
		$data = Excel::load($path, function($reader) {})->get();
		if(!empty($data) && $data->count()){
			PerkuliahanDetail::where('perkuliahan_id','=',$id)->delete();

			foreach ($data->toArray() as $key => $value) {
				if(strlen(str_replace(" ", "", $value['hari']))>0 && strlen(str_replace(" ", "", $value['jam']))>0 && strlen(str_replace(" ", "", $value['ruangan']))>0 && strlen(str_replace(" ", "", $value['kelas']))>0 && strlen(str_replace(" ", "", $value['mata_kuliah']))>0 && strlen(str_replace(" ", "", $value['dosen_pengampu']))>0){		
					$insert['hari']= $value['hari'];
					$insert['jam'] =$value['jam'];
					$insert['ruangan']= $value['ruangan'];
					$insert['kelas'] =$value['kelas'];
					$insert['mata_kuliah']= $value['mata_kuliah'];
					$insert['dosen_pengampu'] =$value['dosen_pengampu'];
					$insert['perkuliahan_id'] =$id;
					if(!empty($insert)){
						PerkuliahanDetail::create($insert);
					}
				}
			}

		}
	}

	public function delete(Request $request)
	{
		$id = $request->input("id");
		PerkuliahanDetail::where("perkuliahan_id","=", $id)->delete();
		Perkuliahan::find($id)->delete();
		return response()->json(['message'=>'success'], 200);
	}

	public function messages()
	{
	    return [
	        'semester.required' => 'Semester Harus Di Isi',
	        'tahun_ajaran.required' => 'Tahun Ajaran Harus Di Isi',
	    ];
	}
	public function download()
	{
		$myFile = public_path("/assets/static/Excel/Perkuliahan.xlsx");
	    	$headers = ['Content-Type: application/xlsx'];
	    	$newName = 'Format-Perkuliahan.xlsx';

	    	return response()->download($myFile, $newName, $headers);
	}
}
