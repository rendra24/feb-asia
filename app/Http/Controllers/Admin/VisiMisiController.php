<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Prodi;
use Illuminate\Http\Request;

class VisiMisiController extends Controller
{
	public function akses($prodi)
	{
		if($prodi=='stmik'){
			$this->middleware('role:visi-misi-stmik');
		}else if($prodi=='ti'){
			$this->middleware('role:visi-misi-ti');
		}else if($prodi=='sk'){
			$this->middleware('role:visi-misi-sk');
		}else {
			$this->middleware('role:visi-misi-dkv');
		}
	}
	public function index($prodi)
	{
		$this->akses($prodi);
		$data['data'] = Prodi::where('name','=', $prodi)->with('Visimisi')->first();
		return view('back.visimisi.index', $data);
	}
	public function edit($prodi)
	{
		$this->akses($prodi);
		$data['data'] = Prodi::where('name','=', $prodi)->with('Visimisi')->first();
		return view('back.visimisi.edit', $data);
	}
	public function update($prodi, Request $request)
	{
		$this->akses($prodi);
		$data['content'] = $request->content;
		$prodi = Prodi::where('name','=', $prodi)->first()->Visimisi->update($data);
		return response()->json(['message' => 'Success'], 200);
	}
}
