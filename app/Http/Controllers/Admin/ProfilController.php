<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Prodi;
use Illuminate\Http\Request;

class ProfilController extends Controller
{
	public function akses($prodi)
	{
		if($prodi=='stmik'){
			$this->middleware('role:profil-stmik');
		}else if($prodi=='ti'){
			$this->middleware('role:profil-ti');
		}else if($prodi=='sk'){
			$this->middleware('role:profil-ti');
		}else {
			$this->middleware('role:profil-dkv');
		}
	}

	public function index($prodi)
	{
		$this->akses($prodi);
		$data['data'] = Prodi::where('name','=', $prodi)->with('Profil')->first();

		return view('back.profil.index', $data);
	}
	public function edit($prodi)
	{
		$this->akses($prodi);
		$data['data'] =Prodi::where('name','=', $prodi)->with('Profil')->first();
		// return $data;
		return view('back.profil.edit', $data);
	}
	public function update($prodi, Request $request)
	{
		$this->akses($prodi);
		$data['content'] = $request->content;
		$prodi = Prodi::where('name','=', $prodi)->first()->Profil()->update($data);
		return response()->json(['message' => 'Success'], 200);
	}
}
