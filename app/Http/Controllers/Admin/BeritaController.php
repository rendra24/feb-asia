<?php

namespace App\Http\Controllers\Admin;

use App\Berita;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;

use Auth;

class BeritaController extends Controller
{
	public function index()
	{
    		$data['berita'] = Berita::orderBy('id', 'desc')->with('user')->paginate(10);
		return view('back.berita.index', $data);
	}
	public function add()
	{
		$data['status'] = 'add';

		return view('back.berita.add', $data);
	}
	public function save(Request $request)
	{
		$this->validate($request, [
		    'title' => 'required',
		    'image' => 'required',
		    'content' => 'required',
		],$this->messages());
		$data['title'] = $request->input('title');
		$data['slug'] = str_slug($data['title']);
		$data['image'] = $request->input('image');
		$data['content'] = $request->input('content');
		$data['status'] = $request->input('status');
		$data['user_id'] = Auth::user()->id;
		Berita::create($data);
		return response()->json(['message'=>"success"], 201);
	}

	public function edit($id)
	{
		$data['berita']= Berita::find($id);
		return view('back.berita.edit', $data);
	}
	public function update($id, Request $request)
	{
		$this->validate($request, [
		    'title' => 'required',
		    'content' => 'required',
		],$this->messages());
		$data['title'] = $request->input('title');
		$data['slug'] = str_slug($data['title']);
		if($request->input('image')!=null&&$request->input('image')!=''){
			$data['image'] = $request->input('image');
		}
		$data['content'] = $request->input('content');
		$data['status'] = $request->input('status');
		$data['user_id'] = Auth::user()->id;
		$berita = Berita::find($id);
		if($berita->status==0&& $data['status']==1){
			$data['publish'] = Carbon::now();
		}
		$berita->update($data);
		return response()->json(['message'=>"success"], 201);
	}


	public function publish(Request $request)
	{
		$id =$request->input('id');
		Berita::find($id)->update(['status'=>1, 'publish'=> Carbon::now()]);
		return response()->json(['message'=>'success'], 200);
	}
	public function delete(Request $request)
	{
		$id =$request->input('id');
		Berita::find($id)->delete();
		return response()->json(['message'=>'success'], 200);
	}

	public function messages()
	{
	    return [
	        'title.required' => 'Title Harus Di Isi',
	        'image.required' => 'Image Harus Di Isi',
	        'content.required'  => 'Content  Harus Di Isi',
	    ];
	}
}
