<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Prodi;
use App\Sertifikasi;
use Illuminate\Http\Request;

class SertifikasiController extends Controller
{
	
	public function index()
	{
		$data['data'] = Sertifikasi::first();
		return view('back.sertifikasi.index', $data);
	}
	public function edit()
	{
		$data['data'] = Sertifikasi::first();
		return view('back.sertifikasi.edit', $data);
	}
	public function update(Request $request)
	{
		
		$data['content'] = $request->content;
		$prodi = Sertifikasi::first()->update($data);
		return response()->json(['message' => 'Success'], 200);
	}
}