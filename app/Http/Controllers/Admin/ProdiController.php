<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Carbon\Carbon;
use App\ProgramStudi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProdiController extends Controller
{
	public function index()
	{
    	$data['prodi'] = ProgramStudi::orderBy('id', 'desc')->with('user')->paginate(10);
		return view('back.prodi.index', $data);
	}
	public function add()
	{
		$data['status'] = 'add';

		return view('back.prodi.add', $data);
	}
	public function save(Request $request)
	{
		$this->validate($request, [
		    'title' => 'required',
		    'image' => 'required',
		    'content' => 'required',
		],$this->messages());
		$data['title'] = $request->input('title');
		$data['slug'] = str_slug($data['title']);
		$data['image'] = $request->input('image');
		$data['content'] = $request->input('content');
		$data['status'] = $request->input('status');
		$data['user_id'] = Auth::user()->id;
		ProgramStudi::create($data);
		return response()->json(['message'=>"success"], 201);
	}

	public function edit($id)
	{
		$data['berita']= ProgramStudi::find($id);
		return view('back.prodi.edit', $data);
	}
	public function update($id, Request $request)
	{
		$this->validate($request, [
		    'title' => 'required',
		    'content' => 'required',
		],$this->messages());
		$data['title'] = $request->input('title');
		$data['slug'] = str_slug($data['title']);
		if($request->input('image')!=null&&$request->input('image')!=''){
			$data['image'] = $request->input('image');
		}
		$data['content'] = $request->input('content');
		$data['status'] = $request->input('status');
		$data['user_id'] = Auth::user()->id;
		$berita = ProgramStudi::find($id);
		if($berita->status==0&& $data['status']==1){
			$data['publish'] = Carbon::now();
		}
		$berita->update($data);
		return response()->json(['message'=>"success"], 201);
	}


	public function publish(Request $request)
	{
		$id =$request->input('id');
		ProgramStudi::find($id)->update(['status'=>1, 'publish'=> Carbon::now()]);
		return response()->json(['message'=>'success'], 200);
	}
	public function delete(Request $request)
	{
		$id =$request->input('id');
		ProgramStudi::find($id)->delete();
		return response()->json(['message'=>'success'], 200);
	}

	public function messages()
	{
	    return [
	        'title.required' => 'Title Harus Di Isi',
	        'image.required' => 'Image Harus Di Isi',
	        'content.required'  => 'Content  Harus Di Isi',
	    ];
	}
}