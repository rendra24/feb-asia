<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use Auth;
use Illuminate\Http\Request;

class AdminController extends Controller
{
	public function index()
	{
		return view('back.dashboard');
	}
	 public function logout()
    	{
    		if(Auth::user()){
    			Auth::logout();
    		}
		return redirect('/');
    	}

    	public function listAdmin()
    	{
    		$data['roles'] = Role::all();
    		$data['listAdmin'] = User::where('super', '=', 0)->with('roles')->paginate(10);

    		return view('back.admin.list', $data);
    	}
    	public function listAdminSave(Request $request)
    	{
    		$data['name'] = $request->input('name');
    		$data['email'] = $request->input('email');
    		$data['password'] = bcrypt($request->input('password'));
    		$admin = User::create($data);
    		foreach (Role::all() as $key => $role) {
    			if($request->input($role->name)){
    				$admin->addRole($role->name);
    			}
    		}
    		return response()->json(['message'=>'success'], 200);
    	}
    	public function listAdminUpdate(Request $request)
    	{
    		$id = $request->input('id');
    		$data['name'] = $request->input('name');
    		$data['email'] = $request->input('email');
    		if($request->input('password')!=null && $request->input('password')!=''){
    			$data['password'] = bcrypt($request->input('password'));
    		}
    		$admin = User::find($id);
    		$admin->update($data);
    		$admin->removeAllRole();
    		foreach (Role::all() as $key => $role) {
    			if($request->input($role->name)){
    				$admin->addRole($role->name);
    			}
    		}
    		return response()->json(['message'=>'success'], 200);
    	}

            public function listAdminDelete(Request $request)
            {
                $id = $request->input("id");

                $admin = User::find($id);
                $admin->removeAllRole();
                $admin->delete();
                return response()->json(['message'=>'success'], 200);
            }

    	public function password()
    	{
    		return view('back.admin.password');
    	}

    	public function passwordUpdate(Request $request)
    	{
    		if(Auth::check()){
    			User::find(Auth::user()->id)->update(['password' => bcrypt($request->input('password'))]);
    			return response()->json(['message'=>'success'], 200);
    		}
    		return response()->json(['message'=>'failed'], 500);
    	}
}
