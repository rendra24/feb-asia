<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Kalender;
use Illuminate\Http\Request;

class KalenderController extends Controller
{
	public function index()
	{
		$data['kalender'] = Kalender::orderBy('tanggal_awal', 'desc')->paginate(10);
		return view('back.kalender.index', $data);
	}

	public function save(Request $request)
	{
		$data['tanggal_awal'] = date('Y-m-d', strtotime($request->input('tanggal-awal')));
		$data['tanggal_akhir'] = date('Y-m-d', strtotime($request->input('tanggal-akhir')));
		$data['keterangan'] = $request->input('keterangan');
		Kalender::create($data);
		return response()->json(['message'=>"success"], 201);
	}
	public function update(Request $request)
	{
		$data['tanggal_awal'] = date('Y-m-d', strtotime($request->input('tanggal-awal')));
		$data['tanggal_akhir'] = date('Y-m-d', strtotime($request->input('tanggal-akhir')));
		$data['keterangan'] = $request->input('keterangan');
		$id = $request->input('id');
		Kalender::find($id)->update($data);
		return response()->json(['message'=>"success"], 200);
	}
	public function delete(Request $request)
	{
		$id = $request->id;
		Kalender::find($id)->delete();
		return response()->json(['message'=>'success'], 200);
	}
}
