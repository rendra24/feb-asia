<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\JamBimbingan;
use App\JamBimbinganDetail;
use Illuminate\Http\Request;

class JamBimbinganDetailController extends Controller
{
	public function index($id)
	{
		$data['jamBimbingan'] = JamBimbingan::with('details')->find($id);
		return view('back.jamBimbingan.detail', $data);
	}

	public function delete(Request $request)
	{
		JamBimbinganDetail::find($request->id)->delete();
		return response()->json(['message'=>'success'], 200);
	}

	public function save($id, Request $request)
	{
		$data= $request->only('hari','jam', 'nama');
		$data['jam_bimbingan_id'] = $id;

		JamBimbinganDetail::create($data);
		return response()->json(['message'=>'success'], 201);
	}

	public function update($id, Request $request)
	{
		$jam_bimbingan_id = $request->input('id');
		$data= $request->only('hari','jam', 'nama');
		
		JamBimbinganDetail::find($jam_bimbingan_id)->update($data);
		return response()->json(['message'=>'success'], 200);
	}

}
