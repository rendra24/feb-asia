<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Pengumuman;
use Illuminate\Http\Request;
use Auth;

class PengumumanController extends Controller
{
	public function index()
	{
    		$data['pengumuman'] = Pengumuman::orderBy('id', 'desc')->with('user')->paginate(10);
		return view('back.pengumuman.index', $data);
	}
	public function add()
	{
		return view('back.pengumuman.add');
	}
	public function edit($id)
	{
		$data['pengumuman'] = Pengumuman::find($id);
		return view('back.pengumuman.edit', $data);
	}
	public function save(Request $request)
	{
		$this->validate($request, [
		    'title' => 'required',
		    'content' => 'required',
		],$this->messages());
		$data['title'] = $request->input('title');
		$data['slug'] = str_slug($data['title']);
		$data['content'] = $request->input('content');
		$data['user_id'] = Auth::user()->id;
		Pengumuman::create($data);
		return response()->json(['message'=>"success"], 201);
	}

	public function update($id, Request $request)
	{
		$this->validate($request, [
		    'title' => 'required',
		    'content' => 'required',
		],$this->messages());
		$data['title'] = $request->input('title');
		$data['slug'] = str_slug($data['title']);
		$data['content'] = $request->input('content');
		$data['user_id'] = Auth::user()->id;
		$pengumuman = Pengumuman::find($id)->update($data);
		return response()->json(['message'=>"success"], 201);
	}

	public function delete(Request $request)
	{
		$id =$request->input('id');
		Pengumuman::find($id)->delete();
		return response()->json(['message'=>'success'], 200);
	}

	public function messages()
	{
	    return [
	        'title.required' => 'Title Harus Di Isi',
	        'content.required'  => 'Content  Harus Di Isi',
	    ];
	}
}
