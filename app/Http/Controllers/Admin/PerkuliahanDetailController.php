<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Perkuliahan;
use App\PerkuliahanDetail;
use Illuminate\Http\Request;

class PerkuliahanDetailController extends Controller
{
	public function index($id)
	{
		$data['perkuliahan'] = Perkuliahan::findOrFail($id);
		$data['detail'] = PerkuliahanDetail::where('perkuliahan_id','=',$id)->get();
		return view('back.perkuliahan.detail', $data);
	}

	public function delete(Request $request)
	{
		PerkuliahanDetail::find($request->id)->delete();
		return response()->json(['message'=>'success'], 200);
	}

	public function save($id, Request $request)
	{
		$this->validate($request, [
			'hari' => 'required',
			'jam' => 'required',
			'ruangan' => 'required',
			'kelas' => 'required',
			'mata_kuliah' => 'required',
			'dosen_pengampu' => 'required',
		],$this->messages());
		$data['hari'] = $request->input('hari');
		$data['jam'] = $request->input('jam');
		$data['ruangan'] = $request->input('ruangan');
		$data['kelas'] = $request->input('kelas');
		$data['mata_kuliah'] = $request->input('mata_kuliah');
		$data['dosen_pengampu'] = $request->input('dosen_pengampu');
		$data['perkuliahan_id'] = $id;

		PerkuliahanDetail::create($data);
		return response()->json(['message'=>'success'], 201);
	}

	public function update($id, Request $request)
	{
		$this->validate($request, [
			'hari' => 'required',
			'jam' => 'required',
			'ruangan' => 'required',
			'kelas' => 'required',
			'mata_kuliah' => 'required',
			'dosen_pengampu' => 'required',
		],$this->messages());
		$id = $request->input('id');
		$data['hari'] = $request->input('hari');
		$data['jam'] = $request->input('jam');
		$data['ruangan'] = $request->input('ruangan');
		$data['kelas'] = $request->input('kelas');
		$data['mata_kuliah'] = $request->input('mata_kuliah');
		$data['dosen_pengampu'] = $request->input('dosen_pengampu');
		
		PerkuliahanDetail::find($id)->update($data);
		return response()->json(['message'=>'success'], 200);
	}

	public function messages()
	{
	    return [
	       'hari' => 'Hari Masih Kosong, Harus Di Isi',
		'jam' => 'Jam Masih Kosong, Harus Di Isi',
		'ruangan' => 'Ruangan Masih Kosong, Harus Di Isi',
		'kelas' => 'Kelas Masih Kosong, Harus Di Isi',
		'mata_kuliah' => 'Mata Kuliah Masih Kosong, Harus Di Isi',
		'dosen_pengampu' => 'Dosen Pengampu Masih Kosong, Harus Di Isi',
	    ];
	}
}
