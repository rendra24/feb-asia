<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Link;
use Illuminate\Http\Request;

class LinkController extends Controller
{
	public function index()
	{
		$data['link'] = Link::paginate(10); 
		return view('back.link.index', $data);
	}

	public function save(Request $request)
	{
		$this->validate($request, [
			'name' => 'required',
			'url' => 'required',
		],$this->messages());
		$data['name'] =  $request->input('name');
		$data['url'] = $request->input('url');
		$Link = Link::create($data);

		return response()->json(['message'=>'success'], 201);
	}
	public function update(Request $request)
	{
		$this->validate($request, [
			'name' => 'required',
			'url' => 'required',
		],$this->messages());
		$data['name'] =  $request->input('name');
		$data['url'] = $request->input('url');
		$id = $request->input('id');
		Link::find($id)->update($data);

		return response()->json(['message'=>'success'], 200);
	}

	public function delete(Request $request)
	{
		$id = $request->input("id");
		Link::find($id)->delete();
		return response()->json(['message'=>'success'], 200);
	}
	public function messages()
	{
	    return [
	        'name.required' => 'Nama Website Masih Kosong, Harus Di Isi',
	        'url.required' => 'URL Website Masih Kosong, Harus Di Isi',
	    ];
	}
}
