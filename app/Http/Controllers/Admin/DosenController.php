<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\ProfilDosen;
use Illuminate\Foundation\Http\response;
use Illuminate\Http\Request;

class DosenController extends Controller
{
	public function index()
	{
		$data['profil'] = ProfilDosen::orderBy('id','desc')->paginate(12);
		return view('back.dosen.index', $data);
	}

	public function save(Request $request)
	{
		$this->validate($request, [
		    'nama-dosen' => 'required',
		    'foto' => 'required',
		],$this->messages());

		$data['nama'] = $request->input('nama-dosen');
		$data['foto'] = $request->input('foto');
		ProfilDosen::create($data);
		return response()->json(['message'=>"success"], 201);
	}
	public function update(Request $request)
	{
		$this->validate($request, [
		    'nama-dosen' => 'required',
		],$this->messages());

		$data['nama'] = $request->input('nama-dosen');
		if($request->input('foto')!=null&&$request->input('foto')!=''){
			$data['foto'] = $request->input('foto');
		}
		$id = $request->input('id');
		ProfilDosen::find($id)->update($data);
		return response()->json(['message'=>"success"], 200);
	}

	public function delete(Request $request)
	{
		$id = $request->input('id');
		ProfilDosen::find($id)->delete();
		return response()->json(['message'=>'success'], 200);
	}
	public function messages()
	{
	    return [
	        'nama.required' => 'Nama Dosen Harus Di Isi',
	        'foto.required' => 'Foto Dosen Masih Kosong',
	    ];
	}
}
