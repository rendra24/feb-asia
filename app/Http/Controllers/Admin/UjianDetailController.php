<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\JenisUjian;
use App\Ujian;
use App\UjianDetail;
use Illuminate\Http\Request;

class UjianDetailController extends Controller
{
	public function index($id, $jenis)
	{
		$data['ujian'] = Ujian::findOrFail($id)->load('jenis');
		$data['details'] = UjianDetail::where('ujian_id', '=', $id)->orderBy('id', 'desc')->get();
		// return $data;
		return view('back.ujian.detail', $data);
	}

	public function delete(Request $request)
	{
		UjianDetail::find($request->id)->delete();
		return response()->json(['message'=>'success'], 200);
	}

	public function save($id, Request $request)
	{
		$this->validate($request, [
			'hari' => 'required',
			'tanggal' => 'required',
			'jam' => 'required',
			'ruang' => 'required',
			'nim' => 'required',
			'nama_mahasiswa' => 'required',
			'judul' => 'required',
		],$this->messages());
		$data['hari'] = $request->input('hari');
		$data['tanggal'] = $request->input('tanggal');
		$data['jam'] = $request->input('jam');
		$data['ruang'] = $request->input('ruang');
		$data['nim'] = $request->input('nim');
		$data['nama_mahasiswa'] = $request->input('nama_mahasiswa');
		$data['judul'] = $request->input('judul');
		$data['ujian_id'] = $id;

		UjianDetail::create($data);
		return response()->json(['message'=>'success'], 201);
	}

	public function update($id, Request $request)
	{
		$this->validate($request, [
			'hari' => 'required',
			'tanggal' => 'required',
			'jam' => 'required',
			'ruang' => 'required',
			'nim' => 'required',
			'nama_mahasiswa' => 'required',
			'judul' => 'required',
		],$this->messages());
		$id = $request->input('id');
		$data['hari'] = $request->input('hari');
		$data['tanggal'] = $request->input('tanggal');
		$data['jam'] = $request->input('jam');
		$data['ruang'] = $request->input('ruang');
		$data['nim'] = $request->input('nim');
		$data['nama_mahasiswa'] = $request->input('nama_mahasiswa');
		$data['judul'] = $request->input('judul');
		
		UjianDetail::find($id)->update($data);
		return response()->json(['message'=>'success'], 200);
	}

	public function messages()
	{
	    return [
	       'hari' => 'Hari Masih Kosong, Harus Di Isi',
		'tanggal' => 'Tanggal Masih Kosong, Harus Di Isi',
		'jam' => 'Jam Masih Kosong, Harus Di Isi',
		'ruang' => 'Ruang Masih Kosong, Harus Di Isi',
		'nim' => 'Nim Masih Kosong, Harus Di Isi',
		'nama_mahasiswa' => 'Nama Mahasiswa Masih Kosong, Harus Di Isi',
		'judul' => 'Judul Masih Kosong, Harus Di Isi',
	    ];
	}
}
