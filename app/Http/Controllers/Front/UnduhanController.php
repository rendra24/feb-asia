<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Unduhan;
use Illuminate\Http\Request;
use File;

class UnduhanController extends Controller
{
	public function index()
	{
		$data['unduhan'] = Unduhan::paginate(10);
		return view('front.unduhan', $data);


	}
	public function download($id)
	{
		$data = Unduhan::find($id);
		// return strlen(trim($data->file));
		if(strlen(trim($data->file))==0){
			// return "dd";
			return back();
		}
		$file = "/assets/unduhan/".$data->file;
		$extension = File::extension($file);
		$myFile = public_path($file);
	    	$headers = ['Content-Type: application/$extension'];
	    	$newName = $data->title.".".$extension;

	    	if(!file_exists($myFile)){
	    		return back();
	    	}
		return response()->download($myFile, $newName, $headers);

	}
}
