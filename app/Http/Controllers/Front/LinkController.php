<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LinkController extends Controller
{
    public function lpmi()
    {
    	return view('front.lpmi');
    }

    public function alumni()
    {
	    return view('front.alumni');
    }
    public function hackathon()
    {
	    return view('front.hack');
    }
     public function mikrotik()
    {
	    return view('front.mikrotik');
    }
}
