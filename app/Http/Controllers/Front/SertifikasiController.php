<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Sertifikasi;
use Illuminate\Http\Request;

class SertifikasiController extends Controller
{
	public function index()
	{
		$data['visimisi'] = Sertifikasi::first();
		return view('front.sertifikasi.index', $data);
	}
}