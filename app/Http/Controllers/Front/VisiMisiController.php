<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\VisiMisi;
use Illuminate\Http\Request;

class VisiMisiController extends Controller
{
	public function index()
	{
		$data['visimisi'] = VisiMisi::where('prodi_id','=', 1)->first();
		return view('front.visi-misi.index', $data);
	}
}
