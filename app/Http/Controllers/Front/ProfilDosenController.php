<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\ProfilDosen;
use Illuminate\Http\Request;

class ProfilDosenController extends Controller
{
	public function index()
	{
		$data['profil'] = ProfilDosen::orderBy('nama','asc')->paginate(12);
		return view('front.profil_dosen', $data);
	}
}
