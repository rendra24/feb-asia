<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Pengumuman;
use Illuminate\Http\Request;

class PengumumanController extends Controller
{
	public function index()
	{
		$data['pengumuman'] = Pengumuman::orderBy('id', 'desc')->paginate(10);
		return view('front.pengumuman.index', $data);
	}
	public function show($id, $slug)
	{
		$data['pengumuman'] = Pengumuman::find($id);
		return view('front.pengumuman.detail', $data);
	}
}
