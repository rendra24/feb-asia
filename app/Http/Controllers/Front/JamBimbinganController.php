<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\JamBimbingan;
use App\JamBimbinganDetail;
use Illuminate\Http\Request;

class JamBimbinganController extends Controller
{
	public function index()
	{
		$data['jamBimbingan'] = JamBimbingan::with('details')
				 		->orderBy('id', 'desc')->first();;
	
		return view('front.jam-bimbingan.index', $data);
	}
	public function search(Request $request)
	{
		$search = $request->q;
		// return $search;
		$jamBimbingan = JamBimbingan::orderBy('id', 'desc')->first();
		$data['jamBimbingan'] = $jamBimbingan;
		$data['details'] = JamBimbinganDetail::where('jam_bimbingan_id','=', $jamBimbingan->id)
											->where(function ($query) use ($search){
												return $query->where('hari','like','%'.$search.'%')
												->orWhere('jam','like','%'.$search.'%')
												->orWhere('nama','like','%'.$search.'%');
											})->get();
		return view('front.jam-bimbingan.search', $data);
	}
}
