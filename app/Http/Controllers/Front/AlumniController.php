<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AlumniController extends Controller
{
    public function index()
    {
    	return view('front.alumni.index');

    }

    public function strukturorganisasi()
    {
        return view ('front.alumni.struktur-organisasi');
    }
    public function tokoh()
    {
        return view ('front.alumni.tokoh');
    }
    public function galeri()
    {
        return view ('front.alumni.galeri');
    }
    public function sumbangan()
    {
        return view ('front.alumni.sumbangan');
    }
    public function pustaka()
    {
        return view ('front.alumni.pustaka');
    }
}
