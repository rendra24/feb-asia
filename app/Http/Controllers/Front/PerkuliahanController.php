<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Perkuliahan;
use App\PerkuliahanDetail;
use Illuminate\Http\Request;

class PerkuliahanController extends Controller
{
	public function index()
	{
		$data['perkuliahan'] = Perkuliahan::orderBy('id', 'desc')->first()->load('details');
		return view('front.perkuliahan.index', $data);
	}
	public function search(Request $request)
	{
		$search = $request->q;
		// return $search;
		$perkuliahan = Perkuliahan::orderBy('id', 'desc')->first();
		$data['perkuliahan'] = $perkuliahan;
		$data['details'] = PerkuliahanDetail::where('perkuliahan_id','=', $perkuliahan->id)
											->where(function ($query) use ($search){
												return $query->where('hari','like','%'.$search.'%')
												->orWhere('jam','like','%'.$search.'%')
												->orWhere('ruangan','like','%'.$search.'%')
												->orWhere('kelas','like','%'.$search.'%')
												->orWhere('mata_kuliah','like','%'.$search.'%')
												->orWhere('dosen_pengampu','like','%'.$search.'%');
											})->get();
		
		return view('front.perkuliahan.search', $data);
	}
}
