<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Kalender;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;

class KalenderController extends Controller
{
	public function index()
	{
		return view('front.kalender.index');
	}
	public function getEvents()
	{
		$month = Carbon::now()->format('m');
		$data = Kalender::select('tanggal_awal as start', DB::raw('DATE_ADD(tanggal_akhir, INTERVAL 1 DAY) as end'), 'keterangan as title','id')
							->whereMonth('tanggal_awal',$month)
							->orWhere(function ($query) use($month) {
					                $query->whereMonth('tanggal_akhir',$month);
					            })->groupBy('id')->get();
		return $data;
	}
	public function eventsMonth(Request $request)
	{
		$month = $request->month;
		$data = Kalender::select('tanggal_awal as start', DB::raw('DATE_ADD(tanggal_akhir, INTERVAL 1 DAY) as end'), 'keterangan as title','id')
							->whereMonth('tanggal_awal',$month)
							->orWhere(function ($query) use($month) {
					                $query->whereMonth('tanggal_akhir',$month);
					            })->groupBy('id')->get();
		return $data;
	}
}
