<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Session;

class UserController extends Controller
{
	public function login()
	{
		if(Auth::user()){ return redirect()->route('admin-index'); }
		return view('back.login');
	}
	public function doLogin(Request $request)
    	{
	        $messages=[
	            "required"=>':attribute harus di isi',
	            "email" =>'format :attribute tidak sesuai',
	        ];

	    	$this->validate($request, [
		    'email' => 'required|email',
		     'password' => 'required',
		], $messages);

	    	$data = $request->only('email', 'password');
	    	if (Auth::attempt($data)) {
	            return redirect()->route('admin-index');
	        }

	    	Session::flash('message','Email atau Password Salah !!!');
	       return back();
    }
}
