<?php

namespace App\Http\Controllers\Front;

use App\Prodi;
use App\Berita;
use App\Prestasi;
use App\Pengumuman;
use App\ProgramStudi;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
	public function index()
	{
		$data['prodi'] = ProgramStudi::where('status','=', 1)->orderBy('publish', "desc")->limit(3)->with('user')->get();
		$data['berita'] = Berita::where('status','=', 1)->orderBy('publish', "desc")->limit(3)->with('user')->get();
		// $data['pengumuman'] = Pengumuman::orderBy('id', 'desc')->with('user')->paginate(3);
		$data['prestasi'] = Prestasi::where('status','=', 1)->orderBy('publish', "desc")->limit(3)->with('user')->get();
		
		return view('front.fakultas.ftd', $data);
	}

    public function ftd()
    {
        return view('front.fakultas.ftd');
    }

	public function prodi($prodi)
	{
		$data['prodi'] =Prodi::where('name','=',$prodi)->first()->load('Profil', 'Visimisi');
		return view('front.prodi', $data);
	}
}