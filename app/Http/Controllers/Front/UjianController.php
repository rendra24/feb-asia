<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\JenisUjian;
use App\Ujian;
use App\UjianDetail;
use Illuminate\Http\Request;

class UjianController extends Controller
{
	public function index($jenis)
	{
		$ujian = Ujian::with('jenis')
						->whereHas('jenis',  function($query) use($jenis) {
							$query->where('name', '=', $jenis);
						})
				 		->orderBy('id', 'desc')->first();
		$data['ujian'] = $ujian;
		$data['details'] = UjianDetail::where('ujian_id','=', $ujian->id)->orderBy('id', 'desc')->get();
		return view('front.ujian.index', $data);
	}

	public function search($jenis,Request $request)
	{
		
		$search = $request->q;
		// return $search;
		$ujian = Ujian::with('jenis')
						->whereHas('jenis',  function($query) use($jenis) {
							$query->where('name', '=', $jenis);
						})->orderBy('id', 'desc')->first();
		
		$data['ujian'] = $ujian;
		$data['details'] = UjianDetail::where('ujian_id','=', $ujian->id)
						->where(function ($query) use ($search){
						    	$query->where('hari','like','%'.$search.'%')
									->orWhere('tanggal','like','%'.$search.'%')
									->orWhere('jam','like','%'.$search.'%')
									->orWhere('ruang','like','%'.$search.'%')
									->orWhere('nim','like','%'.$search.'%')
									->orWhere('nama_mahasiswa','like','%'.$search.'%')
									->orWhere('judul','like','%'.$search.'%');
						})->orderBy('id', 'desc')->get();
				 		
		return view('front.ujian.search', $data);
	}

}
