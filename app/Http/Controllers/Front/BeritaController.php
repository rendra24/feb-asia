<?php

namespace App\Http\Controllers\Front;

use App\Berita;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BeritaController extends Controller
{
	public function index()
	{
		$data['berita'] = Berita::where('status','=', 1)->orderBy('id', 'desc')->paginate(9);
		return view('front.berita.index', $data);
	}

	public function detail($id, $slug)
	{
		$data['berita'] = Berita::find($id)->load('user');
		$data['lain'] = Berita::where('id','<>', $id)->where('status','=', 1)->orderBy('publish', 'desc')->limit(5)->get();
		return view('front.berita.detail', $data);
	}
}
