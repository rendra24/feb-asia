<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisUjian extends Model
{
	protected $table = "jenis_ujian";

	protected $fillable = ['name', 'description'];

	public function ujian()
	{
		return $this->hasMany('App\Ujian');
	}
}
