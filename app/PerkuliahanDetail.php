<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PerkuliahanDetail extends Model
{
    	protected $table = "perkuliahan_detail";

    	protected $fillable = ['hari', 'jam', 'ruangan', 'kelas', 'mata_kuliah', 'dosen_pengampu', 'perkuliahan_id'];

    	public function Perkuliahan()
    	{
    		return $this->belongsTo('App\Perkuliahan');
    	}
}
