<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Kalender extends Model
{
    	protected $table = "kalender";

	protected $fillable = ['tanggal_awal', 'tanggal_akhir', 'keterangan'];


    public function getAwalAttribute()
    {
        return Carbon::parse($this->tanggal_awal)->format('d F Y');
    }
    public function getAkhirAttribute()
    {
        return Carbon::parse($this->tanggal_akhir)->format('d F Y');
    }
}
