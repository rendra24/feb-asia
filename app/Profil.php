<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profil extends Model
{
	protected $table = "profil";

	protected $fillable = ['content', 'prodi_id'];


	public function prodi()
	{
		return $this->hasOne('App\Prodi');
	}
	public function getBodyAttribute()
    	{
    		return str_limit($this->content, 210);
    	}
}
