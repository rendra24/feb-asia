<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sertifikasi extends Model
{
	protected $table = "sertifikasi";

	protected $fillable = ['content'];

}