<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perkuliahan extends Model
{
    	protected $table = "perkuliahan";

	protected $fillable = ['tahun_ajaran', 'semester'];

	public function details()
	{
		return $this->hasMany('App\PerkuliahanDetail');
	}
}
