<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class UjianDetail extends Model
{
    	protected $table = "ujian_detail";

    	protected $fillable = ['hari', 'tanggal', 'jam', 'ruang', 'nim', 'nama_mahasiswa', 'judul', 'ujian_id'];

    	public function ujian()
    	{
    		return $this->belongsTo('App\Ujian');
    	}

   
}
