<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ujian extends Model
{
    	protected $table = "ujian";

	protected $fillable = ['tahun_ajaran', 'semester', 'jenis_ujian_id'];

	public function details()
	{
		return $this->hasMany('App\UjianDetail');
	}
	public function jenis()
    	{
    		return $this->belongsTo('App\JenisUjian', 'jenis_ujian_id');
    	}
}
