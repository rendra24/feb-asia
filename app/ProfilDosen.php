<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfilDosen extends Model
{
	protected $table = "profil_dosen";

	protected $fillable = ['nama', 'foto'];

}
