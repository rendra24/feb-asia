<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prodi extends Model
{
	protected $table = "prodi";

	protected $fillable = ['name', 'description'];

	public function Visimisi()
	{
		return $this->hasOne('App\VisiMisi', 'prodi_id');
	}
	public function Profil()
	{
		return $this->hasOne('App\Profil', 'prodi_id');
	}
}
