<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>FEB ASIA :: Login</title>

		<meta name="description" content="User login page" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="{{ URL::to('/assets/back/css/bootstrap.min.css') }}" />
		<link rel="stylesheet" href="{{ URL::to('/assets/back/font-awesome/css/font-awesome.min.css') }}" />

		<!-- text fonts -->
		<link rel="stylesheet" href="{{ URL::to('/assets/back/css/fonts.googleapis.com.css') }}" />

		<!-- ace styles -->
		<link rel="stylesheet" href="{{ URL::to('/assets/back/css/ace.min.css') }}" />

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="{{ URL::to('/assets/back/css/ace-part2.min.css') }}" />
		<![endif]-->
		<link rel="stylesheet" href="{{ URL::to('/assets/back/css/ace-rtl.min.css') }}" />

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="{{ URL::to('/assets/back/css/ace-ie.min.css') }}" />
		<![endif]-->

		<!-- HTML5shiv and Respond.js') }} for IE8 to support HTML5 elements and media queries -->

		<!--[if lte IE 8]>
		<script src="{{ URL::to('/assets/back/js/html5shiv.min.js') }}"></script>
		<script src="{{ URL::to('/assets/back/js/respond.min.js') }}"></script>
		<![endif]-->
		<style type="text/css">
			.help-block{
				margin-top: 0px;
				margin-bottom: 0px;
				color: #EF5F5A !important;
				font-style: italic;
			}	
		</style>
	</head>

	<body class="login-layout light-login">
		<div class="main-container">
			<div class="main-content">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1" style="padding-top:75px;">
						<div class="login-container">
							<div class="center">
								@if(Session::has('message'))
					                        <div class="alert alert-danger alert-dismissible" role="alert">
					                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					                              <strong><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> </strong> {{ Session::get('message') }}
					                        </div>    
					                  @endif  
								<h1>
									<i class="ace-icon fa fa-user-circle-o green"></i>
									<span class="red">LOGIN </span>
									<span class="white" id="id-text2">STMIK ASIA</span>
								</h1>
							</div>

							<div class="space-6"></div>
							
							<div class="position-relative">
								<div id="login-box" class="login-box visible widget-box no-border">
									<div class="widget-body">
										<div class="widget-main">
											<h4 class="header blue lighter bigger">
												<i class="ace-icon fa fa-coffee green"></i>
												Please Enter Your Information
											</h4>

											<div class="space-6"></div>
											
											<form action="{{ route('doLogin') }}" method="post">
												{{ csrf_field() }}
												<fieldset>
													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="email" class="form-control" placeholder="Email" name="email" />
															<i class="ace-icon fa fa-envelope-o"></i>
														</span>
													  	@if($errors->has('email'))
															<span class="help-block"> {{$errors->first('email')}}</span>
													  	@endif
													</label>

													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="password" name="password" class="form-control" placeholder="Password" />
															<i class="ace-icon fa fa-lock"></i>
														</span>
														@if($errors->has('password'))
															<span class="help-block" > {{$errors->first('password')}}</span>
													  	@endif
													</label>

													<div class="space"></div>

													<div class="clearfix">

														<button type="submit" class="btn btn-sm btn-primary btn-block">
															<i class="ace-icon fa fa-key"></i>
															<span class="bigger-110">Login</span>
														</button>
													</div>

													<div class="space-4"></div>
												</fieldset>
											</form>

											
										</div><!-- /.widget-main -->

										<div class="toolbar clearfix" style="height:7px;">
										</div>
									</div><!-- /.widget-body -->
								</div><!-- /.login-box -->

							</div><!-- /.position-relative -->

						</div>
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div><!-- /.main-content -->
		</div><!-- /.main-container -->

		<!-- basic scripts -->

		<!--[if !IE]> -->
		<script src="{{ URL::to('/assets/back/js/jquery-2.1.4.min.js') }}"></script>

		<!-- <![endif]-->

		<!--[if IE]>
<script src="{{ URL::to('/assets/back/js/jquery-1.11.3.min.js') }}"></script>
<![endif]-->

		
	</body>
</html>
