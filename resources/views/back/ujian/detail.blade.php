@extends('back.layout.app')
@push('style')
	<link rel="stylesheet" type="text/css" href="{{ URL::to('assets/back/flatpickr/flatpickr.min.css') }}">
@endpush

@section('content')
	<div class="page-header">
		<div class="row">
			<div class="col-sm-9">
				<h1><i class="fa fa-university"></i>&nbsp;&nbsp; Jadwal Ujian  Semester {{ $ujian->semester }} Ta. {{ $ujian->tahun_ajaran }}</h1>
			</div>
                  <div class="col-sm-3 text-right">
                        <div class="btn-group" role="group" aria-label="...">
					<a href="{{ route('ujian-index', $ujian->jenis->name) }}" class="btn btn-white btn-warning btn-bold" style="margin-right: 5px;">
						<i class="fa fa-angle-double-left"></i>&nbsp;&nbsp; Back
					</a>
                      		<button type="button" class="btn btn-white btn-primary btn-bold" data-toggle="modal" data-target="#modalDetail" data-status="add"><i class="fa fa-plus"></i>&nbsp;&nbsp;&nbsp;Add New </button>
                      	</div>
                    </div>
		</div>
	</div><!-- /.page-header -->

	<div class="row">
		<div class="col-xs-12">
			<div class="table-responsive">
		              <table class="table  table-bordered table-hover" >
		                  <thead>
			                  <tr>
			                      <th class="text-center">Hari</th>
			                      <th class="text-center">Tanggal</th>
			                      <th class="text-center">Jam</th>
			                      <th class="text-center">Ruang</th>
			                      <th class="text-center">NIM</th>
			                      <th class="text-center">Nama Mahasiswa</th>
			                      <th class="text-center">Judul</th>
			                      <th>&nbsp;</th>
			                  </tr>
		                  </thead>
		                  <tbody>
		                  	@if($details)
		                      	@foreach($details as $data)
				                   <tr>
				                      	<td class="text-center">
				                      		{{ $data->hari }}
				                      	</td>
				                      	<td class="text-center">
				                      		{{ $data->tanggal }}
				                      	</td>
				                      	<td class="text-center">
				                      		{{ $data->jam }}
				                      	</td>
				                      	<td>
				                      		{{ $data->ruang }}
				                      	</td>
				                      	<td class="text-center">
				                      		{{ $data->nim }}
				                      	</td>
				                      	<td>
				                      		{{ $data->nama_mahasiswa }}
				                      	</td>
				                      	<td>
				                      		{{ $data->judul }}
				                      	</td>
				                          <td class="text-center">
				                              <button class="btn btn-white btn-success btn-bold btn-sm" data-toggle="modal" data-target="#modalDetail" data-status="edit" data-hari="{{ $data->hari }}" data-jam="{{ $data->jam }}" data-tanggal="{{ $data->tanggal }}" data-ruang="{{ $data->ruang }}" data-nim="{{ $data->nim }}" data-nama_mahasiswa="{{ $data->nama_mahasiswa }}" data-judul="{{ $data->judul }}" data-id="{{ $data->id }}"  ><i class="fa fa-pencil"></i>&nbsp; Edit</button>
				                              <button class="btn btn-white btn-danger btn-bold btn-sm btn-delete" detailId="{{ $data->id }}"  ><i class="fa fa-trash-o "></i>&nbsp; Delete</button>
				                          </td>
				                    </tr>
		                      	@endforeach
		                      	@endif

		                        
		                  </tbody>
		              </table>
	              </div>
		</div><!-- /.col -->
	</div><!-- /.row -->
@include('back.ujian.detailForm')
@endsection
@push('scripts')
	<script type="text/javascript" src="{{ URL::to('assets/back/flatpickr/flatpickr.min.js') }}"></script>

	<script type="text/javascript">

		$(document).ready(function(){
			// $('#modalForm').modal({backdrop: 'static', keyboard: false});
			// $('#modalForm').modal({backdrop: 'static', keyboard: false})；
			      $(".btn-delete").click(function() {
			      		var detailId = $(this).attr("detailId");
			              swal({
			                title: "Apakah Anda Yakin!",
			                type: "error",
			                confirmButtonClass: "btn-danger",
			                confirmButtonText: "Yes!",
			                showCancelButton: true,
			            }).then(function () {
				                $.ajax({
				                    type: "POST",
				                    url: "{{ route('ujian-detail-delete') }}",
				                    data: {id:detailId},
				                    success: function (data) {
				                            swal(
				                                'Deleted!',
				                                'Detail Jadwal Ujian Berhasil Di Hapus',
				                                'success'
				                              ).then(
				                                function () {
				                                   location.reload();
				                                 },
				                                function (dismiss) {
				                                  if (dismiss === 'timer') {
				                                     location.reload();
				                                  }
				                                }
				                              );
				                           
				                    }, error(data){
				                          swal(
				                            'Cancelled',
				                            'Detail Jadwal Ujian Gagal Di Hapus',
				                            'error'
				                          );
				                    }         
				                });
				          }, function (dismiss) {
				        
				          });
			      });
		});
	</script>
@endpush