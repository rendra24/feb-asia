<!-- Modal -->
<div class="modal fade" id="modalDetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">
    <i class="fa fa-plus"></i>&nbsp;&nbsp;Form <span class="title"></span> Jadwal Ujian
        </h4>
      </div>
      <form action="" method="post" id="formDetail">
      <div class="modal-body">
          <div class="form-horizontal">
      <div class="form-group form-hari">
            <label for="inputEmail3" class="col-sm-3 control-label">Hari</label>
            <div class="col-sm-9">
              <input type="text" class="form-control hari" placeholder="Masukkan Hari" name="hari">
                                <span  class="help-block" style="display: none;">*Hari Masih Kosong, Harus Di Isi</span>
            </div>
      </div>
                   <div class="form-group form-tanggal">
                          <label for="inputEmail3" class="col-sm-3 control-label">Tanggal</label>
                          <div class="col-sm-9">
                                <div class="input-group tanggal-div" data-wrap="true" data-clickOpens="false">
                                    <input class="  form-control tanggal" data-input  name="tanggal">
                                    <span class="input-group-btn">
                                      <button class="btn btn-white btn-primary btn-bold" type="button" data-toggle>
                                          <i class="fa fa-calendar"></i>
                                      </button>
                                    </span>
                                </div>
                          </div>
                   </div>
                   <div class="form-group form-jam">
                          <label for="inputEmail3" class="col-sm-3 control-label">Jam</label>
                          <div class="col-sm-9">
                                <input type="text" class="form-control jam" placeholder="Masukkan Jam" name="jam">
                                <span  class="help-block" style="display: none;">*Jam Masih Kosong, Harus Di Isi</span>
                          </div>
                   </div>
                   <div class="form-group form-ruang">
                          <label for="inputEmail3" class="col-sm-3 control-label">Ruang</label>
                          <div class="col-sm-9">
                                <input type="text" class="form-control ruang" placeholder="Masukkan Ruang" name="ruang">
                                <span  class="help-block" style="display: none;">*Ruang Masih Kosong, Harus Di Isi</span>
                          </div>
                   </div>
                   <div class="form-group form-nim">
                          <label for="inputEmail3" class="col-sm-3 control-label">NIM</label>
                          <div class="col-sm-9">
                                <input type="text" class="form-control nim" placeholder="Masukkan NIM" name="nim">
                                <span  class="help-block" style="display: none;">*NIM Masih Kosong, Harus Di Isi</span>
                          </div>
                   </div>
                   <div class="form-group form-nama_mahasiswa">
                          <label for="inputEmail3" class="col-sm-3 control-label">Nama Mahasiswa</label>
                          <div class="col-sm-9">
                                <input type="text" class="form-control nama_mahasiswa" placeholder="Masukkan Nama Mahasiswa" name="nama_mahasiswa">
                                <span  class="help-block" style="display: none;">*Nama Mahasiswa Masih Kosong, Harus Di Isi</span>
                          </div>
                   </div>
                   <div class="form-group form-judul">
                          <label for="inputEmail3" class="col-sm-3 control-label">Judul</label>
                          <div class="col-sm-9">
                                <input class="id" type="hidden" name="id" value="">
                                <textarea name="judul" id="" cols="30" rows="3" class="form-control judul"></textarea>
                                <span  class="help-block" style="display: none;">*Judul Masih Kosong, Harus Di Isi</span>
                          </div>
                   </div>
    </div>
      </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;&nbsp;Batal</button>
          <button type="submit" class="btn btn-primary" id="btn-save"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Simpan</button>
      </div>
      </form>
    </div>
  </div>
</div>

@push('scripts')
  <script type="text/javascript">
    $(document).ready(function(){
       var tanggal = $(".tanggal-div").flatpickr({ 
                  dateFormat: 'd-m-Y', 
                  defaultDate: Date.now()
        });
      var status_form=true;
      $('#modalDetail').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var status = button.data('status');
            var modal = $(this);
            if(status=='edit'){
                    status_form=false;
                    var id = button.data('id');
                    var tanggal = button.data('tanggal');
                    var hari = button.data('hari');
                    var jam = button.data('jam');
                    var ruang = button.data('ruang');
                    var nim = button.data('nim');
                    var nama_mahasiswa = button.data('nama_mahasiswa');
                    var judul = button.data('judul');
                    console.log(id);
                      modal.find('.modal-body').find("input[name='id']").val(id);
                      modal.find('.modal-body').find("input[name='tanggal']").val(tanggal);
                      modal.find('.modal-body').find("input[name='hari']").val(hari);
                      modal.find('.modal-body').find("input[name='jam']").val(jam);
                      modal.find('.modal-body').find("input[name='ruang']").val(ruang);
                      modal.find('.modal-body').find("input[name='nim']").val(nim);
                      modal.find('.modal-body').find("input[name='nama_mahasiswa']").val(nama_mahasiswa);
                      modal.find('.modal-body .judul').val(judul);
                    modal.find('.modal-title').find(".fa").removeClass("fa-plus");
                    modal.find('.modal-title').find(".fa").addClass("fa-pencil-square-o");
                    modal.find('.modal-title').find(".title").text("Edit ");
            }else{
                    status_form=true;
                    modal.find('.modal-title').find(".fa").removeClass("fa-pencil-square-o");
                    modal.find('.modal-title').find(".fa").addClass("fa-plus");
                    modal.find('.modal-title').find(".title").text("Tambah ");
            }
      });

      $('#modalDetail').on('hide.bs.modal', function (event) {
             var modal = $(this);
            modal.find('.modal-body').find("input[name='id']").val('');
            modal.find('.modal-body').find("input[name='hari']").val('');
            modal.find('.modal-body').find("input[name='jam']").val('');
            modal.find('.modal-body').find("input[name='ruang']").val('');
            modal.find('.modal-body').find("input[name='nim']").val('');
            modal.find('.modal-body').find("input[name='nama_mahasiswa']").val('');
            modal.find('.modal-body .judul').val('');
            $(".help-block").hide();
            $(".form-group").removeClass('has-error');
      });

      $("#formDetail").submit(function(e){
            if(status_form){
              var url = "{{ route('ujian-detail-save', $ujian->id) }}";
            }else{
              var url = "{{ route('ujian-detail-update', $ujian->id) }}";
            }
          e.preventDefault();
          if(isValid()){
                      $.ajax({
                          type: "POST",
                          url: url,
                          data: $(this).serialize(),
                          beforeSend: function() {
                            $("#btn-save .fa").removeClass('fa-floppy-o');
                            $("#btn-save .fa").addClass("fa-spinner fa-pulse fa-lg");
                          },success: function (data) {
                            console.log(data);
                                swal(
                                      'Berhasil!',
                                      'Jadwal Ujian Berhasil Di Simpan',
                                      'success'
                                ).then(
                                      function () {
                                          location.reload();
                                      },function (dismiss) {
                                          if (dismiss === 'timer') {
                                              location.reload();
                                          }
                                  }
                                );
                          },error:function(data, status){
                               if(data.status==422){
                                       isValid();
                                }else{
                                      swal(
                                                'Gagal!',
                                                'Jadwal Ujian Gagal Di Simpan',
                                                'error'
                                          ).then(
                                                function () {
                                                },function (dismiss) {
                                                    if (dismiss === 'timer') {
                                                    }
                                            }
                                          );
                                }
                          },complete: function() {
                            $("#btn-save .fa").removeClass("fa-spinner fa-pulse fa-lg");
                            $("#btn-save .fa").addClass('fa-floppy-o');
                          }
                        });
             }
            return false;
      });

      function isValid(){
            var hari = validasi('hari');
            var jam = validasi('jam');
            var ruang = validasi('ruang');
            var nim = validasi('nim');
            var nama_mahasiswa = validasi('nama_mahasiswa');
            var judul = validasi('judul');

            return (hari && jam && ruang && nim && nama_mahasiswa && judul);
      }

      function validasi(elemt){
            if($("."+elemt).val().trim().length<=0){
                    $(".form-"+elemt).addClass('has-error');
                    $(".form-"+elemt+" .help-block").show();
                    return false;
            }else{
                    $(".form-"+elemt).removeClass('has-error');
                    $(".form-"+elemt+" .help-block").hide();
                    return true;
            }
      }

    })
  </script>
@endpush