@extends('back.layout.app')

@section('content')
	<div class="page-header">
		<div class="row">
			<div class="col-sm-10">
				<h1><i class="fa fa-link"></i>&nbsp;&nbsp; Link Website</h1>
			</div>
			<div class="col-sm-2">
				<button type="button" class="btn btn-white btn-primary btn-bold btn-block" data-toggle="modal" data-target="#modalForm" data-status="add"><i class="fa fa-plus"></i>&nbsp;&nbsp;&nbsp;Add New </button>
			</div>
		</div>
	</div><!-- /.page-header -->

	<div class="row">
		<div class="col-xs-12">
			<div class="table-responsive">
		              <table class="table  table-bordered table-hover" style="font-size: 11pt;">
		                  <thead>
		                  <tr>
		                      <th class="text-center">No</th>
		                      <th>Nama</th>
		                      <th>Alamat</th>
		                      <th>&nbsp;</th>
		                  </tr>
		                  </thead>
		                  <tbody>
		                      	@foreach($link as $key => $data)
				                   <tr>
				                      	<td class="text-center">{{ ($key+1) }}</td>
				                      	<td>
				                      		{{ $data->name }}
				                      	</td>
				                      	<td>
				                      		{{ $data->url }}
				                      	</td>
				                          <td class="text-center">
				                              <button class="btn btn-white btn-info btn-bold btn-sm" data-toggle="modal" data-target="#modalForm" data-id="{{ $data->id }}" data-status="edit" data-name="{{ $data->name }}" data-url="{{ $data->url }}"><i class="fa fa-pencil"></i> Edit</button>
				                              <button class="btn btn-white btn-danger btn-bold btn-sm btn-delete" linkId="{{ $data->id }}"><i class="fa fa-trash-o "></i> Delete</button>
				                          </td>
				                   </tr>
		                      	@endforeach
		                  </tbody>
		              </table>
	              </div>
	             <div class="text-center">
	             	{{ $link->links() }}
	             </div>
		</div><!-- /.col -->
	</div><!-- /.row -->
@include('back.link.form')
@endsection
@push('scripts')
	<script type="text/javascript">

		$(document).ready(function(){
			      $(".btn-delete").click(function() {
			      		var id_link = $(this).attr("linkId");
			              swal({
			                title: "Apakah Anda Yakin!",
			                type: "error",
			                confirmButtonClass: "btn-danger",
			                confirmButtonText: "Yes!",
			                showCancelButton: true,
			            }).then(function () {
				                $.ajax({
				                    type: "POST",
				                    url: "{{ route('link-delete') }}",
				                    data: {id:id_link},
				                    success: function (data) {
				                            swal(
				                                'Deleted!',
				                                'link Website Berhasil Di Hapus',
				                                'success'
				                              ).then(
				                                function () {
				                                   location.reload();
				                                 },
				                                function (dismiss) {
				                                  if (dismiss === 'timer') {
				                                     location.reload();
				                                  }
				                                }
				                              );
				                           
				                    }, error(data){
				                          swal(
				                            'Cancelled',
				                            'link Website Gagal Di Hapus',
				                            'error'
				                          );
				                    }         
				                });
				          }, function (dismiss) {
				        
				          });
			      });
		});
	</script>
@endpush