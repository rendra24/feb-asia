<!-- Modal -->
<div class="modal fade" id="modalForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">
		<i class="fa fa-plus"></i>&nbsp;&nbsp; <span class="title"></span> link Website
        </h4>
      </div>
      <form action="" method="post" id="form">
      <div class="modal-body">
        	<div class="form-horizontal">
			<div class="form-group form-name">
			    	<label for="inputEmail3" class="col-sm-3 control-label">Nama Website</label>
			    	<div class="col-sm-9">
			    		<input type="text" class="form-control name" placeholder="Masukkan Nama Website" name="name">
                                <span  class="help-block" style="display: none;">*Nama Website Masih Kosong, Harus Di Isi</span>
			    	</div>
			</div>
			<div class="form-group form-url">
			    	<label for="inputEmail3" class="col-sm-3 control-label">URL Website</label>
			    	<div class="col-sm-9">
                                <input class="id" type="hidden" name="id" value="">
			      		<textarea name="url" id="" cols="30" rows="3" class="form-control url" placeholder="Masukkan URL Website"></textarea>
                                <span  class="help-block" style="display: none;">*URL Website Masih Kosong, Harus Di Isi</span>
			    	</div>
			</div>
		</div>
      </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;&nbsp;Batal</button>
          <button type="submit" class="btn btn-primary" id="btn-save"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Simpan</button>
      </div>
      </form>
    </div>
  </div>
</div>

@push('scripts')
  <script type="text/javascript">
    $(document).ready(function(){
      var status_form=true;
      $('#modalForm').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var status = button.data('status');
            var modal = $(this);
            if(status=='edit'){
                    status_form=false;
                    var id = button.data('id');
                    var name = button.data('name');
                    var url = button.data('url');

                      modal.find('.modal-body').find("input[name='id']").val(id);
                      modal.find('.modal-body').find("input[name='name']").val(name);
                      modal.find('.modal-body .url').val(url)
                    modal.find('.modal-title').find(".fa").removeClass("fa-plus");
                    modal.find('.modal-title').find(".fa").addClass("fa-pencil-square-o");
                    modal.find('.modal-title').find(".title").text("Edit ");
            }else{
                    status_form=true;
                    modal.find('.modal-title').find(".fa").removeClass("fa-pencil-square-o");
                    modal.find('.modal-title').find(".fa").addClass("fa-plus");
                    modal.find('.modal-title').find(".title").text("Tambah ");
            }
      });

      $('#modalDetail').on('hide.bs.modal', function (event) {
             var modal = $(this);
             modal.find('.modal-body').find("input[name='id']").val('');
            modal.find('.modal-body').find("input[name='name']").val('');
             modal.find('.modal-body .url').val('')
            $(".help-block").hide();
            $(".form-group").removeClass('has-error');
      });

      $("#form").submit(function(e){
            if(status_form){
              var url_link = "{{ route('link-save') }}";
            }else{
              var url_link = "{{ route('link-update') }}";
            }
          e.preventDefault();
          if(isValid()){
                      $.ajax({
                          type: "POST",
                          url: url_link,
                          data: $(this).serialize(),
                          beforeSend: function() {
                            $("#btn-save .fa").removeClass('fa-floppy-o');
                            $("#btn-save .fa").addClass("fa-spinner fa-pulse fa-lg");
                          },success: function (data) {
                            console.log(data);
                                swal(
                                      'Berhasil!',
                                      'link Website Berhasil Di Simpan',
                                      'success'
                                ).then(
                                      function () {
                                          location.reload();
                                      },function (dismiss) {
                                          if (dismiss === 'timer') {
                                              location.reload();
                                          }
                                  }
                                );
                          },error:function(data, status){
                            if(data.status==422){
                                              isValid();
                                        }else{
                              swal(
                                        'Gagal!',
                                        'link Website Gagal Di Simpan',
                                        'error'
                                  ).then(
                                        function () {
                                        },function (dismiss) {
                                            if (dismiss === 'timer') {
                                            }
                                    }
                                  );
                           }
                          },complete: function() {
                            $("#btn-save .fa").removeClass("fa-spinner fa-pulse fa-lg");
                            $("#btn-save .fa").addClass('fa-floppy-o');
                          }
                        });
             }
            return false;
      });

      function isValid(){
            var name = validasi('name');
            var url = validasi('url');

            return (name && url);
      }

      function validasi(elemt){
            if($("."+elemt).val().trim().length<=0){
                    $(".form-"+elemt).addClass('has-error');
                    $(".form-"+elemt+" .help-block").show();
                    return false;
            }else{
                    $(".form-"+elemt).removeClass('has-error');
                    $(".form-"+elemt+" .help-block").hide();
                    return true;
            }
      }

    })
  </script>
@endpush