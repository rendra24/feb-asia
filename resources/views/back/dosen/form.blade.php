<!-- Modal -->
<div class="modal fade" id="modalForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">
		<i class="fa "></i>&nbsp;&nbsp; Form <span class="title"></span> Profil Dosen
        </h4>
      </div>
      <form method="post" action="" id="form-dosen">
      <div class="modal-body">
        	<div class="form-horizontal">
			<div class="form-group form-nama-dosen">
			    	<label for="inputEmail3" class="col-sm-3 control-label">Nama Dosen</label>
			    	<div class="col-sm-9">
			      		<input type="text" class="form-control nama-dosen" name="nama-dosen" placeholder="Nama Dosen">
			      		<input class="id" type="hidden" name="id" value="">
			      		<span  class="help-block" style="display: none;">*Nama Dosen Harus Di Isi</span>
			    	</div>
                          
			</div>
			<div class="row">
				<div class="col-md-6 col-md-offset-3 form-image">
					<div class="well well-image">
		                         <a  data-input="image-input" data-preview="image-dosen" id="choice-image"></a>
		                         <img src="" alt=""  id="image-dosen" class="foto" style="display: none;">
		                         <i class="fa fa-picture-o fa-5x"></i>
		                         <input id="image-input" class="form-control image" type="hidden" name="foto">
		                  </div>
		                  <span  class="help-block" style="display: none;">*Image Harus Di Isi</span>
				</div>
			</div>
		</div>
      </div>
      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;&nbsp;Batal</button>
	        <button type="submit" class="btn btn-primary" id="btn-save"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Simpan</button>
      </div>
      </form>
    </div>
  </div>
</div>

@push('scripts')

	<script type="text/javascript">
		$(document).ready(function(){
			var status_form=true;
			$('#modalForm').on('show.bs.modal', function (event) {
				var button = $(event.relatedTarget);
				var status = button.data('status');
				var modal = $(this);
				if(status=='edit'){
					status_form=false;
					var nama = button.data('nama');
					var foto = button.data('foto');
					var id = button.data('id');
					console.log(nama)
  					modal.find('.modal-body').find("input[name='id']").val(id);
  					modal.find('.modal-body').find("input[name='nama-dosen']").val(nama);
  					modal.find('.modal-body').find(".foto").attr('src', foto);
					modal.find('.modal-title').find(".fa").removeClass("fa-plus");
					modal.find('.modal-title').find(".fa").addClass("fa-pencil-square-o");
					modal.find('.modal-title').find(".title").text("Edit ");
					$(".foto").show();
				}else{
					status_form=true;
					modal.find('.modal-title').find(".fa").removeClass("fa-pencil-square-o");
					modal.find('.modal-title').find(".fa").addClass("fa-plus");
					modal.find('.modal-title').find(".title").text("Tambah ");
				}
				
			});

			$('#modalForm').on('hide.bs.modal', function (event) {
				var modal = $(this);
  				modal.find('.modal-body').find("input[name='id']").val('');
				modal.find('.modal-body').find("input[name='nama-dosen']").val('');
				modal.find('.modal-body').find(".foto").attr('src', '');
				$(".foto").hide();
            			$(".help-block").hide();
            			$(".form-group").removeClass('has-error');
			});
			$("body").on("click", "#choice-image", function(){
	                      localStorage.setItem('target_input', $(this).data('input'));
	                      localStorage.setItem('target_preview', $(this).data('preview'));
	                      var choice = window.open("{{ URL::to('/') }}/laravel-filemanager?type=image", 'FileManager', 'width=900,height=600');
	                    choice.onbeforeunload = function(){
	                        $("#image-dosen").show();
	                        $(".fa-picture-o").hide();
	                    }
	                      return false;
	            });

			$("#form-dosen").submit(function(e){
				if(status_form){
					var url = "{{ route('profil-dosen-save') }}";
				}else{
					var url = "{{ route('profil-dosen-update') }}";
				}
			   	e.preventDefault();
			   	
	                   if(isValid()){
	                        $.ajax({
	                                type: "POST",
	                                url: url,
	                                data: $(this).serialize(),
	                                success: function (data) {
	                                       swal(
	                                          'Berhasil!',
	                                          'Profil Dosen Berhasil Di Simpan',
	                                          'success'
	                                        ).then(
	                                          function () {
	                                             location.reload();
	                                           },
	                                          function (dismiss) {
	                                            if (dismiss === 'timer') {
	                                               location.reload();
	                                            }
	                                          }
	                                      );
	                                }, error:function(data){
	                                      if(data.status==422){
	                                            isValid();
	                                      }else{
	                                           swal(
	                                              'Gagal!',
	                                              'Profil Dosen Gagal Di Simpan',
	                                              'error'
	                                            )
	                                      }
	                                }
	                         });
	                  }
	            });

	            function isValid(){
	                  var nama = validasi('nama-dosen');
	                  if(status_form){
	                  	var foto = validasiFoto();
	                  	return (nama && foto);
	                  }else{
	                  	return nama;
	                  }
	            }

	            function validasi(elemt){
	                  if($("."+elemt).val().trim().length<=0){
	                          $(".form-"+elemt).addClass('has-error');
	                          $(".form-"+elemt+" .help-block").show();
	                          return false;
	                  }else{
	                          $(".form-"+elemt).removeClass('has-error');
	                          $(".form-"+elemt+" .help-block").hide();
	                          return true;
	                  }
	            }
	            
	            function validasiFoto(){
	                   if($("#image-input").val().trim().length<=0){
	                          $(".form-image").addClass('has-error');
	                          $(".form-image .help-block").show();
	                          return false;
	                   }else{
	                          $(".form-image").removeClass('has-error');
	                          $(".form-image .help-block").hide();
	                          return true;
	                  }
	            }

		})
	</script>
@endpush