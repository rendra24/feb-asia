@extends('back.layout.app')
@push('style')
      <link rel="stylesheet" type="text/css" href="{{ URL::to('assets/back/css-page/dosen.css') }}">
@endpush
@section('content')
	<div class="page-header">
		<div class="row">
			<div class="col-sm-10">
				<h1><i class="fa fa-users"></i>&nbsp;&nbsp; Profil  Dosen</h1>
			</div>
			<div class="col-sm-2">
				<button type="button" class="btn btn-white btn-primary btn-bold btn-block" data-toggle="modal" data-target="#modalForm" data-status="add"><i class="fa fa-plus"></i>&nbsp;&nbsp;&nbsp;Add New </button>
			</div>
		</div>
	</div><!-- /.page-header -->

	<div class="row">
		<div class="col-xs-12">
				@if($profil)
					@foreach($profil as $data)
						@if($loop->first)
						<div class="row">
						@endif
						<div class="col-sm-3 text-center box-profil" >
							<img src="{{ URL::to($data->foto) }}" alt="" class="img-responsive" >
							<h5 class="text-center">{{ $data->nama }}</h5>
							<div class="btn-group">
								<button class="btn btn-white btn-info btn-bold btn-sm " data-toggle="modal" data-target="#modalForm" data-id="{{ $data->id }}" data-status="edit" data-foto="{{ URL::to($data->foto) }}"  data-nama="{{ $data->nama }}">
									<i class="fa fa-pencil-square-o"></i> Edit
								</button>
								<button class="btn btn-white btn-danger btn-bold btn-sm btn-delete" dosenId="{{ $data->id }}">
									<i class="fa fa-times"></i> Delete
								</button>
							</div>
						</div>
						@if($loop->last)
							</div>
						@else
							@if($loop->iteration%4==0)
							</div>
							<div class="row">
							@endif
						@endif
					@endforeach
				@endif
			<div class="text-center">
				{{ $profil->links() }}
			</div>
		</div><!-- /.col -->
	</div><!-- /.row -->
	@include('back.dosen.form')
@endsection

@push('scripts')
     <script src="{{ URL::to('/vendor/laravel-filemanager/js/lfm.js') }}"></script>
	<script type="text/javascript">

		$(document).ready(function(){
			      $(".btn-delete").click(function() {
			      		var id_dosen = $(this).attr("dosenId");
			              swal({
			                title: "Apakah Anda Yakin!",
			                type: "error",
			                confirmButtonClass: "btn-danger",
			                confirmButtonText: "Yes!",
			                showCancelButton: true,
			            }).then(function () {
				                $.ajax({
				                    type: "POST",
				                    url: "{{ route('profil-dosen-delete') }}",
				                    data: {id:id_dosen},
				                    success: function (data) {
				                            swal(
				                                'Deleted!',
				                                'Profil Dosen Berhasil Di Hapus',
				                                'success'
				                              ).then(
				                                function () {
				                                   location.reload();
				                                 },
				                                function (dismiss) {
				                                  if (dismiss === 'timer') {
				                                     location.reload();
				                                  }
				                                }
				                              );
				                    }, error(data){
				                          swal(
				                            'Cancelled',
				                            'Profil Dosen Gagal Di Hapus',
				                            'error'
				                          );
				                    }         
				                });
				          }, function (dismiss) {
				        
				          });
			      });
		});
	</script>
@endpush