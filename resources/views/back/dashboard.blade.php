@extends('back.layout.app')

@section('content')
	<div class="row">
		<div class="col-sm-12 text-center">
			<img src="{{ URL::to('assets/static/images/stmik.png') }}" alt="" class="img-responsive" style="height: 175px; margin-left: auto;margin-right: auto; margin-top: 100px;">
			<h1 style="font-weight: 700;    color: #194089;">FEB ASIA </h1>
			<h3 style="margin-top: 10px;   color: #194089;">Fakultas Ekonomi dan Bisnis Asia Malang</h3>
		</div><!-- /.col -->
	</div><!-- /.row -->
@endsection