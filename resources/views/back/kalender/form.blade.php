<!-- Modal -->
<div class="modal fade" id="modalForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">
		<i class="fa fa-plus"></i>&nbsp;&nbsp; <span class="title"></span> Kalender
        </h4>
      </div>
      <form action="" method="post" id="form">
      <div class="modal-body">
        	<div class="form-horizontal">
                  <div class="form-group form-name">
                        <label for="inputEmail3" class="col-sm-3 control-label">Tanggal Awal</label>
                        <div class="col-sm-9">
                               <div class="input-group tanggal-awal" data-wrap="true" data-clickOpens="false">
                                  <input class="  form-control awal" data-input  name="tanggal-awal">
                                  <span class="input-group-btn">
                                    <button class="btn btn-white btn-primary btn-bold" type="button" data-toggle>
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                  </span>
                              </div>
                        </div>
                  </div>
                  <div class="form-group form-name">
                        <label for="inputEmail3" class="col-sm-3 control-label">Tanggal Akhir</label>
                        <div class="col-sm-9">
                               <div class="input-group tanggal-akhir" data-wrap="true" data-clickOpens="false" >
                                  <input class="form-control akhir" data-input  name="tanggal-akhir" >
                                  <span class="input-group-btn">
                                    <button class="btn btn-white btn-primary btn-bold" type="button" data-toggle>
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                  </span>
                              </div>
                        </div>
                  </div>
                   <div class="form-group form-keterangan">
                          <label for="inputEmail3" class="col-sm-3 control-label">Keterangan</label>
                          <div class="col-sm-9">
                                <input class="id" type="hidden" name="id" value="">
                                <textarea name="keterangan" id="" cols="30" rows="3" class="form-control keterangan"></textarea>
                                <span  class="help-block" style="display: none;">*Keterangan Masih Kosong, Harus Di Isi</span>
                          </div>
                   </div>
		</div>
      </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;&nbsp;Batal</button>
          <button type="submit" class="btn btn-primary" id="btn-save"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Simpan</button>
      </div>
      </form>
    </div>
  </div>
</div>

@push('scripts')
  <script type="text/javascript">
    $(document).ready(function(){
      var tanggal_awal = $(".tanggal-awal").flatpickr({ 
                  dateFormat: 'd F Y', 
                  defaultDate: Date.now(),
                  onChange: function(selectedDates, dateStr, instance) {
                           tanggal_akhir.set("minDate", dateStr);
                  }
                });
      var tanggal_akhir = $(".tanggal-akhir").flatpickr({ 
                  dateFormat: 'd F Y', 
                  defaultDate: Date.now(),
                  minDate: $(".awal").val(),

      });
      $("body").on("change", ".awal", function(){
            $(".akhir").val($(this).val());
      });

      function setAkhir(){
          var awal = $(".awal").val();

      }
      var status_form=true;
      $('#modalForm').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var status = button.data('status');
            var modal = $(this);
            if(status=='edit'){
                    status_form=false;
                    var id = button.data('id');
                    var awal = button.data('awal');
                    var akhir = button.data('akhir');
                    var keterangan = button.data('keterangan');
                    console.log(id);
                    tanggal_akhir.set("minDate", awal);
                      modal.find('.modal-body').find("input[name='id']").val(id);
                      modal.find('.modal-body').find("input[name='tanggal-awal']").val(awal);
                      modal.find('.modal-body').find("input[name='tanggal-akhir']").val(akhir);
                      modal.find('.modal-body .keterangan').val(keterangan);
                    modal.find('.modal-title').find(".fa").removeClass("fa-plus");
                    modal.find('.modal-title').find(".fa").addClass("fa-pencil-square-o");
                    modal.find('.modal-title').find(".title").text("Edit ");

            }else{
                    status_form=true;
                    modal.find('.modal-title').find(".fa").removeClass("fa-pencil-square-o");
                    modal.find('.modal-title').find(".fa").addClass("fa-plus");
                    modal.find('.modal-title').find(".title").text("Tambah ");
            }
      });
      


      $('#modalForm').on('hide.bs.modal', function (event) {
             var modal = $(this);
            modal.find('.modal-body').find("input[name='id']").val('');
            modal.find('.modal-body').find("input[name='tanggal-awal']").val(Date.now());
            modal.find('.modal-body').find("input[name='tanggal-akhir']").val(Date.now());
            modal.find('.modal-body .keterangan').val('');
            $(".help-block").hide();
            $(".form-group").removeClass('has-error');
      });

      $("#form").submit(function(e){
            if(status_form){
              var url = "{{ route('kalender-save') }}";
            }else{
              var url = "{{ route('kalender-update') }}";
            }
          e.preventDefault();
          if(isValid()){
                      $.ajax({
                          type: "POST",
                          url: url,
                          data: $(this).serialize(),
                          beforeSend: function() {
                            $("#btn-save .fa").removeClass('fa-floppy-o');
                            $("#btn-save .fa").addClass("fa-spinner fa-pulse fa-lg");
                          },success: function (data) {
                            console.log(data);
                                swal(
                                      'Berhasil!',
                                      'Jadwal Ujian Berhasil Di Simpan',
                                      'success'
                                ).then(
                                      function () {
                                          location.reload();
                                      },function (dismiss) {
                                          if (dismiss === 'timer') {
                                              location.reload();
                                          }
                                  }
                                );
                          },error:function(data, status){
                               if(data.status==422){
                                       isValid();
                                }else{
                                      swal(
                                                'Gagal!',
                                                'Jadwal Ujian Gagal Di Simpan',
                                                'error'
                                          ).then(
                                                function () {
                                                },function (dismiss) {
                                                    if (dismiss === 'timer') {
                                                    }
                                            }
                                          );
                                }
                          },complete: function() {
                            $("#btn-save .fa").removeClass("fa-spinner fa-pulse fa-lg");
                            $("#btn-save .fa").addClass('fa-floppy-o');
                          }
                        });
             }
            return false;
      });

      function isValid(){
            var ket = validasi('keterangan');

            return ket;
      }

      function validasi(elemt){
            if($("."+elemt).val().trim().length<=0){
                    $(".form-"+elemt).addClass('has-error');
                    $(".form-"+elemt+" .help-block").show();
                    return false;
            }else{
                    $(".form-"+elemt).removeClass('has-error');
                    $(".form-"+elemt+" .help-block").hide();
                    return true;
            }
      }

    })
  </script>
@endpush