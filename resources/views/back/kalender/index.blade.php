@extends('back.layout.app')
@push('style')
	<link rel="stylesheet" type="text/css" href="{{ URL::to('assets/back/flatpickr/flatpickr.min.css') }}">
@endpush
@section('content')
	<div class="page-header">
		<div class="row">
			<div class="col-sm-10">
				<h1><i class="fa fa-calendar"></i>&nbsp;&nbsp; Kalender</h1>
			</div>
			<div class="col-sm-2">
				<button type="button" class="btn btn-white btn-primary btn-bold btn-block" data-toggle="modal" data-target="#modalForm" data-status="add"><i class="fa fa-plus"></i>&nbsp;&nbsp;&nbsp;Add New </button>
			</div>
		</div>
	</div><!-- /.page-header -->

	<div class="row">
		<div class="col-xs-12">
			<div class="table-responsive">
		            <table class="table  table-bordered table-hover" style="font-size: 11pt;margin-bottom: 0px;">
		                  <thead>
		                  <tr>
		                      <th class="text-center">No</th>
		                      <th class="text-center">Tanggal Awal</th>
		                  	<th class="text-center">Tanggal Akhir</th>
		                      <th class="text-center" >Keterangan</th>
		                      <th >&nbsp;</th>
		                  </tr>
		                  </thead>
		                  <tbody>
		                      	@foreach($kalender as $key => $data)
				                   <tr>
				                      	<td class="text-center">{{ ($key+1) }}</td>
				                      	<td class="text-center">
				                      		{{ $data->awal }}
				                      	</td>
				                      	<td class="text-center">
				                      		{{ $data->akhir }}
				                      	</td>
				                      	<td>
				                      		{{ $data->keterangan }}
				                      	</td>
				                          <td class="text-center">
				                              <button class="btn btn-white btn-info btn-bold btn-sm" data-toggle="modal" data-target="#modalForm" data-id="{{ $data->id }}" data-status="edit" data-awal="{{ $data->awal }}" data-akhir="{{ $data->akhir }}" data-keterangan="{{ $data->keterangan }}" ><i class="fa fa-pencil"></i> Edit</button>
				                              <button class="btn btn-white btn-danger btn-bold btn-sm btn-delete" kalenderId="{{ $data->id }}"><i class="fa fa-trash-o "></i> Delete</button>
				                          </td>
				                   </tr>
		                      	@endforeach
		                  </tbody>
		            </table>
	            </div>
	            <div class="text-center">
	              	{{ $kalender->links() }}
	            </div>
		</div><!-- /.col -->
	</div><!-- /.row -->
@include('back.kalender.form')
@endsection
@push('scripts')
	<script type="text/javascript" src="{{ URL::to('assets/back/flatpickr/flatpickr.min.js') }}"></script>

	<script type="text/javascript">

		$(document).ready(function(){
			
			
			// tanggal_awal.set("onChange", function(d) {
			// 	// tanggal_akhir.set("minDate", d.fp_incr(1)); //increment by one day/
			// 	console.log("dd");
			// });
			      $(".btn-delete").click(function() {
			      		var id_kalender = $(this).attr("kalenderId");
			              swal({
			                title: "Apakah Anda Yakin!",
			                type: "error",
			                confirmButtonClass: "btn-danger",
			                confirmButtonText: "Yes!",
			                showCancelButton: true,
			            }).then(function () {
				                $.ajax({
				                    type: "POST",
				                    url: "{{ route('kalender-delete') }}",
				                    data: {id:id_kalender},
				                    success: function (data) {
				                            swal(
				                                'Deleted!',
				                                'Data Kalender Berhasil Di Hapus',
				                                'success'
				                              ).then(
				                                function () {
				                                   location.reload();
				                                 },
				                                function (dismiss) {
				                                  if (dismiss === 'timer') {
				                                     location.reload();
				                                  }
				                                }
				                              );
				                           
				                    }, error(data){
				                          swal(
				                            'Cancelled',
				                            'Data Kalender Gagal Di Hapus',
				                            'error'
				                          );
				                    }         
				                });
				          }, function (dismiss) {
				        
				          });
			      });
		});
	</script>
@endpush