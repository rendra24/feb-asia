<!-- Modal -->
<div class="modal fade" id="modalForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">
		<i class="fa "></i>&nbsp;&nbsp; <span class="title"></span> Admin
        </h4>
      </div>
      <form method="post" action=""  id="form-admin">
      <div class="modal-body">
        	<div class="form-horizontal">
			<input class="id" type="hidden" name="id" value="">
			<div class="form-group form-name">
			    	<label for="inputEmail3" class="col-sm-3 control-label">Nama</label>
			    	<div class="col-sm-9">
			      		<input type="text" class="form-control name" name="name" placeholder="Nama">
			      		<span  class="help-block" style="display: none;">* Harus Di Isi</span>
			    	</div>
			</div>
			<div class="form-group form-email">
			    	<label for="inputEmail3" class="col-sm-3 control-label">Email</label>
			    	<div class="col-sm-9">
			      		<input type="text" class="form-control email" name="email" placeholder="Email">
			      		<span  class="help-block" style="display: none;">*Email Harus Di Isi</span>
			    	</div>
			</div>
			<div class="form-group form-password">
			    	<label for="inputEmail3" class="col-sm-3 control-label">Password</label>
			    	<div class="col-sm-9">
			      		<input type="password" class="form-control password" name="password" placeholder="Password">
			      		<span  class="help-block" style="display: none;">*Password Harus Di Isi</span>
			    	</div>
			</div>
			<div class="form-group form-confirm-password">
			    	<label for="inputEmail3" class="col-sm-3 control-label">Confirm Password</label>
			    	<div class="col-sm-9">
			      		<input type="password" class="form-control confirm-password" name="confirm-password" placeholder="Confirm Password">
			      		<span  class="help-block" style="display: none;">*Confirm Password Harus Di Isi</span>
			    	</div>
			</div>
			<div class="form-group">
			    	<label for="inputEmail3" class="col-sm-3 control-label">Hak Akses</label>
			    	<div class="col-sm-9">
			    		
		    			@foreach($roles as $role)
		    				<div class="checkbox">
						    <label>
						      <input type="checkbox" class="hak-akses {{ $role->name }}" name="{{ $role->name }}" > {{ $role->description }}
						    </label>
						</div>
                                @endforeach
			    	</div>
			</div>
		</div>
      </div>
      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;&nbsp;Batal</button>
	        <button type="submit" class="btn btn-primary" id="btn-save"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Simpan</button>
      </div>
      </form>
    </div>
  </div>
</div>
@push('scripts')
	<script type="text/javascript">
		$(document).ready(function(){
			var status_form=true;
			$('#modalForm').on('show.bs.modal', function (event) {
				var button = $(event.relatedTarget);
				var status = button.data('status');
				var modal = $(this);
				if(status=='edit'){
					status_form=false;
					var id = button.data('id');
					var name = button.data('name');
					var email = button.data('email');
					var akses = button.data('akses');
					akses = akses.split(",");

  					modal.find('.modal-body').find("input[name='id']").val(id);
  					modal.find('.modal-body').find("input[name='name']").val(name);
  					modal.find('.modal-body').find("input[name='email']").val(email);
					modal.find('.modal-title').find(".fa").removeClass("fa-plus");
					modal.find('.modal-title').find(".fa").addClass("fa-pencil-square-o");
					modal.find('.modal-title').find(".title").text("Edit ");
					$(akses).each(function(index,item){
						if(item!=''){
							$("."+item).prop('checked', true);
						}
					});

				}else{
					status_form=true;
					modal.find('.modal-title').find(".fa").removeClass("fa-pencil-square-o");
					modal.find('.modal-title').find(".fa").addClass("fa-plus");
					modal.find('.modal-title').find(".title").text("Tambah ");
				}
				
			});

			$('#modalForm').on('hide.bs.modal', function (event) {
				var modal = $(this);
  				modal.find('.modal-body').find("input[name='id']").val('');
				modal.find('.modal-body').find("input[name='tahun-ajaran']").val('');
				modal.find('.modal-body').find("input[name='file-csv']").val('');
  				modal.find('.modal-body').find("option[value='Gasal']").prop('selected',true);
            			$(".help-block").hide();
           			 $(".form-group").removeClass('has-error');
           			 $(".hak-akses").prop('checked', false);
			});

		$("#form-admin").submit(function(e){
			e.preventDefault();
			if(status_form){
				var url =  "{{ route('admin-save') }}";
			}else{
				var url =  "{{ route('admin-update') }}";
			}
			if(isValid()){
				$.ajax({
		                    type: "POST",
		                    url: url,
		                    data: $(this).serialize(),
	                          beforeSend: function() {
	                            $("#btn-save .fa").removeClass('fa-floppy-o');
	                            $("#btn-save .fa").addClass("fa-spinner fa-pulse fa-lg");
	                          },
		                    success: function (data) {
		                          swal(
		                                'Berhasil!',
		                                'Profile Berhasil Di Simpan',
		                                'success'
		                              ).then(
		                                function () {
		                                   location.reload();
		                                 },
		                                function (dismiss) {
		                                  if (dismiss === 'timer') {
		                                     location.reload();
		                                  }
		                                }
		                          );
		                   	},error: function (data) {
		                   		$("#btn-save .fa").removeClass("fa-spinner fa-pulse fa-lg");
		                            $("#btn-save .fa").addClass('fa-floppy-o');
		                            swal(
		                                'Gagal!',
		                                'Profile Gagal Di Simpan',
		                                'error'
		                              ).then(
		                                function () {},
		                                function (dismiss) { if (dismiss === 'timer') {} }
		                              );
						},complete: function() {
		                            $("#btn-save .fa").removeClass("fa-spinner fa-pulse fa-lg");
		                            $("#btn-save .fa").addClass('fa-floppy-o');
		                          }
				});
			}
			return false;
		});
	
		function validasi(elemt){
			if($("."+elemt).val().trim().length<=0){
				$(".form-"+elemt).addClass('has-error');
				$(".form-"+elemt+" .help-block").show();
				return false;
			}else{
				$(".form-"+elemt).removeClass('has-error');
				$(".form-"+elemt+" .help-block").hide();
				return true;
			}
		}

		function validasiPassword(){
			var pass1 = $(".password").val();
			var pass2 = $(".confirm-password").val();

			if(pass2.trim().length<=0){
				$(".form-confirm-password").addClass('has-error');
				$(".form-confirm-password .help-block").text('*Confirm Password Harus Di isi');
				$(".form-confirm-password .help-block").show();
				return false;
			}else{
				if(pass2!=pass1){
					$(".form-confirm-password").addClass('has-error');
					$(".form-confirm-password .help-block").text('*Password Tidak Sama');
					$(".form-confirm-password .help-block").show();
					return false;
				}else{
					$(".form-confirm-password").removeClass('has-error');
					$(".form-confirm-password .help-block").hide();

					return true;
				}
			}		

		}

		function isValid(){
			var name = validasi('name');
			var email = validasi('email');
			if(status_form){
				var password = validasi('password');
				console.log(password);
			}

			if($(".password").val().length>=1){
				var password2 = validasiPassword();
			}
			if(status_form){
				return (name&&email&&password&&password2);
			}
			if($(".password").val().length>=1){
				return (name&&email&&password2);
			}
			return (name&&email);
		}
	});
	</script>
@endpush