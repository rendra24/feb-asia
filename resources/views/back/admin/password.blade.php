@extends('back.layout.app')

@section('content')
	

	<div class="row">
		<div class="col-xs-12 col-sm-6 col-sm-offset-3 widget-container-col ui-sortable" id="widget-container-col-2" style="min-height: 263px;">
			<div class="widget-box widget-color-blue2" id="widget-box-2" style="opacity: 1;">
				<div class="widget-header">
					<h3 class="widget-title bigger lighter">
						<i class="ace-icon fa fa-key"></i>
						Form Ubah Password
					</h3>
				</div>

				<div class="widget-body">
					<div class="widget-main">
						<form action="{{ route('admin-password-update') }}" method="post" id="form-profile">
							<div class="form-group ">
								<label for="">Name :</label>
								<div class="input-group ">
								  	<span class="input-group-addon" id="basic-addon1">
								  		<i class="fa fa-user-o" aria-hidden="true"></i>
								  	</span>
								  	<input type="text" name="name" class="form-control name" placeholder="Name" value="{{ Auth::user()->name }}" readonly="">
								</div>
								<span  class="help-block" style="display: none;">*Name Harus Di isi</span>
							</div>
							<div class="form-group form-email">
								<label for="">Email :</label>
								<div class="input-group ">
								  	<span class="input-group-addon" id="basic-addon1">
								  		<i class="fa fa-envelope-o" aria-hidden="true"></i>
								  	</span>
								  	<input type="email" name="email" class="form-control email" placeholder="Email" value="{{  Auth::user()->email }}" readonly="" >
								</div>
							</div>
							<div class="form-group form-password">
								<label for="">Password: </label>
								<div class="input-group">
								  	<span class="input-group-addon">
								  		<i class="fa fa-key" aria-hidden="true"></i>
								  	</span>
								  	<input type="password" name="password" class="form-control password"  placeholder="Password" >
								</div>
								<span  class="help-block" style="display: none;">*Password Harus Di isi</span>
							</div>
							<div class="form-group form-confirm-password">
								<label for="">Confirm Password: </label>
								<div class="input-group">
								  	<span class="input-group-addon">
								  		<i class="fa fa-key" aria-hidden="true"></i>
								  	</span>
								  	<input type="password" name="password" class="form-control confirm-password"  placeholder="Confirm Password" >
								</div>
								<span  class="help-block" style="display: none;">*Confirm Password Harus Di isi</span>
							</div>
							<div class="row">
								<div class="col-sm-8 col-sm-offset-2">
									<button class="btn btn-white btn-primary btn-bold btn-block" type="submit">
										<i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save Profile
									</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div><!-- /.row -->
@endsection
@push('scripts')
  <script type="text/javascript">
    var url = "{{ route('admin-password-update') }}";
      $("#form-profile").submit(function(e){
	   e.preventDefault();
	   	if(isValid()){
                $.ajax({
                    type: "POST",
                    url: url,
                    data: $(this).serialize(),
                    success: function (data) {
                            swal(
                                'Berhasil!',
                                'Password Berhasil Di Simpan',
                                'success'
                              ).then(
                                function () {
                                   location.reload();
                                 },
                                function (dismiss) {
                                  if (dismiss === 'timer') {
                                     location.reload();
                                  }
                                }
                              );
                   },error: function (data) {
                            swal(
                                'Gagal!',
                                'Password Gagal Di Simpan',
                                'error'
                              ).then(
                                function () {
                                   
                                 },
                                function (dismiss) {
                                  if (dismiss === 'timer') {
                                     
                                  }
                                }
                              );
                   }
                 });
	   	}
	   
	    return false;
	});

      function validasi(elemt){
		if($("."+elemt).val().trim().length<=0){
			$(".form-"+elemt).addClass('has-error');
			$(".form-"+elemt+" .help-block").show();
			return false;
		}else{
			$(".form-"+elemt).removeClass('has-error');
			$(".form-"+elemt+" .help-block").hide();
			return true;
		}
	}

	function validasiPassword(){
		var pass1 = $(".password").val();
		var pass2 = $(".confirm-password").val();

		if(pass2.trim().length<=0){
			$(".form-confirm-password").addClass('has-error');
			$(".form-confirm-password .help-block").text('*Confirm Password Harus Di isi');
			$(".form-confirm-password .help-block").show();
			return false;
		}else{
			if(pass2!=pass1){
				$(".form-confirm-password").addClass('has-error');
				$(".form-confirm-password .help-block").text('*Password Tidak Sama');
				$(".form-confirm-password .help-block").show();
				return false;
			}else{
				$(".form-confirm-password").removeClass('has-error');
				$(".form-confirm-password .help-block").hide();

				return true;
			}
		}		

	}

	function isValid(){
		var password = validasi('password');
		var password2 = validasiPassword();

		return (password&&password2);
	}

  </script>
@endpush