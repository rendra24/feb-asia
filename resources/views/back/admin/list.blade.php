@extends('back.layout.app')

@section('content')
	<div class="page-header">
		<div class="row">
			<div class="col-sm-10">
				<h1><i class="fa fa-users"></i>&nbsp;&nbsp; Hak Akses Admin</h1>
			</div>
			<div class="col-sm-2">
				<button type="button" class="btn btn-white btn-primary btn-bold btn-block" data-toggle="modal" data-target="#modalForm" data-status="add"><i class="fa fa-plus"></i>&nbsp;&nbsp;&nbsp;Add New </button>
			</div>
		</div>
	</div><!-- /.page-header -->

	<div class="row">
		<div class="col-xs-12">
			<div class="table-responsive">
		              <table class="table  table-bordered table-hover" style="font-size: 11pt;">
		                  <thead>
		                  <tr>
		                      	<th class="text-center">No</th>
		                      	<th class="text-center">Nama</th>
		                      	<th class="text-center">Email</th>
		                      	<th class="text-center">Hak Akses</th>
		                      	<th>&nbsp;</th>
		                  </tr>
		                  </thead>
		                  <tbody>
		                      	@foreach($listAdmin as $key => $data)
				                   <tr>
				                      	<td class="text-center">{{ ($key+1) }}</td>
				                      	<td>
				                      		{{ $data->name }}
				                      	</td>
				                      	<td>
				                      		{{ $data->email }}
				                      	</td>
				                      	<td>
				                      		@php
									    $hak_akses='';
									@endphp
				                      		@foreach($data->roles as $no => $role)
				                      			<span class="label arrowed-in-right arrowed {{ ($no%2==1)?'label-success':'label-primary' }} mr-5" data-toggle="tooltip" data-placement="top" title="{{ $role->description }}">
											{{ $role->name }}
											@php
											    $hak_akses.=$role->name.",";
											@endphp
										</span>
				                      		@endforeach
				                      	</td>
				                      	<td class="text-center">
				                      		<button class="btn btn-white btn-success btn-bold btn-sm"  data-toggle="modal" data-target="#modalForm" data-status="edit" data-akses="{{ $hak_akses }}" data-name="{{ $data->name }}" data-email="{{ $data->email }}" data-id="{{ $data->id }}"><i class="fa fa-pencil-square-o"></i>&nbsp; Edit</button>
				                      		<button class="btn btn-white btn-danger btn-bold btn-sm btn-delete" adminId="{{ $data->id }}"><i class="fa fa-trash-o "></i> Delete</button>
				                      	</td>
				                   </tr>
		                      	@endforeach
		                  </tbody>
		              </table>
	              </div>
	             <div class="text-center">
	                   {{ $listAdmin->links() }}
	             </div>
		</div><!-- /.col -->
	</div><!-- /.row -->
	@include('back.admin.form')
@endsection
@push('scripts')
	<script type="text/javascript">

		$(document).ready(function(){
			      $(".btn-delete").click(function() {
			      		var id_admin = $(this).attr("adminId");
			              swal({
			                title: "Apakah Anda Yakin!",
			                type: "error",
			                confirmButtonClass: "btn-danger",
			                confirmButtonText: "Yes!",
			                showCancelButton: true,
			            }).then(function () {
				                $.ajax({
				                    type: "POST",
				                    url: "{{ route('admin-delete') }}",
				                    data: {id:id_admin},
				                    success: function (data) {
				                            swal(
				                                'Deleted!',
				                                'Admin Berhasil Di Hapus',
				                                'success'
				                              ).then(
				                                function () {
				                                   location.reload();
				                                 },
				                                function (dismiss) {
				                                  if (dismiss === 'timer') {
				                                     location.reload();
				                                  }
				                                }
				                              );
				                           
				                    }, error(data){
				                          swal(
				                            'Cancelled',
				                            'Admin Gagal Di Hapus',
				                            'error'
				                          );
				                    }         
				                });
				          }, function (dismiss) {
				        
				          });
			      });
		});
	</script>
@endpush