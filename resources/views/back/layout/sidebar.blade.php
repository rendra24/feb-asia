<div id="sidebar" class="sidebar responsive ace-save-state sidebar-fixed sidebar-scroll" style="padding-top:30px;">
    <script type="text/javascript">
    try {
        ace.settings.loadState('sidebar')
    } catch (e) {}
    </script>


    <ul class="nav nav-list">
        <li class="{{ Request::is('admin')?'active':'' }}">
            <a href="{{ route('admin-index') }}">
                <i class="menu-icon fa fa-tachometer"></i>
                <span class="menu-text"> Dashboard </span>
            </a>
            <b class="arrow"></b>
        </li>
        @if(Auth::user()->super || Auth::user()->hasRole('berita'))
        <li
            class="{{ Request::is('admin/berita')||Request::is('admin/berita/*')||Request::is('admin/berita/*/*')?'active':'' }}">
            <a href="{{ route('berita-index') }}">
                <i class="menu-icon fa fa-newspaper-o"></i>
                <span class="menu-text"> Berita </span>
            </a>
            <b class="arrow"></b>
        </li>
        @endif
        @if(Auth::user()->super || Auth::user()->hasRole('prestasi'))
        <li
            class="{{ Request::is('admin/berita')||Request::is('admin/prestasi/*')||Request::is('admin/prestasi/*/*')?'active':'' }}">
            <a href="{{ route('prestasi-index') }}">
                <i class="menu-icon fa fa-trophy"></i>
                <span class="menu-text"> Prestasi </span>
            </a>
            <b class="arrow"></b>
        </li>
        @endif
        @if(Auth::user()->super || Auth::user()->hasRole('prodi'))
        <li
            class="{{ Request::is('admin/prodi')||Request::is('admin/prodi/*')||Request::is('admin/prodi/*/*')?'active':'' }}">
            <a href="{{ route('prodi-index') }}">
                <i class="menu-icon fa fa-book"></i>
                <span class="menu-text"> Program Studi </span>
            </a>
            <b class="arrow"></b>
        </li>
        @endif
        @if(Auth::user()->super || Auth::user()->hasRole('kalender'))
        <li class="{{ Request::is('admin/kalender')?'active':'' }}">
            <a href="{{ route('kalender-index') }}">
                <i class="menu-icon fa fa-calendar"></i>
                <span class="menu-text"> Kalender </span>
            </a>
            <b class="arrow"></b>
        </li>
        @endif
        @if(Auth::user()->super || Auth::user()->hasRole('perkuliahan'))
        <li class="{{ Request::is('admin/perkuliahan')?'active':'' }}">
            <a href="{{ route('perkuliahan-index') }}">
                <i class="menu-icon fa fa-university"></i>
                <span class="menu-text"> Perkuliahan </span>
            </a>
            <b class="arrow"></b>
        </li>
        @endif
        @if(Auth::user()->super || Auth::user()->hasRole('jam-bimbingan'))
        <li class="{{ Request::is('admin/jam-bimbingan')?'active':'' }}">
            <a href="{{ route('jam-bimbingan-index') }}">
                <i class="menu-icon fa fa-clock-o"></i>
                <span class="menu-text"> Jam Bimbingan </span>
            </a>
            <b class="arrow"></b>
        </li>
        @endif
        @if(Auth::user()->super || Auth::user()->hasRole('ujian'))
        <li class="{{ Request::is('admin/ujian/*')?'active open':'' }}">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-bullhorn"></i>
                <span class="menu-text"> Jadwal Ujian </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">
                <li class="{{ Request::is('admin/ujian/pra-tugas-akhir')?'active':'' }}">
                    <a href="{{ route('ujian-index', 'pra-tugas-akhir') }}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Seminar Proposal
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="{{ Request::is('admin/ujian/tugas-akhir')?'active':'' }}">
                    <a href="{{ route('ujian-index', 'tugas-akhir') }}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Skripsi
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="{{ Request::is('admin/ujian/praktek-kerja-lapanngan')?'active':'' }}">
                    <a href="{{ route('ujian-index', 'praktek-kerja-lapangan') }}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Praktek Kerja Lapangan
                    </a>

                    <b class="arrow"></b>
                </li>
            </ul>
        </li>
        @endif
        @if(Auth::user()->super || Auth::user()->hasRole('profil-stmik') || Auth::user()->hasRole('profil-ti') ||
        Auth::user()->hasRole('profil-sk') || Auth::user()->hasRole('profil-dkv'))
        <li class="{{ Request::is('admin/profil/*')?'active open':'' }}">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-id-card-o"></i>
                <span class="menu-text"> Profil </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">
                @if(Auth::user()->super || Auth::user()->hasRole('profil-stmik'))
                <li class="{{ Request::is('admin/profil/stmik')?'active':'' }}">
                    <a href="{{ route('profil-index', 'stmik') }}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        FTD
                    </a>
                    <b class="arrow"></b>
                </li>
                @endif
                @if(Auth::user()->super || Auth::user()->hasRole('profil-ti'))
                <li class="{{ Request::is('admin/profil/ti')?'active':'' }}">
                    <a href="{{ route('profil-index', 'ti') }}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Teknik Informatika
                    </a>
                    <b class="arrow"></b>
                </li>
                @endif
                @if(Auth::user()->super || Auth::user()->hasRole('profil-sk'))
                <li class="{{ Request::is('admin/profil/sk')?'active':'' }}">
                    <a href="{{ route('profil-index', 'sk') }}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Sistem Komputer
                    </a>
                    <b class="arrow"></b>
                </li>
                @endif
                @if(Auth::user()->super || Auth::user()->hasRole('profil-dkv'))
                <li class="{{ Request::is('admin/profil/dkv')?'active':'' }}">
                    <a href="{{ route('profil-index', 'dkv') }}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Desain Komunikasi Visual
                    </a>
                    <b class="arrow"></b>
                </li>
                @endif
            </ul>
        </li>
        @endif
        @if(Auth::user()->super || Auth::user()->hasRole('visi-misi-stmik') || Auth::user()->hasRole('visi-misi-ti') ||
        Auth::user()->hasRole('visi-misi-sk') || Auth::user()->hasRole('visi-misi-dkv'))
        <li class="{{ Request::is('admin/visi-misi/*')?'active open':'' }}">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-file-text-o"></i>
                <span class="menu-text"> Visi Misi </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">
                @if(Auth::user()->super || Auth::user()->hasRole('visi-misi-stmik'))
                <li class="{{ Request::is('admin/visi-misi/stmik')?'active':'' }}">
                    <a href="{{ route('visi-misi-index', 'stmik') }}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        FTD
                    </a>
                    <b class="arrow"></b>
                </li>
                @endif
                @if(Auth::user()->super || Auth::user()->hasRole('visi-misi-ti'))
                <li class="{{ Request::is('admin/visi-misi/ti')?'active':'' }}">
                    <a href="{{ route('visi-misi-index', 'ti') }}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Teknik Informatika
                    </a>
                    <b class="arrow"></b>
                </li>
                @endif
                @if(Auth::user()->super || Auth::user()->hasRole('visi-misi-sk'))
                <li class="{{ Request::is('admin/visi-misi/sk')?'active':'' }}">
                    <a href="{{ route('visi-misi-index', 'sk') }}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Sistem Komputer
                    </a>
                    <b class="arrow"></b>
                </li>
                @endif
                @if(Auth::user()->super || Auth::user()->hasRole('visi-misi-dkv'))
                <li class="{{ Request::is('admin/visi-misi/dkv')?'active':'' }}">
                    <a href="{{ route('visi-misi-index', 'dkv') }}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Desain Komunikasi Visual
                    </a>
                    <b class="arrow"></b>
                </li>
                @endif
            </ul>
        </li>
        @endif
        @if(Auth::user()->super || Auth::user()->hasRole('pengumuman'))
        <li class="{{ Request::is('admin/sertifikasi')?'active':'' }}">
            <a href="{{ route('sertifikasi') }}">
                <i class="menu-icon fa fa-list"></i>
                <span class="menu-text"> Sertifikasi </span>
            </a>
            <b class="arrow"></b>
        </li>
        @endif
        @if(Auth::user()->super || Auth::user()->hasRole('pengumuman'))
        <li class="{{ Request::is('admin/pengumuman')?'active':'' }}">
            <a href="{{ route('pengumuman-index') }}">
                <i class="menu-icon fa fa-bullhorn"></i>
                <span class="menu-text"> Pengumuman </span>
            </a>
            <b class="arrow"></b>
        </li>
        @endif
        @if(Auth::user()->super || Auth::user()->hasRole('link'))
        <li class="{{ Request::is('admin/link')?'active':'' }}">
            <a href="{{ route('link-index') }}">
                <i class="menu-icon fa fa-link"></i>
                <span class="menu-text"> Link Website </span>
            </a>
            <b class="arrow"></b>
        </li>
        @endif
        @if(Auth::user()->super || Auth::user()->hasRole('unduhan'))
        <li class="{{ Request::is('admin/unduhan')?'active':'' }}">
            <a href="{{ route('unduhan-index') }}">
                <i class="menu-icon fa fa-cloud-download"></i>
                <span class="menu-text"> Unduhan </span>
            </a>
            <b class="arrow"></b>
        </li>
        @endif
        @if(Auth::user()->super || Auth::user()->hasRole('profil-dosen'))
        <li class="{{ Request::is('admin/profil-dosen')?'active':'' }}">
            <a href="{{ route('profil-dosen-index') }}">
                <i class="menu-icon fa fa-users"></i>
                <span class="menu-text"> Profil Dosen </span>
            </a>
            <b class="arrow"></b>
        </li>
        @endif
        @if(Auth::user()->super)
        <li class="{{ Request::is('admin/list')?'active':'' }}">
            <a href="{{ route('admin-list') }}">
                <i class="menu-icon fa fa-users"></i>
                <span class="menu-text"> Daftar Admin </span>
            </a>
            <b class="arrow"></b>
        </li>
        @endif
        <li class="{{ Request::is('admin/password')?'active':'' }}">
            <a href="{{ route('admin-password') }}">
                <i class="menu-icon fa fa-key"></i>
                <span class="menu-text"> Ubah Password </span>
            </a>
            <b class="arrow"></b>
        </li>
        <li class="">
            <a href="{{ route('logout') }}">
                <i class="menu-icon fa fa-power-off"></i>
                <span class="menu-text"> Keluar </span>
            </a>
            <b class="arrow"></b>
        </li>

    </ul><!-- /.nav-list -->

    <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
        <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state"
            data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
    </div>
</div>