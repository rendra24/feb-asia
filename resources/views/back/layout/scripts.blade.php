<!-- basic scripts -->

		<!--[if !IE]> -->
		<script src="{{ URL::to('/assets/back/js/jquery-2.1.4.min.js') }}"></script>
<script src="{{ URL::to('/assets/back/sweetalert2/sweetalert2.min.js') }}"></script>

<script type="text/javascript">
	$.ajaxSetup({
	    headers: {
	        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    }
	});
</script>
		<!-- <![endif]-->

		<!--[if IE]>
<script src="{{ URL::to('/assets/back/js/jquery-1.11.3.min.js') }}"></script>
<![endif]-->
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='{{ URL::to('/assets/back/js/jquery.mobile.custom.min.js') }}'>"+"<"+"/script>");
		</script>
		<script src="{{ URL::to('/assets/back/js/bootstrap.min.js') }}"></script>


		<!-- ace scripts -->
		<script src="{{ URL::to('/assets/back/js/ace-elements.min.js') }}"></script>
		<script src="{{ URL::to('/assets/back/js/ace.min.js') }}"></script>

	


		@stack('scripts')