
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>Admin - FEB ASIA Malang</title>

		<meta name="description" content="responsive photo gallery using colorbox" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<meta name="csrf-token" content="{{ csrf_token() }}">
		
		<link rel="shortcut icon" href="{{ URL::to('/assets/static/images/favicon.png') }}">
		@include('back.layout.stylesheet')
		<style type="text/css">
			.help-block{
				margin-top: 0px;
				margin-bottom: 0px;
				color: #EF5F5A !important;
				font-style: italic;
			}	
		</style>
	</head>

	<body class="no-skin">
		@include('back.layout.navbar')

		<div class="main-container ace-save-state" id="main-container">
			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>
			
			@include('back.layout.sidebar')
			

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
					</div>
					<div class="page-content">

						@yield('content')
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

			@include('back.layout.footer')

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		@include('back.layout.scripts')
	</body>
</html>
