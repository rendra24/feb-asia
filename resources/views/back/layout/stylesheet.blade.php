<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="{{ URL::to('/assets/back/css/bootstrap.min.css') }}" />
		<link rel="stylesheet" href="{{ URL::to('/assets/back/font-awesome//css/font-awesome.min.css') }}" />

		<!-- page specific plugin styles -->
		<link rel="stylesheet" href="{{ URL::to('/assets/back/css/colorbox.min.css') }}" />

		<!-- text fonts -->
		<link rel="stylesheet" href="{{ URL::to('/assets/back/css/fonts.googleapis.com.css') }}" />
		<link rel="stylesheet" href="{{ URL::to('/assets/back/sweetalert2/sweetalert2.min.css') }}" />

		<!-- ace styles -->
		<link rel="stylesheet" href="{{ URL::to('/assets/back/css/ace.min.css') }}" class="ace-main-stylesheet" id="main-ace-style" />

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="{{ URL::to('/assets/back/css/ace-part2.min.css') }}" class="ace-main-stylesheet" />
		<![endif]-->
		<link rel="stylesheet" href="{{ URL::to('/assets/back/css/ace-skins.min.css') }}" />
		<link rel="stylesheet" href="{{ URL::to('/assets/back/css/ace-rtl.min.css') }}" />

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="{{ URL::to('/assets/back/css/ace-ie.min.css') }}" />
		<![endif]-->

		<!-- inline styles related to this page -->

		<!-- ace settings handler -->
		<script src="{{ URL::to('/assets/back/js/ace-extra.min.js') }}"></script>

		<!-- HTML5shiv and Respond.js') }} for IE8 to support HTML5 elements and media queries -->

		<!--[if lte IE 8]>
		<script src="{{ URL::to('/assets/back/js/html5shiv.min.js') }}"></script>
		<script src="{{ URL::to('/assets/back/js/respond.min.js') }}"></script>
		<![endif]-->

		@stack('style')