<div id="navbar" class="navbar navbar-default ace-save-state navbar-fixed-top">
			<div class="navbar-container ace-save-state" id="navbar-container">
				<button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
					<span class="sr-only">Toggle sidebar</span>

					<span class="icon-bar"></span>

					<span class="icon-bar"></span>

					<span class="icon-bar"></span>
				</button>

				<div class="navbar-header pull-left">
					<a href="{{ route('admin-index') }}" class="navbar-brand">
						<small>
							<i class="fa fa-leaf"></i>
							FEB ASIA MALANG
						</small>
					</a>
				</div>

				<div class="navbar-buttons navbar-header pull-right" role="navigation">
					<ul class="nav ace-nav">
						

						<li class="light-blue dropdown-modal">
								<i class="fa fa-user-circle-o fa-2x" style="font-size: 2.5em; margin-top: 7px;"></i>&nbsp;&nbsp;
								<span class="user-info">
									<small>Selamat Datang,</small>
									{{ Auth::user()->name }}
								</span>
						</li>
					</ul>
				</div>
			</div><!-- /.navbar-container -->
		</div>