<!-- Modal -->
<div class="modal fade" id="modalForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">
		<i class="fa "></i>&nbsp;&nbsp; Form <span class="title-form"></span> Unduhan
        </h4>
      </div>
      <form method="post" action="{{ route('unduhan-save') }}" enctype="multipart/form-data" id="form-unduhan">
      <div class="modal-body">
        	<div class="form-horizontal">
			<div class="form-group form-title">
			    	<label for="inputEmail3" class="col-sm-3 control-label">Title</label>
			    	<div class="col-sm-9">
			      		<input type="text" class="form-control title" name="title" placeholder="Title">
			      		<input class="id" type="hidden" name="id" value="">
			      		<span  class="help-block" style="display: none;">*Title Harus Di Isi</span>
			    	</div>
			</div>
			<div class="form-group form-file">
			    	<label for="inputEmail3" class="col-sm-3 control-label">File</label>
			    	<div class="col-sm-9">
			      		<input type="file" class="form-control file"  name="file">
			      		<span  class="help-block" style="display: none;">*File Harus Di Isi</span>
			    	</div>
			</div>
      </div>
      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;&nbsp;Batal</button>
	        <button type="submit" class="btn btn-primary" id="btn-save"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Simpan</button>
      </div>
      </form>
    </div>
  </div>
</div>

@push('scripts')
	<script type="text/javascript">
		$(document).ready(function(){
			var status_form=true;
			$('#modalForm').on('show.bs.modal', function (event) {
				var button = $(event.relatedTarget);
				var status = button.data('status');
				var modal = $(this);
				if(status=='edit'){
					status_form=false;
					var semester = button.data('semester');
					var id = button.data('id');
					var title = button.data('title');
  					modal.find('.modal-body').find("input[name='id']").val(id);
  					modal.find('.modal-body').find("input[name='title']").val(title);
					modal.find('.modal-title').find(".fa").removeClass("fa-plus");
					modal.find('.modal-title').find(".fa").addClass("fa-pencil-square-o");
					modal.find('.modal-title').find(".title-form").text("Edit ");
				}else{
					status_form=true;
					modal.find('.modal-title').find(".fa").removeClass("fa-pencil-square-o");
					modal.find('.modal-title').find(".fa").addClass("fa-plus");
					modal.find('.modal-title').find(".title-form").text("Tambah ");
				}
				
			});

			$('#modalForm').on('hide.bs.modal', function (event) {
				var modal = $(this);
  				modal.find('.modal-body').find("input[name='id']").val('');
				modal.find('.modal-body').find("input[name='title']").val('');
				modal.find('.modal-body').find("input[name='file']").val('');
            			$(".help-block").hide();
            			$(".form-group").removeClass('has-error');
			});

			$("#form-unduhan").submit(function(e){
				if(status_form){
					var url = "{{ route('unduhan-save') }}";
				}else{
					var url = "{{ route('unduhan-update') }}";
				}
			   	e.preventDefault();
			   	if(isValid()){
				   	var formData = new FormData();
				   	formData.append('id', $(".id").val());
				   	formData.append('title', $(".title").val());
				   	formData.append('file', $('input[type=file]')[0].files[0]); 
			                $.ajax({
			                    type: "POST",
			                    url: url,
	            			 processData: false,
	           				 contentType: false,
			                    data: formData,
			                    beforeSend: function() {
			                    	$("#btn-save .fa").removeClass('fa-floppy-o');
			                    	$("#btn-save .fa").addClass("fa-spinner fa-pulse fa-lg");
			                    },success: function (data) {
			                    	console.log(data);
			                          swal(
			                                'Berhasil!',
			                                'Data unduhan Berhasil Di Simpan',
			                                'success'
			                          ).then(
			                                function () {
			                                   	location.reload();
			                                },function (dismiss) {
				                                  if (dismiss === 'timer') {
				                                     	location.reload();
				                                  }
			                          	}
			                          );
			                    },error:function(data, status){
			                    	if(data.status==422){
	                                            isValid();
	                                      }else{
			                    		swal(
				                                'Gagal!',
				                                'Data unduhan Gagal Di Simpan',
				                                'error'
				                          ).then(
				                                function () {
				                                },function (dismiss) {
					                                  if (dismiss === 'timer') {
					                                  }
				                          	}
				                          );
				                   }
			                    },complete: function() {
						       $("#btn-save .fa").removeClass("fa-spinner fa-pulse fa-lg");
			                    	$("#btn-save .fa").addClass('fa-floppy-o');
						}
			                  });
			       }
			    	return false;
			});

			function isValid(){

	                  var title = validasi('title');
	                  if(status_form){
					var file = validasi('file');
	                  	return (title && file);
				}else{
					return title;
				}
	                  
	            }

	            function validasi(elemt){
	                  if($("."+elemt).val().trim().length<=0){
	                          $(".form-"+elemt).addClass('has-error');
	                          $(".form-"+elemt+" .help-block").show();
	                          return false;
	                  }else{
	                          $(".form-"+elemt).removeClass('has-error');
	                          $(".form-"+elemt+" .help-block").hide();
	                          return true;
	                  }
	            }
		})
	</script>
@endpush