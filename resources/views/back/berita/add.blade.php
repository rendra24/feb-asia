@extends('back.layout.app')
@push('style')
      <link rel="stylesheet" type="text/css" href="{{ URL::to('assets/back/css-page/berita.css') }}">
@endpush
@section('content')
	<div class="page-header">
		<div class="row">
			<div class="col-sm-8">
				<h1><i class="fa fa-newspaper-o"></i>&nbsp;&nbsp;Tambah Berita Baru</h1>
			</div>
			<div class="col-sm-4 text-right">
				<div class="btn-group" role="group" aria-label="...">
                              <button type="button" class="btn btn-white btn-primary btn-bold btn-publish"><i class="fa fa-paper-plane-o"></i>&nbsp;Publish</button>
                              <button type="button" class="btn btn-white btn-success btn-bold btn-save"><i class="fa fa-floppy-o"></i>&nbsp;Simpan</button>
                              <a href="{{ route('berita-index') }}" class="btn btn-white btn-danger btn-bold"><i class="fa fa-times"></i>&nbsp;Batal</a>
                          </div>
			</div>
		</div>
	</div><!-- /.page-header -->

	<div class="row">
            <div class="col-sm-8">
                  <div class="form-group  form-title">
                        <label for="">Titile</label>
                        <input type="text" class="form-control title" placeholder="Masukkan Title" value="{{ old('title') }}">
                        <span  class="help-block" style="display: none;">*Title Harus Di Isi</span>
                  </div>
                  <div class="form-group form-content">
                        <label for="">Content</label>
                        <span  class="help-block" style="display: none;">*Content Harus Di Isi</span>
                        <textarea name="" id="content" cols="30" rows="15" class="form-control">{!! old('content') !!}</textarea>
                  </div>
            </div>
            <div class="col-sm-4 form-image">
                  <div class="well well-image">
                         <a  data-input="image-input" data-preview="image-berita" id="choice-image"></a>
                         <img src="" alt=""  id="image-berita"  style="display: none;">
                         <i class="fa fa-picture-o fa-5x"></i>
                         <input id="image-input" class="form-control image" type="hidden" name="image">
                  </div>
                  <span  class="help-block" style="display: none;">*Image Harus Di Isi</span>
            </div>
      </div>
@endsection
@push('scripts')  
    <script src="{{ URL::to('/assets/back/tinymce/tinymce.min.js') }}"></script>
     <script src="{{ URL::to('/vendor/laravel-filemanager/js/lfm.js') }}"></script>
    <script>
             var editor_config = {
              path_absolute : "{{ URL::to('/') }}/",
              height: 500,
              selector: "textarea#content",
              plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern"
              ],
              toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
              relative_urls: false,
              file_browser_callback : function(field_name, url, type, win) {
                var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

                var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                if (type == 'image') {
                  cmsURL = cmsURL + "&type=Images";
                } else {
                  cmsURL = cmsURL + "&type=Files";
                }

                tinyMCE.activeEditor.windowManager.open({
                  file : cmsURL,
                  title : 'Filemanager',
                  width : x * 0.8,
                  height : y * 0.8,
                  resizable : "yes",
                  close_previous : "no"
                });
              }
            };

            tinymce.init(editor_config);
      </script>
      <script type="text/javascript">
              $("body").on("click", "#choice-image", function(){
                      localStorage.setItem('target_input', $(this).data('input'));
                      localStorage.setItem('target_preview', $(this).data('preview'));
                      var choice = window.open("{{ URL::to('/') }}/laravel-filemanager?type=image", 'FileManager', 'width=900,height=600');
                    choice.onbeforeunload = function(){
                        if($("#image-input").val().trim().length>0){
                          $("#image-berita").show();
                          $(".fa-picture-o").hide();
                        }
                    }
                      return false;
            });

              $("body").on("click", ".btn-publish", function(){
                    save(1);
              });
              $("body").on("click", ".btn-save", function(){
                    save();
              });

              function save(status=0){
                   if(isValid()){
                        var title = $(".title").val();
                        var content = tinymce.get("content").getContent();
                        var image = $("#image-input").val();
                        $.ajax({
                                type: "POST",
                                url: "{{ route('berita-save') }}",
                                data: {title: title, image: image, content: content, status: status},
                                success: function (data) {
                                       swal(
                                          'Berhasil!',
                                          'Berita Berhasil Di Simpan',
                                          'success'
                                        ).then(
                                          function () {
                                             window.location = "{{ route('berita-index') }}";
                                           },
                                          function (dismiss) {
                                            if (dismiss === 'timer') {
                                               window.location = "{{ route('berita-index') }}";
                                            }
                                          }
                                      );
                                }, error:function(data){
                                      if(data.status==422){
                                            isValid();
                                      }else{
                                           swal(
                                              'Gagal!',
                                              'Berita Gagal Di Simpan',
                                              'error'
                                            )
                                      }
                                }
                         });
                  }
            }

            function isValid(){
                  var title = validasi('title');
                  var content = validasiContent();
                  var image = validasiImage();
                  return (title && content && image);
            }

            function validasi(elemt){
                  if($("."+elemt).val().trim().length<=0){
                          $(".form-"+elemt).addClass('has-error');
                          $(".form-"+elemt+" .help-block").show();
                          return false;
                  }else{
                          $(".form-"+elemt).removeClass('has-error');
                          $(".form-"+elemt+" .help-block").hide();
                          return true;
                  }
            }
            
            function validasiImage(){
                   if($("#image-input").val().trim().length<=0){
                          $(".form-image").addClass('has-error');
                          $(".form-image .help-block").show();
                          return false;
                   }else{
                          $(".form-image").removeClass('has-error');
                          $(".form-image .help-block").hide();
                          return true;
                  }
            }
            function validasiContent(){
                  var content = tinymce.get("content").getContent();
                   if(content.trim().length<=0){
                          $(".form-content").addClass('has-error');
                          $(".form-content .help-block").show();
                          return false;
                   }else{
                          $(".form-content").removeClass('has-error');
                          $(".form-content .help-block").hide();
                          return true;
                  }
            }


      </script>
@endpush