@extends('back.layout.app')
@section('content')
	<div class="page-header">
		<div class="row">
			<div class="col-sm-8">
				<h1><i class="fa fa-bullhorn"></i>&nbsp;&nbsp;Edit Pengumuman </h1>
			</div>
			<div class="col-sm-4 text-right">
				<div class="btn-group" role="group" aria-label="...">
                              <button type="button" class="btn btn-white btn-primary btn-bold btn-save"><i class="fa fa-floppy-o"></i>&nbsp;Simpan</button>
                              <a href="{{ route('pengumuman-index') }}" class="btn btn-white btn-danger btn-bold"><i class="fa fa-times"></i>&nbsp;Batal</a>
                          </div>
			</div>
		</div>
	</div><!-- /.page-header -->

	<div class="row">
            <div class="col-sm-12">
                  <div class="form-group  form-title">
                        <label for="">Titile</label>
                        <input type="text" class="form-control title" placeholder="Masukkan Title" value="{{ old('title', $pengumuman->title) }}">
                        <span  class="help-block" style="display: none;">*Title Harus Di Isi</span>
                  </div>
                  <div class="form-group form-content">
                        <label for="">Content</label>
                        <span  class="help-block" style="display: none;">*Content Harus Di Isi</span>
                        <textarea name="" id="content" cols="30" rows="15" class="form-control">{!! old('content', $pengumuman->content) !!}</textarea>
                  </div>
            </div>
      </div>
@endsection

@push('scripts')
    <script src="{{ URL::to('/assets/back/tinymce/tinymce.min.js') }}"></script>
     <script src="{{ URL::to('/vendor/laravel-filemanager/js/lfm.js') }}"></script>
    <script>
             var editor_config = {
              path_absolute : "{{ URL::to('/') }}/",
              height: 500,
              selector: "textarea#content",
              plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern"
              ],
              toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
              relative_urls: false,
              file_browser_callback : function(field_name, url, type, win) {
                var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

                var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                if (type == 'image') {
                  cmsURL = cmsURL + "&type=Images";
                } else {
                  cmsURL = cmsURL + "&type=Files";
                }

                tinyMCE.activeEditor.windowManager.open({
                  file : cmsURL,
                  title : 'Filemanager',
                  width : x * 0.8,
                  height : y * 0.8,
                  resizable : "yes",
                  close_previous : "no"
                });
              }
            };

            tinymce.init(editor_config);
      </script>
      <script type="text/javascript">
            $("body").on("click", ".btn-save", function(){
                   if(isValid()){
                        var title = $(".title").val();
                        var content = tinymce.get("content").getContent();
                        $.ajax({
                                type: "POST",
                                url: "{{ route('pengumuman-update', $pengumuman->id) }}",
                                data: {title: title, content: content},
                                success: function (data) {
                                       swal(
                                          'Berhasil!',
                                          'Pengumuman Berhasil Di Simpan',
                                          'success'
                                        ).then(
                                          function () {
                                             window.location = "{{ route('pengumuman-index') }}";
                                           },
                                          function (dismiss) {
                                            if (dismiss === 'timer') {
                                               window.location = "{{ route('pengumuman-index') }}";
                                            }
                                          }
                                      );
                                }, error:function(data){
                                      if(data.status==422){
                                            isValid();
                                      }else{
                                           swal(
                                              'Gagal!',
                                              'Pengumuman Gagal Di Simpan',
                                              'error'
                                            )
                                      }
                                }
                         });
                  }
            });

            function isValid(){
                  var title = validasi('title');
                  var content = validasiContent();
                  return (title && content);
            }

            function validasi(elemt){
                  if($("."+elemt).val().trim().length<=0){
                          $(".form-"+elemt).addClass('has-error');
                          $(".form-"+elemt+" .help-block").show();
                          return false;
                  }else{
                          $(".form-"+elemt).removeClass('has-error');
                          $(".form-"+elemt+" .help-block").hide();
                          return true;
                  }
            }
            
            function validasiContent(){
                  var content = tinymce.get("content").getContent();
                   if(content.trim().length<=0){
                          $(".form-content").addClass('has-error');
                          $(".form-content .help-block").show();
                          return false;
                   }else{
                          $(".form-content").removeClass('has-error');
                          $(".form-content .help-block").hide();
                          return true;
                  }
            }


      </script>
@endpush