@extends('back.layout.app')
@push('style')
      <link rel="stylesheet" type="text/css" href="{{ URL::to('assets/back/css-page/pengumuman.css') }}">
@endpush
@section('content')
	<div class="page-header">
		<div class="row">
			<div class="col-sm-10">
				<h1><i class="fa fa-bullhorn"></i>&nbsp;&nbsp; Pengumuman</h1>
			</div>
			<div class="col-sm-2">
				<a href="{{ route('pengumuman-add') }}" class="btn btn-white btn-info btn-bold btn-block" type="button">
					<i class="ace-icon fa fa-plus bigger-110"></i>
					Add New
				</a>
			</div>
		</div>
	</div><!-- /.page-header -->

	<div class="row">
		<div class="col-xs-12">
			@if($pengumuman)
				@foreach($pengumuman as $data)
					<div class="row box-pengumuman">
						<div class="col-sm-10">
							<h4 class="blue">{{ $data->title }}</h4>
							<h6 class="red">{{ $data->user->name }} | {{ $data->created_at }}</h6>
							<div class="content">
								{!! $data->body !!}
							</div>
						</div>
						<div class="col-sm-2">
							<a href="{{ route('pengumuman-edit', $data->id) }}" class="btn btn-white btn-success btn-bold btn-sm">
		                                    <i class="fa fa-pencil-square-o"></i>&nbsp; Edit 
		                              </a>
		                              <button type="button" class="btn btn-white btn-danger btn-bold btn-sm btn-delete"  pengumumanId="{{ $data->id }}">
		                                    <i class="fa fa-trash-o"></i>&nbsp; Delete 
		                              </button>
						</div>
					</div>
					<div class="line"></div>
				@endforeach
			@endif
	              
	              <div class="text-center">
	              		{{ $pengumuman->links() }}
	              </div>
		</div><!-- /.col -->
	</div><!-- /.row -->
@include('back.unduhan.form')
@endsection
@push('scripts')
	<script type="text/javascript">

		$(document).ready(function(){
			      $(".btn-delete").click(function() {
			      		var id_pengumuman = $(this).attr("pengumumanId");
			              swal({
			                title: "Apakah Anda Yakin!",
			                type: "error",
			                confirmButtonClass: "btn-danger",
			                confirmButtonText: "Yes!",
			                showCancelButton: true,
			            }).then(function () {
				                $.ajax({
				                    type: "POST",
				                    url: "{{ route('pengumuman-delete') }}",
				                    data: {id:id_pengumuman},
				                    success: function (data) {
				                            swal(
				                                'Deleted!',
				                                'Data Unduhan Berhasil Di Hapus',
				                                'success'
				                              ).then(
				                                function () {
				                                   location.reload();
				                                 },
				                                function (dismiss) {
				                                  if (dismiss === 'timer') {
				                                     location.reload();
				                                  }
				                                }
				                              );
				                           
				                    }, error(data){
				                          swal(
				                            'Cancelled',
				                            'Data Unduhan Gagal Di Hapus',
				                            'error'
				                          );
				                    }         
				                });
				          }, function (dismiss) {
				        
				          });
			      });
		});
	</script>
@endpush