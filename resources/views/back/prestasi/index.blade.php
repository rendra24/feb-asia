@extends('back.layout.app')

@section('content')
<div class="page-header">
    <div class="row">
        <div class="col-sm-10">
            <h1><i class="fa fa-newspaper-o"></i>&nbsp;&nbsp; Prestasi</h1>
        </div>
        <div class="col-sm-2">
            <a href="{{ route('prestasi-add') }}" class="btn btn-white btn-primary btn-bold btn-block"><i
                    class="fa fa-plus"></i>&nbsp;&nbsp;&nbsp;Add New </a>
        </div>
    </div>
</div><!-- /.page-header -->

<div class="row">
    <div class="col-xs-12">
        <div class="padding-15">
            @if($prestasi)
            @foreach($prestasi as $data)
            <article class="media">
                <a class="pull-left">
                    <img src="{{ URL::to($data->image) }}" style="width: 175px;height: 125px;">
                </a>
                <div class="media-body">
                    <h4 style="margin-top: 0px;"><a class=" p-head"
                            href="{{ route('prestasi-edit', $data->id) }}">{{ $data->title }}</a></h4>
                    <p class="p-date">{{ $data->user->name }} |
                        @if($data->status)
                        {{ $data->publishing }}
                        <span class="label label-primary arrowed-in-right arrowed label-sm ml-10">Publish</span>
                        @else
                        {{ $data->created }}
                        <span class="label label-success arrowed-in-right arrowed label-sm ml-10">Seved</span>
                        @endif
                    </p>
                    <p>{!! $data->body !!}</p>
                    <div class="text-right">
                        <div class="btn-group" role="group">
                            @if(!$data->status)
                            <button type="button" idB="{{ $data->id }}"
                                class="btn btn-white btn-primary btn-bold btn-sm btn-publish">
                                <i class="fa fa-paper-plane-o"></i>&nbsp; Publish
                            </button>
                            @endif
                            <a href="{{ route('prestasi-edit', $data->id) }}"
                                class="btn btn-white btn-success btn-bold btn-sm">
                                <i class="fa fa-pencil-square-o"></i>&nbsp; Edit
                            </a>
                            <button type="button" class="btn btn-white btn-danger btn-bold btn-sm btn-delete"
                                idB="{{ $data->id }}">
                                <i class="fa fa-trash-o"></i>&nbsp; Delete
                            </button>
                        </div>
                    </div>
                </div>
            </article>
            @endforeach
            @endif
        </div>
        <div class="text-center">
            {{ $prestasi->links() }}
        </div>
    </div><!-- /.col -->
</div><!-- /.row -->
@endsection
@push('scripts')
<script type="text/javascript">
$(document).ready(function() {
    $("body").on("click", ".btn-publish", function() {
        var idB = $(this).attr('idB');
        swal({
            title: "Apakah Anda Yakin Publish Prestasi Ini!",
            type: "info",
            confirmButtonClass: "btn-info",
            confirmButtonText: "Yes!",
            showCancelButton: true,
        }).then(function() {
            $.ajax({
                type: "POST",
                url: "{{ route('prestasi-publish') }}",
                data: {
                    id: idB
                },
                success: function(data) {
                    swal(
                        'Berhasil!',
                        'Prestasi Berhasil Di Publish',
                        'success'
                    ).then(function() {
                        location.reload();
                    }, function(dismiss) {
                        if (dismiss === 'timer') {
                            location.reload();
                        }
                    });
                },
                error(data) {
                    swal(
                        'Gagal',
                        'Prestasi Gagal Di Publish',
                        'error'
                    );
                }
            });
        }, function(dismiss) {});
    });
    $("body").on("click", ".btn-delete", function() {
        var idB = $(this).attr('idB');
        swal({
            title: "Apakah Anda Yakin Menghapus!",
            type: "warning",
            confirmButtonClass: "btn-info",
            confirmButtonText: "Yes!",
            showCancelButton: true,
        }).then(function() {
            $.ajax({
                type: "POST",
                url: "{{ route('prestasi-delete') }}",
                data: {
                    id: idB
                },
                success: function(data) {
                    swal(
                        'Berhasil!',
                        'Prestasi Berhasil Di Hapus',
                        'success'
                    ).then(function() {
                        location.reload();
                    }, function(dismiss) {
                        if (dismiss === 'timer') {
                            location.reload();
                        }
                    });
                },
                error(data) {
                    swal(
                        'Gagal',
                        'Prestasi Gagal Di Hapus',
                        'error'
                    );
                }
            });
        }, function(dismiss) {});
    });
});
</script>
@endpush