@extends('back.layout.app')

@section('content')
	<div class="page-header">
		<div class="row">
			<div class="col-sm-8">
				<h1><i class="fa fa-clock-o"></i>&nbsp;&nbsp; Jam Bimbingan</h1>
			</div>
                    <div class="col-sm-2">
				<a href="{{ route('jam-bimbingan-download') }}" class="btn btn-white btn-success btn-bold btn-block">
					<i class="fa fa-download"></i>&nbsp;&nbsp;&nbsp; Format File
				</a>
			</div>
                  <div class="col-sm-2" style="padding-left: 0px;">
                        <button type="button" class="btn btn-white btn-primary btn-bold btn-block" data-toggle="modal" data-target="#modalForm" data-status="add"><i class="fa fa-plus"></i>&nbsp;&nbsp;&nbsp;Add New </button>
                    </div>
            </div>
	</div><!-- /.page-header -->

	<div class="row">
		<div class="col-xs-12">
			<div class="table-responsive">
		              <table class="table  table-bordered table-hover" >
		                  <thead>
			                  <tr>
			                      <th class="text-center">No</th>
			                      <th>Title</th>
			                      <th>&nbsp;</th>
			                  </tr>
		                  </thead>
		                  <tbody>
		                      	@foreach($jamBimbingan as $key => $data)
				                   <tr>
				                      	<td class="text-center">{{ ($key+1) }}</td>
				                      	<td>
				                      		Jam Bimbingan Semester {{ $data->semester }} Ta. {{ $data->tahun_ajaran }}
				                      	</td>
				                          <td class="text-center">
				                              <a href="{{ route('jam-bimbingan-detail-index', $data->id) }}" class="btn btn-white btn-success btn-bold btn-sm"><i class="fa fa-eye "></i> Detail</a>
				                              <button class="btn btn-white btn-info btn-bold btn-sm" data-toggle="modal" data-target="#modalForm" data-id="{{ $data->id }}" data-status="edit" data-semester="{{ $data->semester }}" data-tahun-ajaran="{{ $data->tahun_ajaran }}"><i class="fa fa-pencil"></i> Edit</button>
				                              <button class="btn btn-white btn-danger btn-bold btn-sm btn-delete" jamBimbinganID="{{ $data->id }}"><i class="fa fa-trash-o "></i> Delete</button>
				                          </td>
				                   </tr>
		                      	@endforeach
		                  </tbody>
		              </table>
	              </div>
	            <div class="text-center">
	            	{{ $jamBimbingan->links() }}
	            </div>
		</div><!-- /.col -->
	</div><!-- /.row -->
@include('back.jamBimbingan.form')
@endsection
@push('scripts')
	<script type="text/javascript">

		$(document).ready(function(){
			// $('#modalForm').modal({backdrop: 'static', keyboard: false});
			// $('#modalForm').modal({backdrop: 'static', keyboard: false})；
			      $(".btn-delete").click(function() {
			      		var id_jamBimbingan = $(this).attr("jamBimbinganID");
			              swal({
			                title: "Apakah Anda Yakin!",
			                type: "error",
			                confirmButtonClass: "btn-danger",
			                confirmButtonText: "Yes!",
			                showCancelButton: true,
			            }).then(function () {
			              	
			              	console.log(id_jamBimbingan);
				                $.ajax({
				                    type: "POST",
				                    url: "{{ route('jam-bimbingan-delete') }}",
				                    data: {id:id_jamBimbingan},
				                    success: function (data) {
				                            swal(
				                                'Deleted!',
				                                'Jam Bimbingan Berhasil Di Hapus',
				                                'success'
				                              ).then(
				                                function () {
				                                   location.reload();
				                                 },
				                                function (dismiss) {
				                                  if (dismiss === 'timer') {
				                                     location.reload();
				                                  }
				                                }
				                              );
				                           
				                    }, error(data){
				                          swal(
				                            'Cancelled',
				                            'Jam Bimbingan Gagal Di Hapus',
				                            'error'
				                          );
				                    }         
				                });
				          }, function (dismiss) {
				        
				          });
			      });
		});
	</script>
@endpush