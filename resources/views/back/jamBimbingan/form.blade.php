<!-- Modal -->
<div class="modal fade" id="modalForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">
		<i class="fa "></i>&nbsp;&nbsp; Form <span class="title"></span> Jadwal Perkuliahan
        </h4>
      </div>
      <form method="post" action="{{ route('jam-bimbingan-save') }}" enctype="multipart/form-data" id="form-perkuliahan">
      <div class="modal-body">
        	<div class="form-horizontal">
			 <input class="id" type="hidden" name="id" value="">
			<div class="form-group form-semester">
			    	<label for="inputEmail3" class="col-sm-3 control-label">Semester</label>
			    	<div class="col-sm-9">
			      		<select name="semester" id="" class="form-control semester">
			      			<option value="Gasal">Gasal</option>
			      			<option value="Genap">Genap</option>
			      		</select>
			    	</div>
			</div>
			<div class="form-group form-tahun-ajaran">
			    	<label for="inputEmail3" class="col-sm-3 control-label">Tahun Ajaran</label>
			    	<div class="col-sm-9">
			      		<input type="text" class="form-control tahun-ajaran" name="tahun-ajaran" placeholder="Tahun Ajaran">
			      		<span  class="help-block" style="display: none;">*Tahun Ajaran Harus Di Isi</span>
			    	</div>
                          
			</div>
			<div class="form-group">
			    	<label for="inputEmail3" class="col-sm-3 control-label">Import Data</label>
			    	<div class="col-sm-9">
			      		<input type="file" class="form-control file-csv"  name="file-csv">
			    	</div>
			</div>
      </div>
      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;&nbsp;Batal</button>
	        <button type="submit" class="btn btn-primary" id="btn-save"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Simpan</button>
      </div>
      </form>
    </div>
  </div>
</div>

@push('scripts')
	<script type="text/javascript">
		$(document).ready(function(){
			var status_form=true;
			$('#modalForm').on('show.bs.modal', function (event) {
				var button = $(event.relatedTarget);
				var status = button.data('status');
				var modal = $(this);
				if(status=='edit'){
					status_form=false;
					var semester = button.data('semester');
					var id = button.data('id');
					var tahun_ajaran = button.data('tahun-ajaran');
  					modal.find('.modal-body').find("input[name='id']").val(id);
  					modal.find('.modal-body').find("input[name='tahun-ajaran']").val(tahun_ajaran);
  					modal.find('.modal-body').find("option[value='"+semester+"']").prop('selected',true);
					modal.find('.modal-title').find(".fa").removeClass("fa-plus");
					modal.find('.modal-title').find(".fa").addClass("fa-pencil-square-o");
					modal.find('.modal-title').find(".title").text("Edit ");
				}else{
					status_form=true;
					modal.find('.modal-title').find(".fa").removeClass("fa-pencil-square-o");
					modal.find('.modal-title').find(".fa").addClass("fa-plus");
					modal.find('.modal-title').find(".title").text("Tambah ");
				}
				
			});

			$('#modalForm').on('hide.bs.modal', function (event) {
				var modal = $(this);
  				modal.find('.modal-body').find("input[name='id']").val('');
				modal.find('.modal-body').find("input[name='tahun-ajaran']").val('');
				modal.find('.modal-body').find("input[name='file-csv']").val('');
  				modal.find('.modal-body').find("option[value='Gasal']").prop('selected',true);
            			$(".help-block").hide();
            			$(".form-group").removeClass('has-error');
			});

			$("#form-perkuliahan").submit(function(e){
				if(status_form){
					var url = "{{ route('jam-bimbingan-save') }}";
				}else{
					var url = "{{ route('jam-bimbingan-update') }}";
				}
			   	e.preventDefault();
			   	if(isValid()){
				   	var formData = new FormData();
				   	formData.append('id', $(".id").val());
				   	formData.append('semester', $(".semester").val());
				   	formData.append('tahun_ajaran', $(".tahun-ajaran").val());
				   	formData.append('file-csv', $('input[type=file]')[0].files[0]); 
			                $.ajax({
			                    type: "POST",
			                    url: url,
	            				processData: false,
	           				 contentType: false,
			                    data: formData,
			                    beforeSend: function() {
			                    	$("#btn-save .fa").removeClass('fa-floppy-o');
			                    	$("#btn-save .fa").addClass("fa-spinner fa-pulse fa-lg");
			                    },success: function (data) {
			                    	console.log(data);
			                          swal(
			                                'Berhasil!',
			                                'Jadwal Perkuliahan Berhasil Di Simpan',
			                                'success'
			                          ).then(
			                                function () {
			                                   	location.reload();
			                                },function (dismiss) {
				                                  if (dismiss === 'timer') {
				                                     	location.reload();
				                                  }
			                          	}
			                          );
			                    },error:function(data, status){
			                    	if(data.status==422){
	                                            isValid();
	                                      }else{
			                    		swal(
				                                'Gagal!',
				                                'Jadwal Perkuliahan Gagal Di Simpan',
				                                'error'
				                          ).then(
				                                function () {
				                                },function (dismiss) {
					                                  if (dismiss === 'timer') {
					                                  }
				                          	}
				                          );
				                   }
			                    },complete: function() {
						       $("#btn-save .fa").removeClass("fa-spinner fa-pulse fa-lg");
			                    	$("#btn-save .fa").addClass('fa-floppy-o');
						}
			                  });
			       }
			    	return false;
			});

			function isValid(){
				if($(".tahun-ajaran").val().trim().length<=0){
	                          $(".form-tahun-ajaran").addClass('has-error');
	                          $(".form-tahun-ajaran .help-block").show();
	                          return false;
	                  }else{
	                          $(".form-tahun-ajaran").removeClass('has-error');
	                          $(".form-tahun-ajaran .help-block").hide();
	                          return true;
	                  }
			}

		})
	</script>
@endpush