@extends('back.layout.app')

@section('content')
	<div class="page-header">
		<div class="row">
			<div class="col-sm-9">
                        <h1><i class="fa fa-newspaper-o"></i>&nbsp;&nbsp; Edit Visi & Misi{{ ($data->name!='stmik'? ' Program Studi ' : '') }} {{ $data->description }}</h1>
                  </div>
                  <div class="col-sm-3 text-right">
                      <div class="btn-group" role="group" aria-label="...">
                          <a href="{{ route('visi-misi-index', $data->name) }}" class="btn btn-white btn-warning  btn-bold" style="margin-right: 5px;">
                            <i class="fa fa-angle-double-left"></i>&nbsp;&nbsp; Back
                          </a>
                          <button type="button" class="btn btn-white btn-primary btn-bold" id="btn-save"> <i class="fa fa-floppy-o"></i>&nbsp;&nbsp;&nbsp;Save Change</button>
                        </div>
                  </div>
		</div>
	</div><!-- /.page-header -->

	<div class="row">
		<div class="col-xs-12">
			<span  class="help-block" style="display: none;">*Content Masih Kosong, Harus Di Isi</span>
                  <textarea name="" id="content" cols="30" rows="15" class="form-control">{!! old('content', $data->Visimisi->content) !!}</textarea>
		</div><!-- /.col -->
	</div><!-- /.row -->
@endsection

@push('scripts')
<script src="{{ URL::to('/assets/back/tinymce/tinymce.min.js') }}"></script>
     <script src="{{ URL::to('/vendor/laravel-filemanager/js/lfm.js') }}"></script>
    <script>
             var editor_config = {
              path_absolute : "{{ URL::to('/') }}/",
              height: 500,
              selector: "textarea#content",
              plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern"
              ],
              toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
              relative_urls: false,
              file_browser_callback : function(field_name, url, type, win) {
                var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

                var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                if (type == 'image') {
                  cmsURL = cmsURL + "&type=Images";
                } else {
                  cmsURL = cmsURL + "&type=Files";
                }

                tinyMCE.activeEditor.windowManager.open({
                  file : cmsURL,
                  title : 'Filemanager',
                  width : x * 0.8,
                  height : y * 0.8,
                  resizable : "yes",
                  close_previous : "no"
                });
              }
            };

            tinymce.init(editor_config);
      </script>
      <script type="text/javascript">
          $(document).ready(function(){
                  $("body").on("click","#btn-save", function(){
                  	// alert("ddd");
                          if(validasiContent()){
                                var content = tinymce.get("content").getContent();
                                swal({
                                      title: "Apakah Anda Yakin Menyimpan Perubahan!",
                                      type: "info",
                                      confirmButtonClass: "btn-info",
                                      confirmButtonText: "Yes!",
                                      showCancelButton: true,
                                  }).then(function () {
                                      $.ajax({
                                          type: "POST",
                                          url: "{{ route('visi-misi-update', $data->name) }}",
                                          data: {content : content},
                                          success: function (data) {
                                                  swal(
                                                      'Berhasil!',
                                                      'Visi & Misi Berhasil Di Simpan',
                                                      'success'
                                                    ).then(function () {
                                                           window.location= "{{ route('visi-misi-index', $data->name) }}";
                                                     },function (dismiss) {
                                                            if (dismiss === 'timer') {
                                                               window.location= "{{ route('visi-misi-index', $data->name) }}";
                                                            }
                                                     });
                                          }, error(data){
                                                swal(
                                                  'Gagal',
                                                  'Visi & Misi Gagal Di Simpan',
                                                  'error'
                                                );
                                          }         
                                      });
                                }, function (dismiss) {});
                          }
                   });
                  
            });
          function validasiContent(){
                  var content = tinymce.get("content").getContent();
                   if(content.trim().length<=0){
                          $(".help-block").show();
                          return false;
                   }else{
                          $(".help-block").hide();
                          return true;
                    }
              }             
    </script>
@endpush