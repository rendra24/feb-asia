@extends('back.layout.app')

@section('content')
	<div class="page-header">
		<div class="row">
			<div class="col-sm-9">
				<h1><i class="fa fa-university"></i>&nbsp;&nbsp; Jadwal Perkuliahan Semestes {{ $perkuliahan->semester }} Ta. {{ $perkuliahan->tahun_ajaran }}</h1>
			</div>
                  <div class="col-sm-3 text-right">
                        <div class="btn-group" role="group" aria-label="...">
					<a href="{{ route('perkuliahan-index') }}" class="btn btn-white btn-warning btn-bold" style="margin-right: 5px;">
						<i class="fa fa-angle-double-left"></i>&nbsp;&nbsp; Back
					</a>
                      		<button type="button" class="btn btn-white btn-primary btn-bold" data-toggle="modal" data-target="#modalDetail" data-status="add"><i class="fa fa-plus"></i>&nbsp;&nbsp;&nbsp;Add New </button>
                      	</div>
                  </div>
		</div>
	</div><!-- /.page-header -->

	<div class="row">
		<div class="col-xs-12">
			<div class="table-responsive">
		              <table class="table  table-bordered table-hover" >
		                  <thead>
				                  <tr>
				                      <th class="text-center">Hari</th>
				                      <th class="text-center">Jam</th>
				                      <th class="text-center">Ruangan</th>
				                      <th class="text-center">Kelas</th>
				                      <th class="text-center">Mata Kuliah</th>
				                      <th class="text-center">Dosen Pengampu</th>
				                      <th>&nbsp;</th>
				                  </tr>
			                  </thead>
			                  <tbody>
			                  	@if($detail)
			                      		@foreach($detail as $data)
					                   <tr>
					                      	<td class="text-center">
					                      		{{ $data->hari }}
					                      	</td>
					                      	<td class="text-center">
					                      		{{ $data->jam }}
					                      	</td>
					                      	<td>
					                      		{{ $data->ruangan }}
					                      	</td>
					                      	<td class="text-center">
					                      		{{ $data->kelas }}
					                      	</td>
					                      	<td>
					                      		{{ $data->mata_kuliah }}
					                      	</td>
					                      	<td>
					                      		{{ $data->dosen_pengampu }}
					                      	</td>
					                          <td class="text-center">
					                              <button class="btn btn-white btn-success btn-bold btn-sm" data-toggle="modal" data-target="#modalDetail" data-status="edit" data-hari="{{ $data->hari }}" data-jam="{{ $data->jam }}" data-ruangan="{{ $data->ruangan }}" data-kelas="{{ $data->kelas }}" data-mata_kuliah="{{ $data->mata_kuliah }}" data-dosen_pengampu="{{ $data->dosen_pengampu }}" data-id="{{ $data->id }}"><i class="fa fa-pencil"></i>&nbsp; Edit</button>
					                              <button class="btn btn-white btn-danger btn-bold btn-sm btn-delete" detailId="{{ $data->id }}"><i class="fa fa-trash-o "></i>&nbsp; Delete</button>
					                          </td>
					                    </tr>
			                      		@endforeach
			                      	@endif
			                  </tbody>
		              </table>
	              </div>
		</div><!-- /.col -->
	</div><!-- /.row -->
@include('back.perkuliahan.detailForm')
@endsection
@push('scripts')
	<script type="text/javascript">

		$(document).ready(function(){
			// $('#modalForm').modal({backdrop: 'static', keyboard: false});
			// $('#modalForm').modal({backdrop: 'static', keyboard: false})；
			      $(".btn-delete").click(function() {
			      		var detailId = $(this).attr("detailId");
			              swal({
			                title: "Apakah Anda Yakin!",
			                type: "error",
			                confirmButtonClass: "btn-danger",
			                confirmButtonText: "Yes!",
			                showCancelButton: true,
			            }).then(function () {
				                $.ajax({
				                    type: "POST",
				                    url: "{{ route('perkuliahan-detail-delete') }}",
				                    data: {id:detailId},
				                    success: function (data) {
				                            swal(
				                                'Deleted!',
				                                'Detail Jadwal Perkuliahan Berhasil Di Hapus',
				                                'success'
				                              ).then(
				                                function () {
				                                   location.reload();
				                                 },
				                                function (dismiss) {
				                                  if (dismiss === 'timer') {
				                                     location.reload();
				                                  }
				                                }
				                              );
				                           
				                    }, error(data){
				                          swal(
				                            'Cancelled',
				                            'Detail Jadwal Perkuliahan Gagal Di Hapus',
				                            'error'
				                          );
				                    }         
				                });
				          }, function (dismiss) {
				        
				          });
			      });
		});
	</script>
@endpush