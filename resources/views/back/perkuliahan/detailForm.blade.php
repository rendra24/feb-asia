<!-- Modal -->
<div class="modal fade" id="modalDetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">
		<i class="fa fa-plus"></i>&nbsp;&nbsp;Form <span class="title"></span> Jadwal Perkuliahan
        </h4>
      </div>
      <form action="" method="post" id="formDetail">
      <div class="modal-body">
        	<div class="form-horizontal">
			<div class="form-group form-hari">
			    	<label for="inputEmail3" class="col-sm-3 control-label">Hari</label>
			    	<div class="col-sm-9">
			    		<input type="text" class="form-control hari" placeholder="Masukkan Hari" name="hari">
                                <span  class="help-block" style="display: none;">*Hari Masih Kosong, Harus Di Isi</span>
			    	</div>
			</div>
                   <div class="form-group form-jam">
                          <label for="inputEmail3" class="col-sm-3 control-label">Jam</label>
                          <div class="col-sm-9">
                                <input type="text" class="form-control jam" placeholder="Masukkan Jam" name="jam">
                                <span  class="help-block" style="display: none;">*Jam Masih Kosong, Harus Di Isi</span>
                          </div>
                   </div>
                   <div class="form-group form-ruangan">
                          <label for="inputEmail3" class="col-sm-3 control-label">Ruangan</label>
                          <div class="col-sm-9">
                                <input type="text" class="form-control ruangan" placeholder="Masukkan Ruangan" name="ruangan">
                                <span  class="help-block" style="display: none;">*Ruangan Masih Kosong, Harus Di Isi</span>
                          </div>
                   </div>
                   <div class="form-group form-kelas">
                          <label for="inputEmail3" class="col-sm-3 control-label">Kelas</label>
                          <div class="col-sm-9">
                                <input type="text" class="form-control kelas" placeholder="Masukkan Kelas" name="kelas">
                                <span  class="help-block" style="display: none;">*Kelas Masih Kosong, Harus Di Isi</span>
                          </div>
                   </div>
                   <div class="form-group form-mata_kuliah">
                          <label for="inputEmail3" class="col-sm-3 control-label">Mata Kuliah</label>
                          <div class="col-sm-9">
                                <input type="text" class="form-control mata_kuliah" placeholder="Masukkan Mata Kuliah" name="mata_kuliah">
                                <span  class="help-block" style="display: none;">*Mata Kuliah Masih Kosong, Harus Di Isi</span>
                          </div>
                   </div>
                   <div class="form-group form-dosen_pengampu">
                          <label for="inputEmail3" class="col-sm-3 control-label">Dosen Pengampu</label>
                          <div class="col-sm-9">
                                <input class="id" type="hidden" name="id" value="">
                                <input type="text" class="form-control dosen_pengampu" placeholder="Masukkan Dosen Pengampu" name="dosen_pengampu">
                                <span  class="help-block" style="display: none;">*Dosen Pengampu Masih Kosong, Harus Di Isi</span>
                          </div>
                   </div>
		</div>
      </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;&nbsp;Batal</button>
          <button type="submit" class="btn btn-primary" id="btn-save"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Simpan</button>
      </div>
      </form>
    </div>
  </div>
</div>

@push('scripts')
  <script type="text/javascript">
    $(document).ready(function(){
      var status_form=true;
      $('#modalDetail').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var status = button.data('status');
            var modal = $(this);
            if(status=='edit'){
                    status_form=false;
                    var id = button.data('id');
                    var hari = button.data('hari');
                    var jam = button.data('jam');
                    var ruangan = button.data('ruangan');
                    var kelas = button.data('kelas');
                    var mata_kuliah = button.data('mata_kuliah');
                    var dosen_pengampu = button.data('dosen_pengampu');
                    console.log(id);
                      modal.find('.modal-body').find("input[name='id']").val(id);
                      modal.find('.modal-body').find("input[name='hari']").val(hari);
                      modal.find('.modal-body').find("input[name='jam']").val(jam);
                      modal.find('.modal-body').find("input[name='ruangan']").val(ruangan);
                      modal.find('.modal-body').find("input[name='kelas']").val(kelas);
                      modal.find('.modal-body').find("input[name='mata_kuliah']").val(mata_kuliah);
                      modal.find('.modal-body').find("input[name='dosen_pengampu']").val(dosen_pengampu);
                    modal.find('.modal-title').find(".fa").removeClass("fa-plus");
                    modal.find('.modal-title').find(".fa").addClass("fa-pencil-square-o");
                    modal.find('.modal-title').find(".title").text("Edit ");
            }else{
                    status_form=true;
                    modal.find('.modal-title').find(".fa").removeClass("fa-pencil-square-o");
                    modal.find('.modal-title').find(".fa").addClass("fa-plus");
                    modal.find('.modal-title').find(".title").text("Tambah ");
            }
      });

      $('#modalDetail').on('hide.bs.modal', function (event) {
             var modal = $(this);
            modal.find('.modal-body').find("input[name='id']").val('');
            modal.find('.modal-body').find("input[name='hari']").val('');
            modal.find('.modal-body').find("input[name='jam']").val('');
            modal.find('.modal-body').find("input[name='ruangan']").val('');
            modal.find('.modal-body').find("input[name='kelas']").val('');
            modal.find('.modal-body').find("input[name='mata_kuliah']").val('');
            modal.find('.modal-body').find("input[name='dosen_pengampu']").val('');
            $(".help-block").hide();
            $(".form-group").removeClass('has-error');
      });

      $("#formDetail").submit(function(e){
            if(status_form){
              var url = "{{ route('perkuliahan-detail-save', $perkuliahan->id) }}";
            }else{
              var url = "{{ route('perkuliahan-detail-update', $perkuliahan->id) }}";
            }
          e.preventDefault();
          if(isValid()){
                      $.ajax({
                          type: "POST",
                          url: url,
                          data: $(this).serialize(),
                          beforeSend: function() {
                            $("#btn-save .fa").removeClass('fa-floppy-o');
                            $("#btn-save .fa").addClass("fa-spinner fa-pulse fa-lg");
                          },success: function (data) {
                            console.log(data);
                                swal(
                                      'Berhasil!',
                                      'Jadwal Perkuliahan Berhasil Di Simpan',
                                      'success'
                                ).then(
                                      function () {
                                          location.reload();
                                      },function (dismiss) {
                                          if (dismiss === 'timer') {
                                              location.reload();
                                          }
                                  }
                                );
                          },error:function(data, status){
                               if(data.status==422){
                                       isValid();
                                }else{
                                      swal(
                                                'Gagal!',
                                                'Jadwal Perkuliahan Gagal Di Simpan',
                                                'error'
                                          ).then(
                                                function () {
                                                },function (dismiss) {
                                                    if (dismiss === 'timer') {
                                                    }
                                            }
                                          );
                                }
                          },complete: function() {
                            $("#btn-save .fa").removeClass("fa-spinner fa-pulse fa-lg");
                            $("#btn-save .fa").addClass('fa-floppy-o');
                          }
                        });
             }
            return false;
      });

      function isValid(){
            var hari = validasi('hari');
            var jam = validasi('jam');
            var ruangan = validasi('ruangan');
            var kelas = validasi('kelas');
            var mata_kuliah = validasi('mata_kuliah');
            var dosen_pengampu = validasi('dosen_pengampu');

            return (hari && jam && ruangan && kelas && mata_kuliah && dosen_pengampu);
      }

      function validasi(elemt){
            if($("."+elemt).val().trim().length<=0){
                    $(".form-"+elemt).addClass('has-error');
                    $(".form-"+elemt+" .help-block").show();
                    return false;
            }else{
                    $(".form-"+elemt).removeClass('has-error');
                    $(".form-"+elemt+" .help-block").hide();
                    return true;
            }
      }

    })
  </script>
@endpush