@extends('back.layout.app')

@section('content')
<div class="page-header">
    <div class="row">
        <div class="col-sm-10">
            <h1>Sertifikasi FTD ASIA</h1>
        </div>
        <div class="col-sm-2">
            <a href="{{ route('sertifikasi-edit', $data->id) }}" class="btn btn-white btn-info btn-bold btn-block"
                type="button">
                <i class="ace-icon fa fa-pencil-square-o bigger-110"></i>
                Edit
            </a>
        </div>
    </div>
</div><!-- /.page-header -->

<div class="row">
    <div class="col-xs-12">
        @if($data->content)
        {!! $data->content !!}
        @endif
    </div><!-- /.col -->
</div><!-- /.row -->
@endsection