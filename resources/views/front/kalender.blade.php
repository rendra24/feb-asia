@extends('front.layout.app')
@section('title', 'KALENDER AKADEMIK SEMESTER '.$kalender->semester.' TA '.$kalender->tahun_ajaran)
@section('content')
<div class="container min-700">
	<div class="row">
		<div class="col-md-12 well well-white mt-50">
			<div class="text-center">
				<h1 class="title-content weight-7">KALENDER AKADEMIK</h1>
				<h1 class="title-content weight-6">SEMESTER <span class="uppercase">{{ $kalender->semester }}</span> TA {{ $kalender->tahun_ajaran }}</span></h1>
			</div>
			@if($details)
				<div class="table-responsive mt-30">
					<table class="table table-bordered table-striped">
						<thead >
							<tr class="default">
								<th class="text-center">TANGGAL</th>
								<th class="text-center">KEGIATAN</th>
							</tr>
						</thead>
						<tbody>
							@foreach($details as $data)
							<tr>
								<td>{{ $data->tanggal }}</td>
								<td>{{ $data->kegiatan }}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			@endif
		</div>
	</div>
</div>
@endsection