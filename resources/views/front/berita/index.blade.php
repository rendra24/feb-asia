@extends('front.layout.app')
@section('title', 'Pojok Berita FEB ASIA MALANG')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12 well well-white mt-50 min-700">
			<h2>Pojok Berita</h2>
			<div class="row">
				@foreach($berita as $data)
				<div class="col-md-4 box-berita">
					<img src="{{ URL::to($data->image) }}" alt="" class="img-responsive">
					<h4 class="title"><a href="{{ route('berita-detail', [ 'id' => $data->id, 'slug'=>$data->slug ]) }}">{{ $data->title }}</a></h4>
					<h6 class="date">{{ $data->user->name }} | {{ $data->date }}</h6>
					<div class="content">
						{!! $data->body !!}
					</div>
				</div>
				@endforeach
			</div>
			<div class="text-center">
				{{ $berita->links() }}
			</div>
		</div>
	</div>
</div>
@endsection