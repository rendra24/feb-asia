@extends('front.layout.app')
@section('title', $berita->title)
@section('description', strip_tags($berita->content))
@section('meta_image', URL::to($berita->image))
@section('content')
<div class="container min-700">
	<div class="row">
		<div class="col-md-12 well well-white mt-50">
			<div class="row">
				<div class="col-md-9">

					<h1 class="mt-0 title-berita">{{ $berita->title }}</h1>
					<h5>{{ $berita->user->name }} | {{ $berita->date }}</h5>
					<div>
						<img src="{{ URL::to($berita->image) }}" alt="" class="img-100">
					</div>
					<div class="mt-15">
						{!! $berita->content !!}
					</div>
				</div>
				<div class="col-md-3">
					<h2 class="mb-0">Berita Terbaru</h2>
					<div class="list-group menu-home">
                                      @foreach($lain as $key=>$data)
					  	      <a href="{{ route('berita-detail', ['id' => $data->id, 'slug' => $data->slug]) }}" class="list-group-item">{{ $data->title }}</a>
                                      @endforeach
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection