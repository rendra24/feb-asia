@extends('front.layout.new.home')

@section('title', 'Fakultas Ekonomi & Bisnis (FEB)')

@section('style')
<link rel="stylesheet" href="https://unpkg.com/swiper@8/swiper-bundle.min.css" />
@endsection

@section('content')
<div class="hero">
    <div class="d-flex flex-column h-100 px-3 px-lg-5 py-5 align-items-lg-start justify-content-center">
        <h1 class="text-white text-center text-lg-left hero-title">
            Fakultas Ekonomi dan <br> Bisnis (FEB)
        </h1>
        <p class="hero-content text-white mt-2 mx-auto mx-lg-0 text-center text-lg-left">
            Pilihan Studi bagi calon Pemimpin Bisnis dan Akuntan Profesional di Masa Depan. FEB Institut Asia selalu
            berkomitmen
            untuk mencetak eksekutif muda dengan karir yang cemerlang!
        </p>
        <a href="#" class="btn btn-warning rounded-pill px-3 px-lg-4 mt-4 mt-0 text-uppercase btn-lg custom-text-semibold">Hubungi Kami</a>
    </div>
</div>
<div class="overview w-100">
    <div class="d-flex flex-column flex-lg-row align-items-center">
        <div class="col-md-6 p-0">
            <img src="{{ asset('assets/banner/banner2.jpg') }}" width="100%" height="auto">
        </div>
        <div class="col-12 col-lg-6 px-lg-5 px-4 py-5 py-lg-0 text-lg-left text-center">
            <div class="d-flex ml-lg-n4">
                <img class="mr-lg-2 d-none d-lg-inline" src="{{ asset('assets/icons/awal_kutip.png') }}" alt="" width="20px" height="15px">
                <p class="">
                    Bersama Institut Asia, tidak hanya mendapatkan gelar, namun memperluas pengetahuan dan membuatmu mampu
                    menjadi
                    pemimpin
                    yang brilian, inovatif dan produktif.
                    <img class="d-none d-lg-inline" src="{{ asset('assets/icons/akhir_kutip.png') }}" alt="" width="20px" height="15px">
                </p>
            </div>
            <h5>
                RISA SANTOSO, B.A., M.ED
            </h5>
            <h6 class="jabatan-text">
                Rektor Universitas Institut Asia
            </h6>
        </div>
    </div>
    <div class="d-none d-lg-flex flex-row h100 align-items-center">
        <div class="col-12 col-lg-6 m-0 px-4 px-lg-5 py-5 py-lg-0">
            <p class="text-center text-lg-left w-75 m-auto">
                Sebagai kampus yang menonjolkan soft-skill, FEB menerapkan metode pembelajaran interaktif yang didukung
                dengan
                fasilitas
                modern. Lulusan FEB Institut Asia dipersiapkan untuk menghadapi tantangan bisnis, sebagai profesional
                dan
                entrepreneur
                tangguh.
            </p>
        </div>
        <div class="col-md-6 d-none d-lg-block p-0 m-0">
            <img src="{{ asset('assets/banner/prestasi1.jpg') }}" width="100%" height="auto">
        </div>
    </div>
    <div class="d-none d-lg-flex flex-row h-100 align-items-center">
        <div class="col-md-6 p-0 d-none d-lg-block">
            <img src="{{ asset('assets/banner/gedung1.jpg') }}" width="100%" height="auto">
        </div>
        <div class="quote col-12 col-lg-6 px-lg-5 px-4 py-5 py-lg-0 text-lg-left text-center">
            <h5>KAMPUS 1</h5>
            <p>
                Memberikan fasilitas perkuliahan yang dilengkapi oleh ruang kelas, ruang teater, perpustakaan, berbagai
                laboratorium,
                dan sarana lainnya.
            </p>
        </div>
    </div>
    <div class="d-none d-lg-flex flex-row h100 align-items-center">
        <div class="col-12 col-lg-6 m-0 px-4 px-lg-5 py-5 py-lg-0 text-center text-lg-left">
            <h5 class="w-75 m-auto">KAMPUS 2</h5>
            <p class="w-75 m-auto">
                Memiliki interior yang modern yang dilengkapi dengan sarana terbaru, untuk pengalaman belajar yang
                menyenangkan.
            </p>
        </div>
        <div class="col-md-6 d-none d-lg-block p-0 m-0">
            <img src="{{ asset('assets/banner/gedung2.jpg') }}" width="100%" height="auto">
        </div>
    </div>
    <div class="px-4">
        <div class="swiper d-md-none" id="swiper-mobile-fasilitas">
            <div class="swiper-wrapper">
                @php
                $fasilitas = [
                    'assets/banner/prestasi1.jpg',
                    'assets/banner/gedung1.jpg',
                    'assets/banner/gedung2.jpg'
                ];
                @endphp
                @foreach ($fasilitas as $item)
                <div class="swiper-slide">
                    <img src="{{ asset($item) }}" alt="" width="100%">
                    <p class="mt-3">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Sequi ratione, adipisci distinctio eos facilis
                        molestias rem porro alias, ad nostrum perferendis, quos excepturi! Est libero nihil quis nobis a.
                        Similique.
                    </p>
                </div>
                @endforeach
            </div>
            <div class="slider-button-prev text-warning" id="fasilitas-prev"></div>
            <div class="slider-button-next text-warning" id="fasilitas-next"></div>
        </div>
    </div>
</div>
<div class="mt-4 mt-md-0 mb-4">
    <img src="{{ asset('assets/banner/video-img.jpg') }}" alt="" class="w-100 mx-auto" height="auto">
</div>
<div class="container py-lg-4">
    <hr class="section-line">
    <h4 class="text-custom-primary text-uppercase">Program Studi</h4>
    <div class="mt-4">
        @php
        $prodi = (object) [
            (object) [
                'url' => 'assets/images/berita/berita1.jpg',
                'nama' => 'Akutansi'
            ],
            (object) [
                'url' => 'assets/images/berita/berita2.jpg',
                'nama' => 'Professional Bussiness Management'
            ],
            (object) [
                'url' => 'assets/images/berita/berita6.jpg',
                'nama' => 'Program Pasca Sarjana (S2)'
            ]
        ]
        @endphp

        <div class="swiper" id="swiper-prodi">
            <div class="swiper-wrapper">
                @foreach ($prodi as $item)
                <div class="card col-lg-5 px-0 shadow swiper-slide">
                    <img src="{{ asset($item->url) }}" class="card-img-top" alt="">
                    <div class="card-body d-flex flex-column justify-content-center text-center">
                        <h5 class="card-title title-ellipsis">{{ $item->nama }}</h5>
                        <a href="#" class="btn btn-warning">Lihat Lebih Lanjut</a>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="slider-button-next text-warning" id="prodi-next"></div>
            <div class="slider-button-prev text-warning" id="prodi-prev"></div>
        </div>
    </div>
</div>
<div class="container mt-3">
    @php
    $prodi = (object) [
        (object) [
            'url' => 'assets/images/berita/berita3.jpg',
            'nama' => 'KEBERHASILAN TIM UTASI DI TINGKAT NASIONAL , JUARA 3 DALAM KATEGORI IDE BISNIS'
        ],
        (object) [
            'url' => 'assets/images/berita/berita4.jpg',
            'nama' => 'PEMBUKAAN ASIA VIRTUAL HACKFEST 2021 INSTITUT ASIA MALANG'
        ],
        (object) [
            'url' => 'assets/images/berita/berita5.jpg',
            'nama' => 'ANUGERAH KAMPUS UNGGULAN UNTUK INSTITUT ASIA MALANG DARI LLDIKTI VII JAWA TIMUR'
        ]
    ]
    @endphp
    <div class="row justify-content-center px-3 p-lg-0">
        @foreach ($prodi as $item)
            <div class="card border-0 col-lg-3 px-0 my-1 mx-2">
                <img src="{{ $item->url }}" class="card-img-top">
                <hr class="article-line my-4">
                <div class="card-body px-0 pt-lg-0">
                    <h6 class="card-title content-ellipsis article-title">{{ $item->nama }}</h6>
                    <p class="card-text content-ellipsis">
                        Lorem ipsum, dolor sit amet consectetur adipisicing elit. Porro vero eligendi unde dolor, impedit voluptas.
                        Tempora ullam itaque ducimus voluptate dolorum. Consequatur ea magni quod sit amet quas earum praesentium.
                    </p>
                    <a href="#" class="text-custom-primary d-flex">Read More <i class="fa-solid fa-chevron-right fa-xs align-self-center ml-2"></i></a>
                </div>
            </div>
        @endforeach
    </div>
</div>
<div class="container py-4">
    <hr class="section-line">
    <h4 class="text-custom-primary text-uppercase">
        Serunya Belajar di Asia
    </h4>
    @php
        $galeri = (object )[
            'assets/images/galeri/gallery1.jpg',
            'assets/images/galeri/gallery2.jpg',
            'assets/images/galeri/gallery3.jpg',
            'assets/images/galeri/gallery4.jpg',
            'assets/images/galeri/gallery5.jpg',
            'assets/images/galeri/gallery6.jpg'
        ]
    @endphp

    <div class="gallery-container mt-3 d-none">
        <img class="rounded-lg" src="{{ asset('assets/images/galeri/gallery1.jpg') }}" alt="">
        <img class="rounded-lg" src="{{ asset('assets/images/galeri/gallery2.png') }}" alt="">
        <img class="rounded-lg" src="{{ asset('assets/images/galeri/gallery3.png') }}" alt="">
        <img class="rounded-lg" src="{{ asset('assets/images/galeri/gallery4.jpg') }}" alt="">
    </div>
    <div class="row justify-content-center mt-1 d-none d-md-flex">
        <img class="rounded-lg col-lg-6" src="{{ asset('assets/images/galeri/gallery5.jpg') }}" alt="">
        <img class="rounded-lg col-lg-6" src="{{ asset('assets/images/galeri/gallery6.png') }}" alt="">
    </div>

    <div class="mt-3 d-md-none overflow-hidden">
        <div class="swiper" id="swiper-gallery">
            <div class="swiper-wrapper">
                <div class="swiper-slide mx-2">
                    <img class="rounded-lg w-100" src="{{ asset('assets/images/galeri/gallery1.jpg') }}" alt="" height="auto">
                </div>
                <div class="swiper-slide mx-2">
                    <img class="rounded-lg w-100" src="{{ asset('assets/images/galeri/gallery4.jpg') }}" alt="" height="auto">
                </div>
            </div>
            <div class="slider-button-prev text-white" id="gallery-prev"></div>
            <div class="slider-button-next text-white" id="gallery-next"></div>
        </div>
    </div>
</div>

<div class="container py-4">
    <hr class="section-line">
    <h4 class="text-custom-primary text-uppercase">
        Kata Mereka Tentang Asia
    </h4>
    <div class="swiper col-md-12 px-0" id="swiper-testimoni">
        <div class="swiper-wrapper">
            @php
                $testimoni = [
                    'assets/images/testi/testi1.png',
                    'assets/images/testi/testi2.png'
                ]
            @endphp

            @foreach ($testimoni as $item)
                <div class="swiper-slide">
                    <img src="{{ asset($item) }}" alt="" width="100%">
                </div>
            @endforeach
        </div>
        <div class="slider-button-prev slider-testimoni text-warning" id="testimoni-prev"></div>
        <div class="slider-button-next slider-testimoni text-warning" id="testimoni-next"></div>
    </div>
</div>

<div>
    <img class="w-100" src="{{ asset('assets/banner/banner-footer.jpg') }}" alt="">
    <div class="bg-custom-primary d-flex flex-column py-">
        <div class="d-none d-lg-flex flex-column">
            <img class="col-10 mx-auto" src="{{ asset('assets/banner/banner-program1.png') }}" alt="">
            <img class="col-10 mx-auto" src="{{ asset('assets/banner/banner-program2.png') }}" alt="">
            <img class="col-10 mx-auto" src="{{ asset('assets/banner/banner-program3.png') }}" alt="">
        </div>

        <div class="d-md-none">
            @php
            $prodi = (object) [
                (object) [
                    'url' => 'assets/images/berita/berita1.jpg',
                    'nama' => 'Akutansi',
                    'deskripsi' => '
                        Bagi yang suka bermain angka, posisi akuntan cocok deh buat kamu. Disini kamu bisa
                        belajar
                        untuk mencatat informasi finansial dengan benar dan menyajikan laporan keuangan untuk
                        manajer perusahaan.
                    '
                ],
                (object) [
                    'url' => 'assets/images/berita/berita2.jpg',
                    'nama' => 'Professional Bussiness Management',
                    'deskripsi' => '
                        Program Studi Profesional bisnis Manajemen (PBM) memberikan pengalaman yang menarik
                        dalam
                        mempelajari bisnis di level startup hingga perusahaan multinasional. Disini kamu akan
                        menjadi lulusan Sarjana Manajemen (S.M.) yang smart dan kreatif, serta mampu berinovasi,
                        hingga menjalin kerjasama bisnis dengan para profesional global.
                    '
                ],
                (object) [
                    'url' => 'assets/images/berita/berita6.jpg',
                    'nama' => 'Program Pasca Sarjana (S2)',
                    'deskripsi' => '
                        Program Akuntansi (Bergelar S.Ak) yang berbasis teori & Praktek Akuntansi Keuangan dan
                        Manajerial Perusahaan dengan konsentrasi penjurusan : Akuntansi Keuangan & Pemeriksaan
                        Akuntansi; Perpajakan dan Akuntansi Manajemen Industri.
                    '
                ]
            ]
            @endphp

            <div class="swiper px-3" id="swiper-new-prodi">
                <div class="swiper-wrapper">
                    @foreach ($prodi as $item)
                    <div class="card card-prodi col-lg-5 px-0 shadow swiper-slide bg-warning">
                        <img src="{{ asset($item->url) }}" class="card-img-top" alt="">
                        <div class="card-body d-flex flex-column justify-content-center text-center">
                            <h5 class=" card-title title-ellipsis">{{ $item->nama }}</h5>
                            <p class="">
                                {{ $item->deskripsi }}
                            </p>
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="slider-button-prev text-white" id="new-prodi-prev"></div>
                <div class="slider-button-next text-white" id="new-prodi-next"></div>
            </div>
        </div>

        <div class="mt-3 d-flex flex-column container">
            <h4 class="text-center text-white text-upppercase">
                Pendaftaran Mahasiswa Baru
            </h4>
            <p class="text-white col-12 col-md-8 text-center mx-auto">
                Bergabunglah bersama FEB Institut Asia sekarang, dan raihlah
                kesempatan memperoleh diskon biaya studi hingga beasiswa!
            </p>
            <div class="d-none d-lg-flex flex-lg-column">
                <img class="col-11 mx-auto my-3" src="{{ asset('assets/images/step-pendaftaran.png') }}" alt="">
                <div class="d-flex flex-row text-white mx-4">
                    <div class="col-3 step-up d-flex flex-column justify-content-center">
                        <div class="text-center">
                            <h4 class="text-uppercase">Langkah 1</h4>
                            <p>Membayar Biaya Pendaftaran</p>
                            <h5>Rp 150.000</h5>
                        </div>
                        <i class="fa-solid fa-caret-down fa-xl"></i>
                        <div>
                            <h4 class="text-center">Mengisi Formulir Pendaftaran</h4>
                            <ul>
                                <li>
                                    Datang ke kampus
                                </li>
                                <li>
                                    Melalui online pendaftaran
                                    <strong>asia.ac.id</strong>
                                </li>
                                <li>
                                    Atau melalui sekolah yang
                                    sudah bekerja sama dengan
                                    <strong>Institut Asia</strong>
                                </li>
                            </ul>
                        </div>
                        <i class="fa-solid fa-caret-down fa-xl"></i>
                        <p class="text-center">
                            Menyerahkan fotocopy
                            ijazah minimal SLTA & foto
                            berwarna 3x4 sebanyak 4
                            lembar (bisa menyusul)
                        </p>
                    </div>
                    <div class="col-3 step-2 d-flex flex-column justify-content-center">
                        <div class="text-center">
                            <h4 class="text-uppercase">Langkah 2</h4>
                            <p>
                                Mengisi Formulir Daftar Ulang
                            </p>
                        </div>
                        <i class="fa-solid fa-caret-down fa-xl"></i>
                        <div class="text-center">
                            <p>
                                Membayar Biaya
                                Daftar Ulang
                            </p>
                            <h5>
                                AK/PBM
                                <br>
                                Rp 5.050.000
                            </h5>
                        </div>
                    </div>
                    <div class="col-3 step-up">
                        <div class="text-center">
                            <h4 class="text-uppercase">Langkah 3</h4>
                            <p>
                                Mahasiswa Aktif
                            </p>
                        </div>
                    </div>
                    <div class="col-3 step-4">
                        <div class="text-center">
                            <h4 class="text-uppercase">Langkah 4</h4>
                            <p>
                                Kuliah di Kampus Asia
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="d-lg-none">
                <div class="swiper px-3" id="swiper-new-prodi">
                    <div class="swiper-wrapper">
                        <div class="col-lg-5 px-0 swiper-slide bg-custom-primary text-white d-flex flex-column">
                            <img src="{{ asset('assets/images/langkah/langkah1.png') }}" class="mx-auto mb-3" alt="" width="60%">
                            <div class="d-flex flex-column justify-content-center text-center">
                                <div class="text-center">
                                    <h4 class="text-uppercase">Langkah 1</h4>
                                    <p>Membayar Biaya Pendaftaran</p>
                                    <h5>Rp 150.000</h5>
                                </div>
                                <i class="fa-solid fa-caret-down fa-xl"></i>
                                <div>
                                    <h4 class="text-center">Mengisi Formulir Pendaftaran</h4>
                                    <ul>
                                        <li>
                                            Datang ke kampus
                                        </li>
                                        <li>
                                            Melalui online pendaftaran
                                            <strong>asia.ac.id</strong>
                                        </li>
                                        <li>
                                            Atau melalui sekolah yang
                                            sudah bekerja sama dengan
                                            <strong>Institut Asia</strong>
                                        </li>
                                    </ul>
                                </div>
                                <i class="fa-solid fa-caret-down fa-xl"></i>
                                <p class="text-center">
                                    Menyerahkan fotocopy
                                    ijazah minimal SLTA & foto
                                    berwarna 3x4 sebanyak 4
                                    lembar (bisa menyusul)
                                </p>
                            </div>
                        </div>
                        <div class="col-lg-5 px-0 swiper-slide bg-custom-primary text-white d-flex flex-column">
                            <img src="{{ asset('assets/images/langkah/langkah2.png') }}" class="mx-auto mb-3" alt="" width="60%">
                            <div class="d-flex flex-column justify-content-center text-center">
                                <div class="text-center">
                                    <h4 class="text-uppercase">Langkah 2</h4>
                                    <p>
                                        Mengisi Formulir Daftar Ulang
                                    </p>
                                </div>
                                <i class="fa-solid fa-caret-down fa-xl"></i>
                                <div class="text-center">
                                    <p>
                                        Membayar Biaya
                                        Daftar Ulang
                                    </p>
                                    <h5>
                                        AK/PBM
                                        <br>
                                        Rp 5.050.000
                                    </h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-5 px-0 swiper-slide bg-custom-primary text-white d-flex flex-column">
                            <img src="{{ asset('assets/images/langkah/langkah3.png') }}" class="mx-auto mb-3" alt="" width="60%">
                            <div class="d-flex flex-column justify-content-center text-center">
                                <h4 class="text-uppercase">Langkah 3</h4>
                                <p>
                                    Mahasiswa Aktif
                                </p>
                            </div>
                        </div>
                        <div class="col-lg-5 px-0 swiper-slide bg-custom-primary text-white d-flex flex-column">
                            <img src="{{ asset('assets/images/langkah/langkah4.png') }}" class="mx-auto mb-3" alt="" width="60%">
                            <div class="d-flex flex-column justify-content-center text-center">
                                <div class="text-center">
                                    <h4 class="text-uppercase">Langkah 4</h4>
                                    <p>
                                        Kuliah di Kampus Asia
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="slider-button-prev text-white" id="new-prodi-prev"></div>
                    <div class="slider-button-next text-white" id="new-prodi-next"></div>
                </div>
            </div>
        </div>
        <div class="d-flex flex-column my-5">
            <div class="d-flex flex-column flex-lg-row container text-center text-lg-left">
                <div class="col-lg-6 px-md-5 mt-4 right-border">
                    <span class="bg-warning rounded-pill py-1 px-2 h6 ">
                        BIAYA DAFTAR ULANG ANGSURAN
                    </span>
                    <div class="d-flex flex-row mt-4">
                        <div class="w-50">
                            <h4 class="text-white">ANGSURAN 1</h4>
                            <h5 class="text-white mt-4">AK/PBM</h5>
                            <h5 class="text-warning">Rp 2.600.000</h5>
                        </div>
                        <div class="w-50">
                            <h4 class="text-white">ANGSURAN 2</h4>
                            <h5 class="text-white mt-4">AK/PBM</h5>
                            <h5 class="text-warning">Rp 3.200.000</h5>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 px-md-5 d-flex flex-column">
                    <div class="mt-4">
                        <span class="bg-warning rounded-pill py-1 px-2 h6 ">
                            SPP PER SEMESTER (1-7)
                        </span>
                        <div class="mt-4">
                            <h5 class="text-white mt-4">AK/PBM</h5>
                            <h5 class="text-warning">Rp 3.200.000</h5>
                        </div>
                    </div>
                    <div class="mt-4">
                        <span class="bg-warning rounded-pill py-1 px-2 h6 ">
                            SPP PER SEMESTER 8 KEATAS
                        </span>
                        <div class="mt-4">
                            <h5 class="text-white mt-4">AK/PBM</h5>
                            <h5 class="text-warning">Rp 3.200.000</h5>
                        </div>
                    </div>
                </div>
            </div>
            <a href="#" class="btn btn-warning rounded-pill px-3 mt-3 mx-auto col-6 col-md-4 col-lg-2">
                Daftar Sekarang
            </a>
        </div>
    </div>
</div>
@endsection

@section('script')
<script src="https://unpkg.com/swiper@8/swiper-bundle.min.js"></script>

<script>
    const swiperProdi = new Swiper('#swiper-prodi', {
        direction: 'horizontal',
        loop: true,
        touchable: true,
        slidesPerView: window.innerWidth >= 768 ? 3 : 1 ,
        spaceBetween: 15,
        navigation: {
            nextEl: '#prodi-next',
            prevEl: '#prodi-prev',
        },
    });

    const swiperGallery = new Swiper('#swiper-gallery', {
        direction: 'horizontal',
        loop: true,
        touchable: true,
        slidesPerView: 2,
        spaceBetween: 15,
        navigation: {
            nextEl: '#gallery-next',
            prevEl: '#gallery-prev',
        },
    });

    const swiperTestimoni = new Swiper('#swiper-testimoni', {
        direction: 'horizontal',
        loop: true,
        touchable: true,
        slidesPerView: window.innerWidth >= 768 ? 3 : 1,
        spaceBetween: 5,
        navigation: {
            nextEl: '#testimoni-next',
            prevEl: '#testimoni-prev',
        },
    })

    const swiperFasilitas = new Swiper('#swiper-mobile-fasilitas', {
        direction: 'horizontal',
        loop: true,
        touchable: true,
        slidesPerView: 1,
        spaceBetween: 15,
    })

    const swiperNewProdi = new Swiper('#swiper-new-prodi', {
        direction: 'horizontal',
        loop: true,
        touchable: true,
        slidesPerView: 1,
        spaceBetween: 20,
        navigation: {
            nextEl: '#new-prodi-next',
            prevEl: '#new-prodi-prev',
        },
    })

    const swiperStep = new Swiper('#swiper-step', {
        direction: 'horizontal',
        loop: false,
        touchable: true,
        slidesPerView: 1,
        spaceBetween: 15,
        navigation: {
            nextEl: '#step-next',
            prevEl: '#step-prev',
        },
    })

    $('.slick').slick({
        dots: false,
        infinite: true,
        variableWidth: true,
        prevArrow: "<img style='width:40px;height:40px;' class='a-left control-c prev slick-prev' src='assets/icon/kiri.png'>",
        nextArrow: "<img style='width:40px;height:40px;' class='a-right control-c next slick-next' src='assets/icon/kanan.png'>"
    });
</script>
@endsection