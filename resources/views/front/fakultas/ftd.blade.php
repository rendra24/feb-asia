@extends('front.layout.new.home')

@section('title', 'Fakultas Teknologi & Desain (FTD)')

@section('style')
<link rel="stylesheet" href="https://unpkg.com/swiper@8/swiper-bundle.min.css" />
<style>
.floating-register {
    position: fixed;
    bottom: 0;
    left: 0;
    z-index: 1000;
    width: 630px;
    height: 97px;
    overflow: hidden;
    background-color: rgba(255, 255, 255, 0.6);
    -webkit-backdrop-filter: blur(4px);
    backdrop-filter: blur(4px);
    padding-left: 100px;
    display: none;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    -webkit-transition: all 0.1s ease-in-out;
    transition: all 0.1s ease-in-out;
}

.floating-register span {
    font-family: "Libre Baskerville", serif;
    font-size: 19px;
    line-height: 24px;
    letter-spacing: 0.05em;
    color: #1e1e1e;
    width: 229px;
}

.register {
    font-family: "Lato", sans-serif;
    font-size: 14px;
    line-height: 17px;
    letter-spacing: 0.05em;
    color: #fff;
    background-color: #FFC800;
    padding: 24px 35px;
    -webkit-box-shadow: 0px 8px 16px rgb(0 0 0 / 50%);
    box-shadow: 0px 8px 16px rgb(0 0 0 / 50%);
    border-radius: 10px;
    text-transform: uppercase;
    display: inline-block;
}

.wabi {
    font-family: "Lato", sans-serif;
    font-size: 14px;
    line-height: 17px;
    letter-spacing: 0.05em;
    color: #fff;
    background-color: #0bc64b;
    padding: 10px;
    border-radius: 10px;
    text-transform: uppercase;
    display: inline-block;
    margin-left: 20px;
    margin-right: 20px;
    width: 22%;
    text-align: center;
}

.wabi img {
    width: 40px;
}
</style>
@endsection

@section('content')
<div class="floating-register" style="display: flex;">
    <span>Be Inspired</span>
    <a href="https://pendaftaranonline.asia.ac.id/" class="register" style="width:50%;cursor:pointer;color:#fff;">daftar
        sekarang</a>

    <a href="https://api.whatsapp.com/send/?phone=6287881444088&text=Halo+WA+Center+Institute+Asia+Malang%2C+Saya+mau+tanya...&app_absent=0"
        style="cursor:pointer;" class="wabi"><img src="https://asia.ac.id/assets/css/images/waicon.png"></a>
</div>
<div class="hero-ftd">
    <div class="d-flex flex-column h-100 p-5 align-items-lg-start justify-content-center">
        <h1 class="hero-title text-white text-center text-lg-left ">
            Fakultas Teknologi dan <br> Desain (FTD)
        </h1>
        <p class="hero-content text-white mt-2 mx-auto mx-lg-0 text-center text-lg-left h5">
            Jadilah talenta digital terbaik dengan kemampuan untuk merancang teknologi informasi, dan desain interaktif.
        </p>
        <a href="https://api.whatsapp.com/send/?phone=6287881444088&text=Halo+WA+Center+Institute+Asia+Malang%2C+Saya+mau+tanya...&app_absent=0"
            class="btn btn-warning rounded-pill px-3 px-lg-4 mt-4 mt-0 text-uppercase btn-lg">Hubungi Kami</a>
    </div>
</div>
<div class="overview w-100">
    <div class="d-flex flex-column flex-lg-row align-items-center">
        <div class="col-md-6 p-0">
            <img src="{{ asset('assets/banner/banner2.jpg') }}" width="100%" height="auto">
        </div>
        <div class="col-12 col-lg-6 px-lg-5 px-4 py-5 py-lg-0 text-lg-left text-center">
            <div class="d-flex ml-n4">
                <img class="mr-2" src="{{ asset('assets/icons/awal_kutip.png') }}" alt="" width="20px" height="15px">
                <p>
                    Bersama Institut Asia, tidak hanya mendapatkan gelar, namun memperluas pengetahuan dan membuatmu
                    mampu
                    menjadi
                    pemimpin
                    yang brilian, inovatif dan produktif.
                    <img src="{{ asset('assets/icons/akhir_kutip.png') }}" alt="" width="20px" height="15px">
                </p>
            </div>
            <h5>
                RISA SANTOSO, B.A., M.Ed.
            </h5>
            <h6 class="jabatan-text">
                Rektor Universitas Institut Asia
            </h6>
        </div>
    </div>
</div>

<div class="container py-lg-4">
    <hr class="section-line">
    <h4 class="text-custom-primary text-uppercase">Program Studi</h4>
    <div class="row mt-4">



        @foreach ($prodi as $item)
        <div class="col-md-4">
            <div class="card px-0 shadow-none swiper-slide">
                <img src="{{ asset($item->image) }}" class="card-img-top" alt="">
                <div class="card-body d-flex flex-column justify-content-center text-center">
                    <div class="d-flex align-items-center justify-content-center" style="min-height:60px;">
                        <h5 class="card-title title-ellipsis">{{ $item->title }}</h5>
                    </div>
                    <a href="#" class="btn btn-warning">Lihat Lebih Lanjut</a>
                </div>
            </div>
        </div>
        @endforeach

    </div>
</div>

<div class="container mt-lg-4 mb-lg-4 pt-4 pb-4">
    <hr class="section-line">
    <h4 class="text-custom-primary text-uppercase mb-4">Prestasi</h4>
    <div class="px-3 p-lg-0">

        <div class="swiper" id="swiper-prodi">
            <div class="swiper-wrapper">
                @foreach ($prestasi as $item)

                <div class="card border-0 swiper-slide px-0 my-1">
                    <img src="{{ $item->image }}" class="card-img-top" style="min-height:220px;">
                    <div class="card-body" style="background: #e4e7e8c7;">
                        <p class="card-text text-custom-primary">
                            {!! $item->content !!}
                        </p>
                    </div>
                </div>

                @endforeach
            </div>
            <div class="slider-button-next text-warning" id="prodi-next"></div>
            <div class="slider-button-prev text-warning" id="prodi-prev"></div>
        </div>

    </div>
</div>

<div class="container py-4">
    <hr class="section-line">
    <h4 class="text-custom-primary text-uppercase">
        Serunya Belajar di Asia
    </h4>
    @php
    $galeri = (object )[
    'assets/images/galeri/gallery1.jpg',
    'assets/images/galeri/gallery2.jpg',
    'assets/images/galeri/gallery3.jpg',
    'assets/images/galeri/gallery4.jpg',
    'assets/images/galeri/gallery5.jpg',
    'assets/images/galeri/gallery6.jpg'
    ]
    @endphp

    <div class="gallery-container mt-3 d-none">
        <img class="rounded-lg" src="{{ asset('assets/images/galeri/gallery1_ftd.jpg') }}" alt="">
        <img class="rounded-lg" src="{{ asset('assets/images/galeri/gallery2_ftd.png') }}" alt="">
        <img class="rounded-lg" src="{{ asset('assets/images/galeri/gallery3_ftd.png') }}" alt="">
        <img class="rounded-lg" src="{{ asset('assets/images/galeri/gallery4_ftd.jpg') }}" alt="">
    </div>
    <div class="row justify-content-center mt-1 d-none d-md-flex">
        <div class="col-lg-6">
            <img class="rounded-lg" style="cursor: pointer;" src="{{ asset('assets/images/galeri/gallery5_ftd.jpg') }}"
                alt="" onclick="window.open('https://www.youtube.com/channel/UClmjiKomfiJlWI3tM1kU5hg', '_blank')">
            <h6 class="card-title content-ellipsis article-title mt-3">SEKILAS PERJALANAN RISA SANTOSO SEBELUM MENJADI
                REKTOR TERMUDA DI INDONESIA</h6>
        </div>
        <div class="col-lg-6">
            <img class="rounded-lg" style="cursor: pointer;" src="{{ asset('assets/images/galeri/gallery6_ftd.jpg') }}"
                alt="" onclick="window.open('https://www.youtube.com/channel/UClmjiKomfiJlWI3tM1kU5hg', '_blank')">
            <h6 class="card-title content-ellipsis article-title mt-3">MAU TAU PENDAPAT MEREKA TENTANG KAMPUS ASIA?
                SIMAK SAMPAI SELESAI VIDEONYA GAESS</h6>
        </div>
    </div>

    <div class="mt-3 d-md-none overflow-hidden">
        <div class="swiper" id="swiper-gallery">
            <div class="swiper-wrapper">
                <div class="swiper-slide mx-2">
                    <img class="rounded-lg w-100" src="{{ asset('assets/images/galeri/gallery1.jpg') }}" alt=""
                        height="auto">
                </div>
                <div class="swiper-slide mx-2">
                    <img class="rounded-lg w-100" src="{{ asset('assets/images/galeri/gallery4.jpg') }}" alt=""
                        height="auto">
                </div>
            </div>
            <div class="slider-button-prev text-white" id="gallery-prev"></div>
            <div class="slider-button-next text-white" id="gallery-next"></div>
        </div>
    </div>
</div>

<div class="container px-2 mt-3">
    <div class="row justify-content-center px-3 p-lg-0">

        <div class="swiper" id="swiper-prodi">
            <div class="swiper-wrapper">
                @foreach ($berita as $item)
                <div class="card border-0 swiper-slide px-0 my-1 mx-2">
                    <img src="{{ $item->image }}" class="card-img-top">
                    <hr class="article-line my-4">
                    <div class="card-body px-0 pt-lg-0">
                        <h6 class="card-title content-ellipsis article-title">{{ $item->title }}</h6>
                        <p class="card-text content-ellipsis">
                            {!! $item->content !!}
                        </p>
                        <a href="#" class="text-custom-primary">Read More</a>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="slider-button-next text-warning" id="prodi-next"></div>
            <div class="slider-button-prev text-warning" id="prodi-prev"></div>
        </div>

    </div>
</div>



<div class="container py-4">
    <hr class="section-line">
    <h4 class="text-custom-primary text-uppercase">
        Kata Mereka Tentang Asia
    </h4>
    <div class="swiper col-md-12 px-0" id="swiper-testimoni">
        <div class="swiper-wrapper">
            @php
            $testimoni = [
            'assets/images/testi/testi1.png',
            'assets/images/testi/testi2.png'
            ]
            @endphp

            @foreach ($testimoni as $item)
            <div class="swiper-slide">
                <img src="{{ asset($item) }}" alt="" width="100%">
            </div>
            @endforeach
        </div>
        <div class="slider-button-prev slider-testimoni text-warning" id="testimoni-prev"></div>
        <div class="slider-button-next slider-testimoni text-warning" id="testimoni-next"></div>
    </div>
</div>

<div>
    <img class="w-100" src="{{ asset('assets/banner/banner-footer_ftd.webp') }}" alt="">
    <div class="bg-custom-primary d-flex flex-column py-">
        <div class="d-none d-lg-flex flex-column">
            <img class="col-10 mx-auto" src="{{ asset('assets/banner/banner-program1.webp') }}" alt="">
            <img class="col-10 mx-auto" src="{{ asset('assets/banner/banner-program2.webp') }}" alt="">
            <img class="col-10 mx-auto" src="{{ asset('assets/banner/banner-program4.webp') }}" alt="">
        </div>

        <div class="d-md-none">
            <div class="swiper px-3" id="swiper-new-prodi">
                <div class="swiper-wrapper">
                    @foreach ($prodi as $item)
                    <div class="card card-prodi col-lg-5 px-0 shadow swiper-slide bg-warning">
                        <img src="{{ asset($item->image) }}" class="card-img-top" alt="">
                        <div class="card-body d-flex flex-column justify-content-center text-center">
                            <h5 class=" card-title title-ellipsis">{{ $item->title }}</h5>
                            <p class="">
                                {!! $item->content !!}
                            </p>
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="slider-button-prev text-white" id="new-prodi-prev"></div>
                <div class="slider-button-next text-white" id="new-prodi-next"></div>
            </div>
        </div>

        <div class="mt-3 d-flex flex-column container">
            <h4 class="text-center text-white text-upppercase">
                Pendaftaran Mahasiswa Baru
            </h4>
            <p class="text-white col-12 col-md-8 text-center mx-auto">
                Bergabunglah bersama FEB Institut Asia sekarang, dan raihlah
                kesempatan memperoleh diskon biaya studi hingga beasiswa!
            </p>
            <div class="d-none d-lg-flex flex-lg-column">
                <img class="col-11 mx-auto my-3" src="{{ asset('assets/images/step-pendaftaran.png') }}" alt="">
                <div class="d-flex flex-row text-white mx-4">
                    <div class="col-3 step-up d-flex flex-column">
                        <div class="text-center">
                            <h4 class="text-uppercase">Langkah 1</h4>
                            <p>Membayar Biaya Pendaftaran</p>
                            <h5>Rp 150.000</h5>
                        </div>
                        <i class="fa-solid fa-caret-down fa-xl" style="cursor: pointer;"
                            onclick="toogleFunction('div-langkah1');"></i>
                        <div id="div-langkah1" class="mystyle" style="display:none;text-align: center;">
                            <div style="text-align: left;">
                                <h4 class="text-center">Mengisi Formulir Pendaftaran</h4>
                                <ul>
                                    <li>
                                        Datang ke kampus
                                    </li>
                                    <li>
                                        Melalui online pendaftaran
                                        <strong>asia.ac.id</strong>
                                    </li>
                                    <li>
                                        Atau melalui sekolah yang
                                        sudah bekerja sama dengan
                                        <strong>Institut Asia</strong>
                                    </li>
                                </ul>
                            </div>
                            <i class="fa-solid fa-caret-down fa-xl"></i>
                            <p class="text-center">
                                Menyerahkan fotocopy
                                ijazah minimal SLTA & foto
                                berwarna 3x4 sebanyak 4
                                lembar (bisa menyusul)
                            </p>
                        </div>
                    </div>
                    <div class="col-3 step-2 d-flex flex-column justify-content-center">
                        <div class="text-center">
                            <h4 class="text-uppercase">Langkah 2</h4>
                            <p>
                                Mengisi Formulir Daftar Ulang
                            </p>
                        </div>
                        <i class="fa-solid fa-caret-down fa-xl"></i>
                        <div class="text-center">
                            <p>
                                Membayar Biaya
                                Daftar Ulang
                            </p>
                        </div>
                    </div>
                    <div class="col-3 step-up">
                        <div class="text-center">
                            <h4 class="text-uppercase">Langkah 3</h4>
                            <p>
                                Mahasiswa Aktif
                            </p>
                        </div>
                    </div>
                    <div class="col-3 step-4">
                        <div class="text-center">
                            <h4 class="text-uppercase">Langkah 4</h4>
                            <p>
                                Kuliah di Kampus Asia
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="d-lg-none">
                <div class="swiper px-3" id="swiper-new-prodi">
                    <div class="swiper-wrapper">
                        <div class="col-lg-5 px-0 swiper-slide bg-custom-primary text-white d-flex flex-column">
                            <img src="{{ asset('assets/images/langkah/langkah1.png') }}" class="mx-auto mb-3" alt=""
                                width="60%">
                            <div class="d-flex flex-column justify-content-center text-center">
                                <div class="text-center">
                                    <h4 class="text-uppercase">Langkah 1</h4>
                                    <p>Membayar Biaya Pendaftaran</p>
                                    <h5>Rp 150.000</h5>
                                </div>
                                <i class="fa-solid fa-caret-down fa-xl"></i>
                                <div>
                                    <h4 class="text-center">Mengisi Formulir Pendaftaran</h4>
                                    <ul>
                                        <li>
                                            Datang ke kampus
                                        </li>
                                        <li>
                                            Melalui online pendaftaran
                                            <strong>asia.ac.id</strong>
                                        </li>
                                        <li>
                                            Atau melalui sekolah yang
                                            sudah bekerja sama dengan
                                            <strong>Institut Asia</strong>
                                        </li>
                                    </ul>
                                </div>
                                <i class="fa-solid fa-caret-down fa-xl"></i>
                                <p class="text-center">
                                    Menyerahkan fotocopy
                                    ijazah minimal SLTA & foto
                                    berwarna 3x4 sebanyak 4
                                    lembar (bisa menyusul)
                                </p>
                            </div>
                        </div>
                        <div class="col-lg-5 px-0 swiper-slide bg-custom-primary text-white d-flex flex-column">
                            <img src="{{ asset('assets/images/langkah/langkah2.png') }}" class="mx-auto mb-3" alt=""
                                width="60%">
                            <div class="d-flex flex-column justify-content-center text-center">
                                <div class="text-center">
                                    <h4 class="text-uppercase">Langkah 2</h4>
                                    <p>
                                        Mengisi Formulir Daftar Ulang
                                    </p>
                                </div>
                                <i class="fa-solid fa-caret-down fa-xl"></i>
                                <div class="text-center">
                                    <p>
                                        Membayar Biaya
                                        Daftar Ulang
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-5 px-0 swiper-slide bg-custom-primary text-white d-flex flex-column">
                            <img src="{{ asset('assets/images/langkah/langkah3.png') }}" class="mx-auto mb-3" alt=""
                                width="60%">
                            <div class="d-flex flex-column justify-content-center text-center">
                                <h4 class="text-uppercase">Langkah 3</h4>
                                <p>
                                    Mahasiswa Aktif
                                </p>
                            </div>
                        </div>
                        <div class="col-lg-5 px-0 swiper-slide bg-custom-primary text-white d-flex flex-column">
                            <img src="{{ asset('assets/images/langkah/langkah4.png') }}" class="mx-auto mb-3" alt=""
                                width="60%">
                            <div class="d-flex flex-column justify-content-center text-center">
                                <div class="text-center">
                                    <h4 class="text-uppercase">Langkah 4</h4>
                                    <p>
                                        Kuliah di Kampus Asia
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="slider-button-prev text-white" id="new-prodi-prev"></div>
                    <div class="slider-button-next text-white" id="new-prodi-next"></div>
                </div>
            </div>
        </div>
        <div class="d-flex flex-column my-2 mb-5">
            {{-- <div class="d-flex flex-column flex-lg-row container text-center text-lg-left">
                <div class="col-lg-6 px-md-5 mt-4 right-border">
                    <span class="bg-warning rounded-pill py-1 px-2 h6 ">
                        Periode Daftar Ulang
                    </span>
                    <div class="d-flex flex-row mt-4">
                        <div class="col-md-4">
                            <h4 class="text-white">AK/PBM</h4>
                            <h6 class="text-white mt-4">Sampai 5 Feb 2022</h6>
                            <h6 class="text-warning">Rp 6.500.000</h6>

                            <h6 class="text-white mt-4">6 Feb - 5 Apr 2022</h6>
                            <h6 class="text-warning">Rp 7.000.000</h6>

                            <h6 class="text-white mt-4">6 Apr - 5 Jun 2022</h6>
                            <h6 class="text-warning">Rp 7.500.000</h6>

                            <h6 class="text-white mt-4">6 Jun - 5 Agust 2022</h6>
                            <h6 class="text-warning">Rp 8.000.000</h6>

                            <h6 class="text-white mt-4">6 Agust - 15 Sept 2022</h6>
                            <h6 class="text-warning">Rp 8.500.000</h6>
                        </div>
                        <div class="col-md-4">
                            <h4 class="text-white">DKV</h4>
                            <h6 class="text-white mt-4">Sampai 5 Feb 2022</h6>
                            <h6 class="text-warning">Rp 9.750.000</h6>

                            <h6 class="text-white mt-4">6 Feb - 5 Apr 2022</h6>
                            <h6 class="text-warning">Rp 10.250.000</h6>

                            <h6 class="text-white mt-4">6 Apr - 5 Jun 2022</h6>
                            <h6 class="text-warning">Rp 10.750.000</h6>

                            <h6 class="text-white mt-4">6 Jun - 5 Agust 2022</h6>
                            <h6 class="text-warning">Rp 11.250.000</h6>

                            <h6 class="text-white mt-4">6 Agust - 15 Sept 2022</h6>
                            <h6 class="text-warning">Rp 11.750.000</h6>
                        </div>
                        <div class="col-md-4">
                            <h4 class="text-white">TI/SK</h4>
                            <h6 class="text-white mt-4">Sampai 5 Feb 2022</h6>
                            <h6 class="text-warning">Rp 8.00.000</h6>

                            <h6 class="text-white mt-4">6 Feb - 5 Apr 2022</h6>
                            <h6 class="text-warning">Rp 8.500.000</h6>

                            <h6 class="text-white mt-4">6 Apr - 5 Jun 2022</h6>
                            <h6 class="text-warning">Rp 9.000.000</h6>

                            <h6 class="text-white mt-4">6 Jun - 5 Agust 2022</h6>
                            <h6 class="text-warning">Rp 9.500.000</h6>

                            <h6 class="text-white mt-4">6 Agust - 15 Sept 2022</h6>
                            <h6 class="text-warning">Rp 10.000.000</h6>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 px-md-5 d-flex flex-column">
                    <div class="mt-4">
                        <span class="bg-warning rounded-pill py-1 px-2 h6 ">
                            SPP PER SEMESTER (1-7)
                        </span>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="mt-4">
                                    <h5 class="text-white mt-4">AK/PBM</h5>
                                    <h5 class="text-warning">Rp 3.200.000</h5>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mt-4">
                                    <h5 class="text-white mt-4">DKV</h5>
                                    <h5 class="text-warning">Rp 4.100.000</h5>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mt-4">
                                    <h5 class="text-white mt-4">TI/SK</h5>
                                    <h5 class="text-warning">Rp 3.650.000</h5>
                                </div>
                            </div>
                        </div>
                                   
                    </div>
                    <div class="mt-4">
                        <span class="bg-warning rounded-pill py-1 px-2 h6 ">
                            SPP PER SEMESTER 8 KEATAS
                        </span>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="mt-4">
                                    <h5 class="text-white mt-4">AK/PBM</h5>
                                    <h5 class="text-warning">Rp 1.500.000</h5>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mt-4">
                                    <h5 class="text-white mt-4">DKV</h5>
                                    <h5 class="text-warning">Rp 1.500.000</h5>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mt-4">
                                    <h5 class="text-white mt-4">TI/SK</h5>
                                    <h5 class="text-warning">Rp 1.500.000</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> --}}
            <a href="https://pendaftaranonline.asia.ac.id/"
                class="btn btn-warning rounded-pill px-3 mt-3 mx-auto col-6 col-md-4 col-lg-2">
                Daftar Sekarang
            </a>
        </div>
    </div>
</div>
@endsection

@section('script')
<script src="https://unpkg.com/swiper@8/swiper-bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"
    integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script>
function toogleFunction(nama_id) {
    $("#" + nama_id).toggle();
}
const swiperProdi = new Swiper('#swiper-prodi', {
    direction: 'horizontal',
    loop: true,
    touchable: true,
    slidesPerView: window.innerWidth >= 768 ? 3 : 1,
    spaceBetween: 15,
    navigation: {
        nextEl: '#prodi-next',
        prevEl: '#prodi-prev',
    },
});

const swiperGallery = new Swiper('#swiper-gallery', {
    direction: 'horizontal',
    loop: true,
    touchable: true,
    slidesPerView: 2,
    spaceBetween: 15,
    navigation: {
        nextEl: '#gallery-next',
        prevEl: '#gallery-prev',
    },
});

const swiperTestimoni = new Swiper('#swiper-testimoni', {
    direction: 'horizontal',
    loop: true,
    touchable: true,
    slidesPerView: window.innerWidth >= 768 ? 3 : 1,
    spaceBetween: 5,
    navigation: {
        nextEl: '#testimoni-next',
        prevEl: '#testimoni-prev',
    },
})

const swiperFasilitas = new Swiper('#swiper-mobile-fasilitas', {
    direction: 'horizontal',
    loop: true,
    touchable: true,
    slidesPerView: 1,
    spaceBetween: 15,
})

const swiperNewProdi = new Swiper('#swiper-new-prodi', {
    direction: 'horizontal',
    loop: true,
    touchable: true,
    slidesPerView: 1,
    spaceBetween: 20,
    navigation: {
        nextEl: '#new-prodi-next',
        prevEl: '#new-prodi-prev',
    },
})

const swiperStep = new Swiper('#swiper-step', {
    direction: 'horizontal',
    loop: false,
    touchable: true,
    slidesPerView: 1,
    spaceBetween: 15,
    navigation: {
        nextEl: '#step-next',
        prevEl: '#step-prev',
    },
})

$('.slick').slick({
    dots: false,
    infinite: true,
    variableWidth: true,
    prevArrow: "<img style='width:40px;height:40px;' class='a-left control-c prev slick-prev' src='assets/icon/kiri.png'>",
    nextArrow: "<img style='width:40px;height:40px;' class='a-right control-c next slick-next' src='assets/icon/kanan.png'>"
});
</script>
@endsection