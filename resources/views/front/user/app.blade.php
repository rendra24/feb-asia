<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
	<title>Form Login</title>
	<link rel="stylesheet" href="{{ URL::to('front/bootstrap/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<style type="text/css">
		.box{

			margin-top: 125px;
		}
		.well{
			background-color: #FFF;
			-webkit-box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 3px 1px -2px rgba(0,0,0,0.12), 0 1px 5px 0 rgba(0,0,0,0.2);
    			 box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 3px 1px -2px rgba(0,0,0,0.12), 0 1px 5px 0 rgba(0,0,0,0.2); 
		}
		body{
			background-color: #EFEFEE;
			    font-family: 'Open Sans', sans-serif;
		}
		h3{
			font-size: 22px;
			    font-weight: 300;
			    color: #555;
			    line-height: 30px;
			 margin-top: 0px;
			 margin-bottom: 15px;
		}
		.icon{
			  color: #888;
		}
		.input-group-addon{
			background-color: #FFF;
		}
		label{
			font-weight: 300;
		}
		.btn-primary{
			padding-top: 5px;
			padding-bottom: 5px;
			font-weight: 500;
			font-size: 15px;
			border-radius: 0px;
		}
		.help-block {
		    margin-top: 0px;
		    color: #737373;
		    font-style: italic;
		    font-size: 13px;
		}
	</style>
</head>
<body>
	@yield('content')

	<script type="text/javascript" src="{{ URL::to('/front/bootstrap/js/jquery-2.1.1.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::to('/front/bootstrap/js/bootstrap.min.js') }}"></script>
</body>
</html>