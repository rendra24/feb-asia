@extends('front.user.app')

@section('content')
	<div class="container-fluid  box">
		<div class="col-md-4 col-md-offset-4">
                   @if(Session::has('message'))
                        <div class="alert alert-danger alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <strong><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> </strong> {{ Session::get('message') }}
                        </div>    
                    @endif  
			<div class="well ">
				<div class="text-center">
					<img src="{{ URL::to('front/images/stmik.png') }}" alt="" class="img-responsive" style="margin-left: auto;margin-right: auto;height: 175px;">
					<h3>Please Login</h3>
				</div>
				<form action="{{ route('doLogin') }}" method="post">
					{{ csrf_field() }}
					<div class="form-group {{ ($errors->has('email')? 'has-error has-feedback':'') }}">
						<div class="input-group ">
						  	<span class="input-group-addon" id="basic-addon1">
						  		<i class="fa fa-envelope-o" aria-hidden="true"></i>
						  	</span>
						  	<input type="email" name="email" class="form-control" placeholder="Email" value="{{ old('email') }}" >
						</div>
						  	@if($errors->has('email'))
								<span class="help-block"> {{$errors->first('email')}}</span>
						  	@endif
					</div>
					<div class="form-group  {{ ($errors->has('password')? 'has-error':'') }}">
						<div class="input-group">
						  	<span class="input-group-addon">
						  		<i class="fa fa-key" aria-hidden="true"></i>
						  	</span>
						  	<input type="password" name="password" class="form-control" placeholder="Password" >
						</div>
						  	@if($errors->has('password'))
								<span class="help-block" > {{$errors->first('password')}}</span>
						  	@endif
					</div>
					<div class="form-group">
							<button class="btn btn-primary btn-block" type="submit">LOGIN</button>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection