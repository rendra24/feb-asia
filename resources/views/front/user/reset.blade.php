@extends('front.user.app')

@section('content')
	<div class="container-fluid box">
		<div class="col-md-4 col-md-offset-4">

                   @if(Session::has('message'))
                        <div class="alert {{ (Session::get('status')=='failed')? 'alert-danger':'alert-success' }} alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <strong><i class="fa {{ (Session::get('status')=='failed')? 'fa-exclamation-triangle':'fa-check-square-o' }} " aria-hidden="true"></i> </strong> {{ Session::get('message') }} 
                              @if(Session::get('status')=='success')
                              <a href="{{ route('login') }}">, Kembali ke login</a>
                              @endif
                        </div>    
                    @endif  

			<div class="well ">
				<div class="text-center">
					<i class="fa fa-user-circle-o fa-5x icon" aria-hidden="true" style=""></i>
					<h3>Lupa Password</h3>
				</div>
				<form action="{{ route('doReset') }}" method="post">
					{{ csrf_field() }}
					<div class="form-group {{ ($errors->has('email')? 'has-error has-feedback':'') }}">
						<div class="input-group ">
						  	<span class="input-group-addon" id="basic-addon1">
						  		<i class="fa fa-envelope-o" aria-hidden="true"></i>
						  	</span>
						  	<input type="email" name="email" class="form-control" placeholder="Email" value="{{ old('email') }}" >
						</div>
						  	@if($errors->has('email'))
								<span class="help-block"> {{$errors->first('email')}}</span>
						  	@endif
					</div>
					
					<div class="row">
						<div class="col-sm-8 col-sm-offset-2" >
							<button class="btn btn-primary btn-block" type="submit">Send</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection