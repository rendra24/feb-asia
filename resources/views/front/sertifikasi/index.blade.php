@extends('front.layout.app')
@section('title', 'Sertifikasi FEB ASIA')
@section('description', strip_tags($visimisi->content))
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 well well-white mt-50 min-700">
            <!--
			<img src="{{ URL::to('/assets/static/images/stmik.png') }}" alt="" class="img-responsive img-stmik">
			-->
            <h1 class="title-content weight-7" style="margin-bottom: 20px;">SKEMA SERTIFIKASI
                FAKULTAS TEKNOLOGI DAN DESAIN</h1>
            {!! $visimisi->content !!}

            <img src="assets/images/logo-kerjasama.png" alt="" style="width: 100%;">
        </div>
    </div>
</div>
@endsection