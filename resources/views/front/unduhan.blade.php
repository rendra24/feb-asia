@extends('front.layout.app')
@section('title', 'UNDUHAN FORM')
@push('style')
	<style type="text/css">
		.box-unduhan{
			padding: 15px;
			min-height: 500px;
		}
		.box-unduhan .item{
			font-size: 13.5pt;
			border-bottom: 1px solid #d7d7d7 !important;
			padding-top: 7px;
			padding-bottom: 8px;
			color: #333346 !important
		}
		.box-unduhan .item a{
			color: #333346 !important;
			cursor: pointer;
		}
		.box-unduhan .item a:hover, .box-unduhan .item a:focus{
			color: #EF5F5A !important;
		}
	</style>
@endpush
@section('content')
<div class="container min-700">
	<div class="row">
		<div class="col-md-10 col-md-offset-1 well well-white mt-50">
			<div class="text-center">
				<h1 class="title-content weight-6">UNDUHAN</h1>
			</div>
			<div class="box-unduhan">
				@foreach($unduhan as $data)
					<div class="item">
						<a href="{{ route('unduhan-download', $data->id) }}">{{ $data->title }}</a>
					</div>
				@endforeach
			</div>
			<div class="text-center">
				{{ $unduhan->links() }}
			</div>
		</div>
	</div>
</div>
@endsection