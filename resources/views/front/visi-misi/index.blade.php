@extends('front.layout.app')
@section('title', 'Visi & Misi FEB ASIA')
@section('description', strip_tags($visimisi->content))
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 well well-white mt-50 min-700">
            <!--
			<img src="{{ URL::to('/assets/static/images/stmik.png') }}" alt="" class="img-responsive img-stmik">
			-->
            <h1 class="title-content weight-7" style="margin-bottom: 20px;">Visi & Misi Fakultas Teknologi dan Desain
                Institut Asia Malang</h1>
            {!! $visimisi->content !!}
        </div>
    </div>
</div>
@endsection