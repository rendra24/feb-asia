@extends('front.layout.app')
@section('title', 'Kalender Akademik')
@push('style')
	<link href="{{ URL::to('/assets/front/fullcalendar/fullcalendar.min.css') }}" rel='stylesheet' />
	<link href="{{ URL::to('/assets/front/fullcalendar/fullcalendar.print.min.css') }}" rel='stylesheet' media='print' />
@endpush
@section('content')
<div class="container min-700">
	<div class="row">
		<div class="col-md-12 well well-white mt-50">
			<div class="text-center">
				<h1 class="title-content weight-7">KALENDER AKADEMIK</h1>
			</div>
			<div class="box-kalender" style="margin-top: 30px;">
				<div id='calendar'></div>
			</div>
		</div>
	</div>
</div>
<!-- Button trigger modal -->

@endsection
@include('front.kalender.calendar')