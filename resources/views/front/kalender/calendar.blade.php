@push("scripts")
	<script src="{{ URL::to('/assets/front/fullcalendar/moment.min.js') }}"></script>
	<script src="{{ URL::to('/assets/front/fullcalendar/fullcalendar.min.js') }}"></script>
	<script src="{{ URL::to('/assets/front/fullcalendar/locale-all.js') }}"></script>
<script>

	$(document).ready(function() {

		$('#calendar').fullCalendar({
			header: {
	                    left: 'title',
	                    right: 'month,listMonth, prev,next'
	                },
	            locale: 'id',
	            columnFormat: 'dddd',
			defaultDate: Date.now(),
			editable: true,
			// eventLimit: true, // allow "more" link when too many events
			events: "{{ route('kalender-event') }}",
			timeFormat: 'H(:mm)',
		});
		$('#calendar').on('click', '.fc-prev-button', function() {
			var value = $('#calendar').fullCalendar('getDate');
			var date = new Date(value);
			loadEvents(date.getMonth()+1);
			
		});
		$('#calendar').on('click', '.fc-next-button', function() {
			var value = $('#calendar').fullCalendar('getDate');
			var date = new Date(value);
			loadEvents(date.getMonth()+1);
		});

		function loadEvents(month){
				$.ajax({
		                    type: "GET",
		                    url: "{{ route('kalender-month') }}",
		                    data: {month:month},
		                    success: function (data) {
		                        $('#calendar').fullCalendar('removeEvents');
		                    	$.each(data, function( index, value ) {
						  	        $('#calendar').fullCalendar( 'renderEvent',value );
						        });
		                    	
		                   }});
			
		}
		
	});

</script>
@endpush