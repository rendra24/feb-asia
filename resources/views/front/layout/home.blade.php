<!DOCTYPE html>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
	<title>HOME FEB INSTITUT ASIA</title>
	<meta name="description" content="Menjadi Perguruan Tinggi yang unggul dalam membentuk sarjana komputer profesional, memiliki pribadi tangguh, dan mampu bersaing di era global">
<meta name="google-site-verification" content="Gf-VAzYDwQ95p-CzT18qv45u0882C-_PGnP2FBN0yxQ" />

	<meta name="author" content="FEB ASIA MALANG">
	<meta property="og:title" content="HOME FEB ASIA MALANG" /> 
	<meta property="og:type" content="article" /> 
	<meta property="og:url" content="{{ url()->current() }}" /> 
	<meta property="og:image" content="{{ URL::to('/assets/front/images/stmik.png') }}" /> 
	<meta property="og:description" content="Menjadi Perguruan Tinggi yang unggul dalam membentuk sarjana komputer profesional, memiliki pribadi tangguh, dan mampu bersaing di era global" /> 
	<meta property="og:site_name" content="FEB INSTITUT ASIA" /> 

	<meta name="twitter:site" content="{{ url()->current() }}"> 
	<meta name="twitter:title" content="HOME FEB ASIA MALANG"> 
	<meta name="twitter:description" content="Menjadi Perguruan Tinggi yang unggul dalam membentuk sarjana komputer profesional, memiliki pribadi tangguh, dan mampu bersaing di era global"> 
	<meta name="twitter:image:src" content="{{ URL::to('/assets/front/images/stmik.png') }}">

	<link rel="shortcut icon" href="{{ URL::to('/assets/static/images/favicon.png') }}">
	@include('front.layout.stylesheet')
</head>
	@include('front.layout.header')
	<div class="content">
	@yield('content')
	</div>
	@include('front.layout.footer')
</body>
</html>