<body id="page-top" class="index" cz-shortcut-listen="true">
    <div class="row top-nav">
        <div class="container">
            <div class="col-lg-3 col-sm-3">
                <p><i class="fa fa-envelope-o" style="font-size:15px;margin-right:5px;"></i> feb@asia.ac.id</p>
                <p>
                </p>
            </div>
            <div class="col-lg-4 col-sm-4">
                <p><i class="fa fa-map-marker" style="font-size:15px;margin-right:5px;"></i> Jalan Soekarno Hatta -
                    Rembuksari 1A, Malang, Jawa Timur</p>
                <p>
                </p>
            </div>

            <div class="col-lg-2 col-sm-2">
                <p><i class="fa fa-fax" style="font-size:15px;margin-right:5px;"></i> (0341) 4345225</p>
                <p>
                </p>
            </div>

            <div class="col-lg-2 col-sm-2" style="margin-left:-40px;">
                <p><i class="fa fa-phone" style="font-size:15px;margin-right:5px;"></i> (0341) 478877</p>
                <p>
                </p>
            </div>
        </div>
    </div>
    <!-- Navigation -->
    <nav id="mainNav" class="navbar navbar-default navbar-custom navbar-fixed-top affix-top ">
        <div class="container container-menu">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="" href="{{ route('home') }}"><img src="{{ URL::to('/assets/front/logo_asia.png') }}"
                        class="img-responsive img-logo"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="margin-top:10px;">
                <ul class="nav navbar-nav menu-header ">
                    <li class="hidden active">
                        <a href="{{ route('home') }}"></a>
                    </li>
                    <li>
                        <a class="page-scroll" href="{{ route('home') }}">Home</a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="menu-dropdown" data-toggle="dropdown" role="button" aria-haspopup="true"
                            aria-expanded="false">About<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ route('visi-misi') }}">Visi & Misi</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="{{ route('prodi','ti') }}">Teknik Informatika</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="{{ route('prodi','sk') }}">Sistem Komputer</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="{{ route('prodi','dkv') }}">Desain Komunikasi Visual</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="http://akademik.asia.ac.id/?h=4&vj=1&f=2&vakdj=1&idj=4">Akreditasi</a></li>

                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="menu-dropdown" data-toggle="dropdown" role="button" aria-haspopup="true"
                            aria-expanded="false">Akademik<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ route('kalender') }}">Kalender</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="{{ route('perkuliahan') }}">Perkuliahan</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="{{ route('ujian', 'pra-tugas-akhir') }}">Seminar Proposal</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="{{ route('ujian', 'tugas-akhir') }}">Skripsi</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="{{ route('ujian', 'praktek-kerja-lapangan') }}">Praktek KerjaLapangan</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="{{ route('jam-bimbingan') }}">Jam Bimbingan</a></li>


                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="menu-dropdown" data-toggle="dropdown" role="button" aria-haspopup="true"
                            aria-expanded="false">Link <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            @foreach(App\Link::all() as $data)
                            <li><a {!! 'href="' . $data->url . '"' . ($data->lnk_target == '_self' ? '' : ' target="' .
                                    $data->lnk_target . '"') !!}>{{ $data->name }}</a></li>
                            <li role="separator" class="divider"></li>
                            @endforeach
                            <li><a href="{{ route('pengumuman') }}">Pengumuman</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="{{ route('unduhan') }}">Unduhan</a></li>
                        </ul>
                    </li>
                    <li>
                        <a class="page-scroll" href="{{ route('sertifikasi-front') }}">Sertifikasi &
                            Partnerships</a>
                    </li>

                    <li class="dropdown">
                        <a href="#" class="menu-dropdown" data-toggle="dropdown" role="button" aria-haspopup="true"
                            aria-expanded="false">Lembaga <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="https://lp2m.asia.ac.id/">LP2M</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="https://asia.ac.id/usp/">USP</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="https://lpmi.asia.ac.id/">LPMI</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="https://inbis.asia.ac.id/">INBIS</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="https://kui.asia.ac.id/">KUI</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="https://careercenter.asia.ac.id/">CAREER CENTER</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="https://konseling.asia.ac.id/">KONSELING</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="https://arc.asia.ac.id/">ARC</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /.container-fluid -->
    </nav>

    <nav class="navbar navbar-default navbar-fixed-top navbar-mobile">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <a href="#menu-mobile" class="menu-link">
                    <i class="fa fa-bars fa-2x"></i>
                </a>
                <a class="" href="{{ route('home') }}"><img src="{{ URL::to('/assets/front/logo_asia.png') }}"
                        class="img-responsive img-logo"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>