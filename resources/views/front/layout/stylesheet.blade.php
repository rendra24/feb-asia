<link rel="stylesheet" href="{{ URL::to('/assets/front/bootstrap/css/bootstrap.css') }}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="{{ URL::to('/assets/front/agency.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ URL::to('/assets/front/style2.css') }}">
<link rel="stylesheet" type="text/css" href="{{ URL::to('/assets/front/bootstrap/css/style.css') }}">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/6.1.0/jquery.mmenu.all.css">
<style type="text/css">
	body{
		background-color: #ececec;
		font-family: 'Open Sans', sans-serif !important;
	}
	.mm-slideout { z-index:auto !important;}
</style>
@stack('style')