<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
	<title>@yield('title')</title>
	<meta name="description" content="@yield('description')">
	<meta name="author" content="FEB ASIA MALANG">
<meta name="google-site-verification" content="Gf-VAzYDwQ95p-CzT18qv45u0882C-_PGnP2FBN0yxQ" />

	<meta property="og:title" content="@yield('title')" /> 
	<meta property="og:type" content="article" /> 
	<meta property="og:url" content="{{ url()->full() }}" /> 
	<meta property="og:image" content="@yield('meta_image', URL::to('assets/static/images/stmik.png'))" /> 
	<meta property="og:description" content="@yield('description')" /> 
	<meta property="og:site_name" content="STMIK ASIA MALANG" /> 
	
	<meta name="twitter:site" content="{{ url()->full() }}"> 
	<meta name="twitter:title" content="@yield('title')"> 
	<meta name="twitter:description" content="@yield('description')"> 
	@if(Request::segment(1)=='berita')
		<meta name="twitter:image:src" content>
	@else
		<meta name="twitter:image:src" content="@yield('meta_image', URL::to('assets/static/images/stmik.png'))">
	@endif
	
	<link rel="shortcut icon" href="{{ URL::to('/assets/static/images/favicon.png') }}">
	@include('front.layout.stylesheet')
	@yield('stylesheet')
</head>
	@include('front.layout.header')
	<div class="content">
	<header class="page"></header>
	@yield('content')
	</div>
	@include('front.layout.footer')
</body>
</html>