<footer>
    <span class="copyright">Copyright © www.feb.asia.ac.id
        <script>
            document.write(new Date().getFullYear())
        </script>
    </span>
</footer>
<nav id="menu-mobile" style="z-index: 8000;">
    <ul>
        <li class="hidden active">
            <a href="{{ route('home') }}"></a>
        </li>
        <li>
            <a class="page-scroll" href="{{ route('home') }}">Beranda</a>
        </li>
        <li>
            <a class="page-scroll" href="{{ route('visi-misi') }}">Visi & Misi</a>
        </li>
        <li>
            <a class="page-scroll" href=" {{ route('kalender') }}">Kalender</a>
        </li>
        <li>
            <a class="page-scroll" href="{{ route('unduhan') }}">Unduhan</a>
        </li>
        <li>
            <a class="page-scroll" href="{{ route('perkuliahan') }}">Perkuliahan</a>
        </li>
        <li><span>Jadwal Ujian</span>
            <ul>
                <li><a href="{{ route('ujian', 'pra-tugas-akhir') }}">Pra Tugas Akhir</a></li>
                <li><a href="{{ route('ujian', 'tugas-akhir') }}">Tugas Akhir</a></li>
            </ul>
        </li>
        <li><span>Link</span>
            <ul>
                @foreach(App\Link::all() as $data)
                <li><a href="{{ $data->url }}" target="_blank">{{ $data->name }}</a></li>
                @endforeach
            </ul>
        </li>
        <li><span>Lain - lain</span>
            <ul>
                <li><a href="{{ route('pengumuman') }}">Pengumuman</a></li>
                <li><a href="{{ route('berita') }}">Berita</a></li>
                <li><a href="{{ route('profil-dosen') }}">Profil Dosen</a></li>
                <li><a href="{{ route('jam-bimbingan') }}">Jam Bimbingan</a></li>
            </ul>
        </li>
    </ul>
</nav>
<script type="text/javascript" src="{{ URL::to('/assets//front/bootstrap/js/jquery-2.1.1.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::to('/assets//front/bootstrap/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::to('/assets//front/agency.min.js') }}"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/6.1.0/jquery.mmenu.all.js">
</script>
<script type="text/javascript">
    $(function() {
        $('nav#menu-mobile').mmenu({
            extensions	: [ 'fx-menu-slide', 'shadow-page', 'shadow-panels', 'listview-large', 'pagedim-white' ],
            iconPanels	: true,
            counters	: true,
            keyboardNavigation : {
                enable	: true,
                enhance	: true
            },
            searchfield : {
                placeholder	: 'Search menu items'
            },
            navbar : {
                title : 'MENU'
            },
            navbars	: [
                {
                    position	: 'top',
                    content		: [ 'breadcrumbs', 'close' ]
                }
            ]
        });
    });
</script>
@stack('scripts')