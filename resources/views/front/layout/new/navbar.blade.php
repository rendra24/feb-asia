<nav class="navbar navbar-expand-lg navbar-light bg-custom-primary d-flex flex-row align-items-center">
    <div class="container">
        <a class="navbar-brand" href="#">
            <img src="{{ asset('/assets/front/logo.png') }}" alt="" width="100px" height="auto">
        </a>
        <div class="d-none d-md-block collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item mr-2">
                    <a class="nav-link text-white" href="/">Home</a>
                </li>
                <li class="nav-item dropdown dropleft mr-2">
                    <a id="link-item" class="nav-link text-white" href="#" role="button" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">About</a>

                    <div class="dropdown-menu" aria-labelledby="link-item">
                        <h6 class="dropdown-header">Sub-Menu</h6>
                        <a class="dropdown-item" href="{{ route('visi-misi') }}">Visi & Misi</a>
                        <a class="dropdown-item" href="{{ route('prodi','ti') }}">Teknik Informatika</a>
                        <a class="dropdown-item" href="{{ route('prodi','sk') }}">Sistem Komputer</a>
                        <a class="dropdown-item" href="{{ route('prodi','dkv') }}">Desain Komunikasi Visual</a>
                        <a class="dropdown-item"
                            href="http://akademik.asia.ac.id/?h=4&vj=1&f=2&vakdj=1&idj=4">Akreditasi</a>
                    </div>
                </li>
                <li class="nav-item dropdown dropleft mr-2">
                    <a id="link-item" class="nav-link text-white" href="#" role="button" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">Akademik</a>

                    <div class="dropdown-menu" aria-labelledby="link-item">
                        <h6 class="dropdown-header">Sub-Menu</h6>
                        <a class="dropdown-item" href="{{ route('kalender') }}">Kalender</a>
                        <a class="dropdown-item" href="{{ route('perkuliahan') }}">Perkuliahan</a>
                        <a class="dropdown-item" href="{{ route('ujian', 'pra-tugas-akhir') }}">Seminar Proposal</a>
                        <a class="dropdown-item" href="{{ route('ujian', 'tugas-akhir') }}">Skripsi</a>
                        <a class="dropdown-item" href="{{ route('ujian', 'praktek-kerja-lapangan') }}">Praktek Kerja
                            Lapangan</a>
                        <a class="dropdown-item" href="{{ route('jam-bimbingan') }}">Jam Bimbingan</a>

                    </div>
                </li>
                <li class="nav-item dropdown dropleft mr-2">
                    <a id="link-item" class="nav-link text-white" href="#" role="button" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">Link</a>
                    <div class="dropdown-menu" aria-labelledby="link-item">
                        <h6 class="dropdown-header">Sub-Menu</h6>
                        @foreach(App\Link::all() as $data)
                        <a class="dropdown-item" {!! 'href="' . $data->url . '"' . ($data->lnk_target == '_self' ? '' :
                            '
                            target="' .
                            $data->lnk_target . '"') !!}>{{ $data->name }}</a>
                        @endforeach
                        <a class="dropdown-item" href="{{ route('pengumuman') }}">Pengumuman</a>
                        <a class="dropdown-item" href="{{ route('unduhan') }}">Unduhan</a>
                    </div>
                </li>
                <!-- <li class="nav-item dropdown dropleft mr-2">
                    <a id="link-item" class="nav-link text-white" href="#" role="button" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">Sertifikasi & Partnerships</a>
                    <div class="dropdown-menu" aria-labelledby="link-item">
                        <h6 class="dropdown-header">Sub-Menu</h6>
                        <a href="#" class="dropdown-item">Sertifikasi</a>
                        <a href="#" class="dropdown-item">Logo Partnerships</a>
                    </div>
                </li> -->
                <li class="nav-item mr-2">
                    <a class="nav-link text-white" href="{{ route('sertifikasi-front') }}">Sertifikasi &
                        Partnerships</a>
                </li>
                <li class="nav-item dropdown dropleft">
                    <a id="link-item" class="nav-link text-white" href="#" role="button" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">Lembaga</a>
                    <div class="dropdown-menu" aria-labelledby="link-item">
                        <h6 class="dropdown-header">Sub-Menu</h6>
                        <!-- <a href="https://feb.asia.ac.id/pengumuman" class="dropdown-item">Pengumuman</a>
                        <a href="https://feb.asia.ac.id/berita" class="dropdown-item">Berita</a>
                        <a href="https://feb.asia.ac.id/profil-dosen" class="dropdown-item">Profil Dosen</a>
                        <a href="https://feb.asia.ac.id/jam-bimbingan" class="dropdown-item">Jam Bimbingan</a> -->
                        <a href="https://lp2m.asia.ac.id/" class="dropdown-item">LP2M</a>
                        <a href="https://asia.ac.id/usp/" class="dropdown-item">USP</a>
                        <a href="https://lpmi.asia.ac.id/" class="dropdown-item">LPMI</a>
                        <a href="https://inbis.asia.ac.id/" class="dropdown-item">INBIS</a>
                        <a href="https://kui.asia.ac.id/" class="dropdown-item">KUI</a>
                        <a href="https://careercenter.asia.ac.id/" class="dropdown-item">CAREER CENTER</a>
                        <a href="https://konseling.asia.ac.id/" class="dropdown-item">KONSELING</a>
                        <a href="https://arc.asia.ac.id/" class="dropdown-item">ARC</a>
                    </div>
                </li>
            </ul>
        </div>
        <button
            class="btn bg-transparent container d-flex flex-column justify-content-center align-items-center d-md-none col-1 p-0 m-0"
            id="burger-btn">
            <span class="w-100 bg-white" style="height: 1px"></span>
            <span class="w-100 bg-white mt-2" style="height: 1px"></span>
            <span class="w-100 bg-white mt-2" style="height: 1px"></span>
        </button>
    </div>
</nav>
<div class="mobile-menu p-3" id="burger-menu">
    <div class="d-flex flex-column py-3 align-items-end" id="burger-close">
        <span class="burger-close"></span>
        <span class="burger-close"></span>
    </div>
    <div class="menu-section">
        <div class="burger-item">
            <a href="/">Home</a>
        </div>
        <div class="burger-item">
            <a href="#sub-about" role="button" data-toggle="collapse" aria-expanded="false"
                aria-controls="sub-about">About</a>
            <div class="collapse sub-wrapper" id="sub-about">
                <a class="sub-burger" href="{{ route('visi-misi') }}">Visi & Misi</a>
                <a class="sub-burger" href="{{ route('prodi','ti') }}">Akutansi</a>
                <a class="sub-burger" href="{{ route('prodi','sk') }}">Professional Bussiness Management</a>
                <a class="sub-burger" href="http://akademik.asia.ac.id/?h=4&vj=1&f=2&vakdj=1&idj=4">Akreditasi</a>
            </div>
        </div>
        <div class="burger-item">
            <a href="#sub-akademik" role="button" data-toggle="collapse" aria-expanded="false"
                aria-controls="sub-akademik">Akademik</a>
            <div class="collapse sub-wrapper" id="sub-akademik">
                <a class="sub-burger" href="{{ route('kalender') }}">Kalender</a>
                <a class="sub-burger" href="{{ route('perkuliahan') }}">Perkuliahan</a>
                <a class="sub-burger" href="{{ route('ujian', 'pra-tugas-akhir') }}">Seminar Proposal</a>
                <a class="sub-burger" href="{{ route('ujian', 'tugas-akhir') }}">Skripsi</a>
                <a class="sub-burger" href="{{ route('ujian', 'praktek-kerja-lapangan') }}">Praktek Kerja
                    Lapangan</a>
                <a class="sub-burger" href="{{ route('jam-bimbingan') }}">Jam Bimbingan</a>

            </div>
        </div>
        <div class="burger-item">
            <a href="#sub-link" role="button" data-toggle="collapse" aria-expanded="false"
                aria-controls="sub-link">Link</a>
            <div class="collapse sub-wrapper" id="sub-link">

                @foreach(App\Link::all() as $data)
                <a class="sub-burger" {!! 'href="' . $data->url . '"' . ($data->lnk_target == '_self' ? '' :
                    '
                    target="' .
                    $data->lnk_target . '"') !!}>{{ $data->name }}</a>
                @endforeach
                <a class="sub-burger" href="{{ route('pengumuman') }}">Pengumuman</a>
                <a class="sub-burger" href="{{ route('unduhan') }}">Unduhan</a>
            </div>
        </div>
        <div class="burger-item">
            <a href="/">Sertifikasi & Partnerships</a>
        </div>
    </div>
</div>