<footer class="bg-custom-primary pb-4">
    <div class="container text-white row mx-auto align-items-center">
        <div class="col-md-6">
            <iframe
                src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15806.36067253063!2d112.6266231!3d-7.937799!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xa2178e9d5a420380!2sInstitut%20Asia%20Malang!5e0!3m2!1sen!2sid!4v1641052248633!5m2!1sen!2sid"
                width="100%" height="323" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
        </div>
        <div class="col-md-6 py-3 py-lg-0">
            <h5 class="text-center text-md-left">
                Lokasi Sangat Strategis di Pusat Kota Malang
            </h5>
            <h5 class="mt-4 text-center text-md-left">
                KAMPUS 1 & 2 :
            </h5>
            <div class="d-flex flex-row align-items-center mt-2 mx-3">
                <i class="fa-solid fa-location-dot fa-xl"></i>
                <p class="mb-0 mx-3">
                    Jl. Soekarno Hatta <br> Rembuksari 1A Malang, Jawa Timur
                </p>
            </div>
            <div class="row align-items-center my-3 mx-3">
                <i class="fa-solid fa-phone fa-xl"></i>
                <p class="mb-0 mx-3">
                    (0341) 478877
                </p>
            </div>
            <h5 class="text-center text-md-left">Social Media</h5>
            <div class="row px-2 d-flex flex-row justify-content-center justify-content-lg-start">
                <a href="https://www.instagram.com/institut_asia/" class="text-white" target="_blank"><i class="fa-brands fa-instagram fa-xl mx-2"></i></a>
                <a href="https://web.facebook.com/institutasiamalang" class="text-white" target="_blank"><i class="fa-brands fa-facebook fa-xl mx-2"></i></a>
                <a href="https://api.whatsapp.com/send/?phone=6287881444088&text=Halo+WA+Center+Institute+Asia+Malang%2C+Saya+mau+tanya...&app_absent=0" class="text-white" target="_blank"><i class="fa-brands fa-whatsapp fa-xl mx-2"></i></a>
                <a href="https://www.youtube.com/channel/UClmjiKomfiJlWI3tM1kU5hg" class="text-white" target="_blank"><i class="fa-brands fa-youtube fa-xl mx-2"></i></a>
                <a href="https://www.linkedin.com/school/perguruan-tinggi-asia/" class="text-white" target="_blank"><i class="fa-brands fa-linkedin fa-xl mx-2"></i></a>
                <a href="https://www.tiktok.com/@institutasia" class="text-white" target="_blank"><i class="fa-brands fa-tiktok fa-xl mx-2"></i></a>
            </div>
        </div>
    </div>
</footer>