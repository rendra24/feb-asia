@extends('front.layout.app')
@section('title', 'Program Studi '.$prodi->description)
@section('description', strip_tags($prodi->Profil->content))
@section('content')
<div class="container-fluid well well-white  min-700" style="margin-bottom: 0px;padding-bottom: 50px;">
    <div class="row mt-50">
        <div class="col-md-8 col-md-offset-2 ">
            <div class="row">
                <!--
				<div class="col-md-3 ">
					<img src="{{ URL::to('/assets/static/images/stmik.png') }}" alt="" class="img-responsive"> 
				</div>	
				-->
                <div class="col-md-9 ">
                    <h1 style="margin-top: 10px;margin-bottom: 20px; font-size: 25pt;">
                        Program Studi {{ $prodi->description }}
                    </h1>
                    {!! $prodi->Profil->content !!}
                </div>
            </div>
            <div class="mt-30">
                {!! $prodi->Visimisi->content !!}
            </div>
        </div>
    </div>
</div>
@endsection