@extends('front.layout.app')
@section('title', 'HACKATHON STMIK ASIA')
@section('content')
    <div class="container min-700">
        <div class="row">
            <div class="col-md-12 well well-white mt-50">
                <div class="text-center">
                    <h1 class="title-content weight-7">Informasi Hackathon STMIK ASIA</h1>
                    
                    <p></p>
                    
                    <img src="{{ URL::to('/assets/static/images/hackathon.png') }}" width="300"  height="400" alt="" >
                </div>
                
                
            </div>
        </div>
    </div>
@endsection