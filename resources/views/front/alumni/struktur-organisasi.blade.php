@extends('front.layout.app')
@section('title', 'ALUMNI STMIK ASIA')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('css/alumni/custom.css') }}">
@endsection
@section('content')
    <div class="container min-700">
        <div class="row">
            <div class="col-md-12 well well-white mt-50">
                @include('front.alumni.menu')
                <div class="text-center">
                    <h1 class="title-content weight-7">ALUMNI STMIK ASIA</h1>
                </div>
                <h3><strong>STRUKTUR ORGANISASI</strong></h3>
                <h3><strong>Dewan Pengurus Himpunan Alumni STMIK Asia Malang</strong></h3>
                <table class="MsoTableGrid" style="margin-left: 5.4pt; border-collapse: collapse; border: none; mso-border-alt: solid windowtext .5pt; mso-yfti-tbllook: 1184; mso-padding-alt: 0cm 5.4pt 0cm 5.4pt;" border="1" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr>
                        <td style="width: 467.8pt; border: solid windowtext 1.0pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" colspan="2" valign="top" width="624">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><b><span lang="EN-US" style="font-size: 12.0pt;">DEWAN PENGURUS HIMPUNAN ALUMNI STMIK ASIA MALANG</span></b></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 467.8pt; border: solid windowtext 1.0pt; border-top: none; mso-border-top-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" colspan="2" valign="top" width="624">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><b><span lang="EN-US" style="font-size: 12.0pt;">PENGURUS INTI</span></b></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 234.0pt; border: solid windowtext 1.0pt; border-top: none; mso-border-top-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="312">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt;">Ketua Umum</span></p>
                        </td>
                        <td style="width: 233.8pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="312">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt;">Jaenal Arifin, S.Kom., M.M., M.Kom.</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 234.0pt; border: solid windowtext 1.0pt; border-top: none; mso-border-top-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="312">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt;">Sekretaris Jenderal</span></p>
                        </td>
                        <td style="width: 233.8pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="312">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt;">Ida Wahyuni, S.Kom., M.Kom.</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 234.0pt; border: solid windowtext 1.0pt; border-top: none; mso-border-top-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="312">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt;">Wakil Sekjen 1</span></p>
                        </td>
                        <td style="width: 233.8pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="312">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt;">Bambang Tri Wahyo Utomo, S.Kom.</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 234.0pt; border: solid windowtext 1.0pt; border-top: none; mso-border-top-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="312">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt;">Wakil Sekjen 2</span></p>
                        </td>
                        <td style="width: 233.8pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="312">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt;">Tria Aprilianto, S.Kom., M.Kom.</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 234.0pt; border: solid windowtext 1.0pt; border-top: none; mso-border-top-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="312">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt;">Bendahara</span></p>
                        </td>
                        <td style="width: 233.8pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="312">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt;"> </span></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 234.0pt; border: solid windowtext 1.0pt; border-top: none; mso-border-top-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="312">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt;"> </span></p>
                        </td>
                        <td style="width: 233.8pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="312">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt;"> </span></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 467.8pt; border: solid windowtext 1.0pt; border-top: none; mso-border-top-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" colspan="2" valign="top" width="624">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><b><span lang="EN-US" style="font-size: 12.0pt;">DIVISI PENGEMBANGAN ORGANISASI &amp; KELEMBAGAAN</span></b></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 234.0pt; border: solid windowtext 1.0pt; border-top: none; mso-border-top-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="312">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt;">Ketua Divisi</span></p>
                        </td>
                        <td style="width: 233.8pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="312">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt;">Rizli Firdaus, S.Kom.</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 234.0pt; border: solid windowtext 1.0pt; border-top: none; mso-border-top-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="312">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt;">Anggota</span></p>
                        </td>
                        <td style="width: 233.8pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="312">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt;">Riza Fathu Uzzi, S.Kom.</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 234.0pt; border: solid windowtext 1.0pt; border-top: none; mso-border-top-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="312">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt;"> </span></p>
                        </td>
                        <td style="width: 233.8pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="312">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt;"> </span></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 234.0pt; border: solid windowtext 1.0pt; border-top: none; mso-border-top-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="312">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><b><span lang="EN-US" style="font-size: 12.0pt;">DIVISI PEMBERDAYAAN ALUMNI</span></b></p>
                        </td>
                        <td style="width: 233.8pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="312">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><b><span lang="EN-US" style="font-size: 12.0pt;"> </span></b></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 234.0pt; border: solid windowtext 1.0pt; border-top: none; mso-border-top-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="312">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt;">Ketua</span></p>
                        </td>
                        <td style="width: 233.8pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="312">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt;">Budi Dedy Fajar S, S.Kom, CH, CHT</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 234.0pt; border: solid windowtext 1.0pt; border-top: none; mso-border-top-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="312">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt;">Anggota</span></p>
                        </td>
                        <td style="width: 233.8pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="312">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt;">Hendrik Bayu, S.Kom.</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 234.0pt; border: solid windowtext 1.0pt; border-top: none; mso-border-top-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="312">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt;"> </span></p>
                        </td>
                        <td style="width: 233.8pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="312">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt;"> </span></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 467.8pt; border: solid windowtext 1.0pt; border-top: none; mso-border-top-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" colspan="2" valign="top" width="624">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><b><span lang="EN-US" style="font-size: 12.0pt;">DIVISI PENELITIAN DAN PENGEMBANGAN IPTEK</span></b></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 234.0pt; border: solid windowtext 1.0pt; border-top: none; mso-border-top-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="312">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt;">Ketua</span></p>
                        </td>
                        <td style="width: 233.8pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="312">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt;">Rina Dewi Indah Sari, S.Kom., M.Kom.</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 234.0pt; border: solid windowtext 1.0pt; border-top: none; mso-border-top-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="312">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt;">Anggota</span></p>
                        </td>
                        <td style="width: 233.8pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="312">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt;">Ida Wahyuni, S.Kom., M.Kom.</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 234.0pt; border: solid windowtext 1.0pt; border-top: none; mso-border-top-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="312">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt;"> </span></p>
                        </td>
                        <td style="width: 233.8pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="312">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt;"> </span></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 234.0pt; border: solid windowtext 1.0pt; border-top: none; mso-border-top-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="312">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><b><span lang="EN-US" style="font-size: 12.0pt;">DIVISI HUBUNGAN ANTAR LEMBAGA</span></b></p>
                        </td>
                        <td style="width: 233.8pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="312">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><b><span lang="EN-US" style="font-size: 12.0pt;"> </span></b></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 234.0pt; border: solid windowtext 1.0pt; border-top: none; mso-border-top-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="312">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt;">Ketua</span></p>
                        </td>
                        <td style="width: 233.8pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="312">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt;">Akhlis Munazilin, S.Kom, M.T.</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 234.0pt; border: solid windowtext 1.0pt; border-top: none; mso-border-top-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="312">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt;">Anggota</span></p>
                        </td>
                        <td style="width: 233.8pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="312">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt;">Nia Ratmelia, S.Kom.</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 234.0pt; border: solid windowtext 1.0pt; border-top: none; mso-border-top-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="312">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt;"> </span></p>
                        </td>
                        <td style="width: 233.8pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="312">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt;"> </span></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 467.8pt; border: solid windowtext 1.0pt; border-top: none; mso-border-top-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" colspan="2" valign="top" width="624">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><b><span lang="EN-US" style="font-size: 12.0pt;">DIVISI HUMAS (SOSIAL MEDIA)</span></b></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 234.0pt; border: solid windowtext 1.0pt; border-top: none; mso-border-top-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="312">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt;">Ketua</span></p>
                        </td>
                        <td style="width: 233.8pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="312">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt;"> </span></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 234.0pt; border: solid windowtext 1.0pt; border-top: none; mso-border-top-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="312">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt;">Anggota</span></p>
                        </td>
                        <td style="width: 233.8pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="312">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt;"> </span></p>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <h3><strong>Struktur Organisasi Himpunan Alumni STMIK Asia Malang</strong></h3>
                <img src="{{ url('images/alumni/bagan.png') }}" class="img-responsive">

            </div>
        </div>
    </div>
@endsection