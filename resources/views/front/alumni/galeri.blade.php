@extends('front.layout.app')
@section('title', 'ALUMNI STMIK ASIA')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('css/alumni/custom.css') }}">
@endsection
@section('content')
    <div class="container min-700">
        <div class="row">
            <div class="col-md-12 well well-white mt-50">
                @include('front.alumni.menu')
                <div class="text-center">
                    <h1 class="title-content weight-7">ALUMNI STMIK ASIA</h1>
                </div>
                <h3><strong>GALERI</strong></h3>

                <h4 class="text-center"><b>Reuni Akbar 1 Alumni Perguruan Tinggi Asia Malang. Sabtu, 26 Agustus 2017 </b></h4>
                <div class="row">
                    <div class="col-md-4 box-profil">
                        <img src="{{ url('images/galeri/reuni/1.png') }}" class="img-responsive">
                    </div>
                    <div class="col-md-4 box-profil">
                        <img src="{{ url('images/galeri/reuni/2.png') }}" class="img-responsive">
                    </div>
                    <div class="col-md-4 box-profil">
                        <img src="{{ url('images/galeri/reuni/3.png') }}" class="img-responsive">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 box-profil">
                        <img src="{{ url('images/galeri/reuni/4.png') }}" class="img-responsive">
                    </div>
                    <div class="col-md-4 box-profil">
                        <img src="{{ url('images/galeri/reuni/5.png') }}" class="img-responsive">
                    </div>
                    <div class="col-md-4 box-profil">
                        <img src="{{ url('images/galeri/reuni/6.png') }}" class="img-responsive">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 box-profil">
                        <img src="{{ url('images/galeri/reuni/7.png') }}" class="img-responsive">
                    </div>
                    <div class="col-md-4 box-profil">
                        <img src="{{ url('images/galeri/reuni/8.png') }}" class="img-responsive">
                    </div>
                    <div class="col-md-4 box-profil">
                        <img src="{{ url('images/galeri/reuni/9.png') }}" class="img-responsive">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 box-profil">
                        <img src="{{ url('images/galeri/reuni/10.png') }}" class="img-responsive">
                    </div>
                    <div class="col-md-4 box-profil">
                        <img src="{{ url('images/galeri/reuni/11.png') }}" class="img-responsive">
                    </div>
                    <div class="col-md-4 box-profil">
                        <img src="{{ url('images/galeri/reuni/12.png') }}" class="img-responsive">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 box-profil">
                        <img src="{{ url('images/galeri/reuni/13.png') }}" class="img-responsive">
                    </div>
                    <div class="col-md-4 box-profil">
                        <img src="{{ url('images/galeri/reuni/14.png') }}" class="img-responsive">
                    </div>

                </div>
                <p></p>

                <h4 class="text-center"><b>Kuliah Tamu Mata Kuliah Bussiness Practice 3 20 Oktober 2016. Kuliah tamu 20 Oktober 2016 pagi ini membahasa mengenai Internet Marketing, yang disampaikan langsung oleh Mahasiswa Alumni Perti Asia yaitu Mas Deddy Fajar</b></h4>
                <div class="row">
                    <div class="col-md-4 box-profil">
                        <img src="{{ url('images/galeri/bp3/1.jpg') }}" class="img-responsive">
                    </div>
                    <div class="col-md-4 box-profil">
                        <img src="{{ url('images/galeri/bp3/2.jpg') }}" class="img-responsive">
                    </div>
                    <div class="col-md-4 box-profil">
                        <img src="{{ url('images/galeri/bp3/3.jpg') }}" class="img-responsive">
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection