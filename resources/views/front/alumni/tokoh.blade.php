@extends('front.layout.app')
@section('title', 'ALUMNI STMIK ASIA')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('css/alumni/custom.css') }}">
@endsection
@section('content')
    <div class="container min-700">
        <div class="row">
            <div class="col-md-12 well well-white mt-50">
                @include('front.alumni.menu')
                <div class="text-center">
                    <h1 class="title-content weight-7">ALUMNI STMIK ASIA</h1>
                </div>
                <h3><strong>TOKOH ALUMNI STMIK ASIA</strong></h3>

                <div class="media alumni-tokoh">
                    <div class="media-heading">
                        <h4>Rina Dewi Indah Sari, S.Kom., M.Kom.</h4>
                    </div>
                    <div class="media-left">
                        <a href="{{ url('') }}"><img class="media-object" src="{{ url('images/tokoh/rina.png') }}" alt="Rina Dewi Indah Sari, S.Kom., M.Kom."></a>
                    </div>
                    <div class="media-body">
                        <p>Ibu Rina Dewi Indah Sari, S.Kom., M.Kom. atau yang sering disapa Bu Rina adalah alumnus STMIK Asia Angkatan 2003. Beliau mengambil kuliah pada Jurusan Teknik Informatika STMIK Asia. Setelah lulus kuliah beliau diminta untuk mengajar di STMIK Asia Malang, waktu itu sekitar tahun 2007. Beliau kemudian melanjutkan sekolah magisternya pada tahun 2011 di STTS Surabaya dan mendapatkan gelar Magister Komputer. Setelah beberapa tahun mengajar di STMIK Asia Malang, beliau menunjukkan peningkatan karier yang cukup cepat. Pada tahun 2012 beliau diangkat menjadi Ketua Jurusan Program Teknik Informatika di STMIK Asia Malang hingga sekarang. Selain aktif dikampus, Beliau juga menekuni usaha retail di rumahnya. Usaha tersebut bisa dikatakan cukup sukses dan bisa menghasilkan penghasilan tambahan yang cukup menjanjikan.</p>
                        <p>Bu Rina yang memulai pendidikannya di STMIK Asia Malang, dalam waktu kurang dari 10 tahun kini sudah sukses dalam karier menjadi dosen, ketua jurusan, dan menjalani usaha. Mau seperti beliau? Kuliah di STMIK Asia bisa menjadi salah satu langkah awal. (Red: Ida)</p>
                    </div>
                </div>

                <h3 class="text-center"><b>Budi Dedy Fajar S, S.Kom, CH, CHT</b></h3>
                <img src="{{ url('images/tokoh/dedi.png') }}" class="img-responsive">
                <p></p>
                <p>Budi Dedy Fajar S, S.Kom, CH, CHT atau sering disapa Mas Dedy adalah seorang anak PNS (Pegawai Negeri Swasta) yang tinggal di Ngawi, provinsi Jawa Timur. Namun, meskipun berasal dari keluarga PNS, Mas Dedy tidak punya keinginan untuk menjadi PNS juga, melainkan ingin menjadi pengusaha.
                    Tahun 2006, Mas Dedy memutuskan untuk melanjutkan pendidikannya di Malang yaitu di STMIK Asia Malang. Awal kenginginan memulai usaha dia mulai dari bangku kuliah. Suatu hari Mas Dedy mendapat tawaran dari MLM (MultiLevel Marketing), namun karena Mas Dedy adalah seorang Mahasiswa yang tidak punya uang karena belum bekerja. Akhirnya Mas Dedy mencoba meyakinkan orang tuanya tentang MLM yang ingin diikutinya dan meminjam uang sebesar Rp. 2.000.000,-. Namun, setelah mengikuti kegiatan MLM tersebut, Mas Dedy merasa trauma dengan pekerjaan itu.
                    Tidak menyerah sampain disitu, ia mencoba mencari uang dari PPC (Pay Per Click), ternyata 15 PPC (Pay Per Click) yang pernah diikuti tidak ada hasil. Gagal PPC akhirnya ia mencoba mencari uang lewat PTR (Pair To Review), namun akhirnya ia harus mengeluarkan banyak uang untuk hal itu dan Mas Dedy memutuskan untuk mengakhirinya. Sempat juga bekerja di CRTV Malang, menjadi seorang reporter, namun gaji yang diterimanya tidak begitu menjanjikan.
                    Pada perkuliahan semester 5, Mas Dedy mencoba membuat program yang kemudian dijualnya dengan harga 500.000 sampai 1.500.000 per program. Sempat juga dia dan teman-temannya menjual gorengan, karena mereka berfikir tidak setiap hari orang akan membeli program.
                    Suatu hari Mas Dedy berinisiatif untuk menjualkan produk kosmetik temannya hingga tingkat penjualannya melebihi teman yang pertama kali memberikannya dagangan. Tidak lama kemudian temannya berhenti berjualan kosmetik dan tidak lagi memberi dagangan untuk Mas Dedy. Namun tidak berhenti disitu, ia merasa akan sukses dengan berjualan produk ini dan akhirnya Mas Dedy mengambil barang dari pedagang besar untuk dipasarkan dan kadangkala juga ada seles yang mengiriminya barang tanpa bayar didepan (tanpa modal awal).
                    Setelah belajar internet marketing selama 4 bulan bersama Bapak Agus Setiyawan, akhirnya sekarang dia bisa sukses berjualan online. Bekerja didepan komputer selama beberapa jam, sudah banyak orang yang ingin order produknya. Tidak perlu keluar rumah untuk memasarkan produknya, hanya duduk didepan komputer dan menerima pesanan lewat telepon dari konsumen. Jika berjualan di internet orang yang akan menghubungi kita, sudah pasti 50% tertarik dengan produk yang kita jual. Tetapi kerja lewat internet harus optimasi 100% untuk menghadapi pesaing di dunia internet. Mas Dedy juga mempunyai jadwal pengerjaan yang rapi dan tertib, sehingga pekerjaan dapat menghasilkan sesuatu yang berharga. (Sumber: L.I.K Putri – dengan perubahan)
                </p>
                <p></p>
                <h3 class="text-center"><b>Hendrik Bayu, S.Kom </b></h3>
                <img src="{{ url('images/tokoh/hendrik.jpg') }}" class="img-responsive">
                <p></p>
                                <p>Hendrik Bayu atau yang sering disapa Mas Hendrik, adalah alumnus STMIK asia angkatan tahun 2006. Beliau mengambil jurusan TI-Desain Grafis (saat ini menjadi Desain Komunikasi Visual atau DKV. Mas Hendrik memulai membuat usaha di bidang desain kemasan atau branding product sebelum lulus kuliah. Produk yang pertama kali dia branding adalah keripik usus yang beliau branding dengan nama “Kripik Monster”.
                    Awal penjualan Kripik Monster beliau mulai dari kampusnya, yaitu mulai dipasarkan kepada teman-teman dan dosen-dosen di area STMIK Asia. Hasilnya cukup menggembirakan, produk tersebut cukup laris dan lama-kelamaan makin terkenal.
                    Setelah beberapa tahun, Mas Hendrik mencoba peruntungan lain untuk memasarkan produknya ke luar kota bahkan sampi ke luar negeri dan hasilnya pun sangat memuaskan. Sampai pada akhirnya beliau membuat franchaise Kripik Monster untuk memberikan orang lain lapangan pekerjaan dengan berjualan barang dagangannya.
                    Setelah sukses mem-branding produknya sendiri, Mas hendrik berkeinginan untuk mem-branding produk milik orang lain, khususnya produk para Usaha Kecil Menengah (UKM). Keinginan itu beliau mulai dengan membuat website yang disebut http://markazdesign.net/. Pada website tersebut Mas Hendrik menyediakan layanan desain logo, packaging, dan website dengan harga yang cukup terjangkau oleh kalangan UKM. Seperti slogannya, Mas Hendrik ingin membuat “UKM Naik Kelas” dengan membuatkan desain logo UKM yang lebih menaik, desain kemasan yang lebih menjual, dan website untuk pemasaran produk yang optimal. Berkat jasanya, banyak UKM yang sudah bisa naik kelas dengan omset penjualan yang semakin meningkat. (Red: Ida)
                </p>
                <p></p>
                <h3 class="text-center"><b>Erwan Saputra, S.Kom. </b></h3>
                <img src="{{ url('images/tokoh/erwan.png') }}" class="img-responsive">
                <p></p>
                <p>Erwan Saputra merupakan  mahasiswa dari Sekolah Tinggi Manajemen Ilmu dan Komputer atau STMIK ASIA  Malang angkatan 2011. Selain Erwan menjadi salah satu mahasiswa jurusan desain grafis, dia juga menjadi founding father alias pendiri dari situs verifikasi toko online yang dia namakan polisionline.com. ”Ini lagi mengecek e-mail yang masuk ke inbox atau mempelajari penawaran dari toko online,” terang Erwan.
                    Dia menjelaskan, website yang dia ciptakan ini memiliki fungsi sebagai verifikator keaslian toko online. Artinya, jika online shop yang sudah dia verifikasi serta diberi banner logo polisionline.com, maka toko online itu bukan toko tipu-tipu alias toko asli dan tidak abal-abal. Jika sudah diverifikasi dan mendapatkan tanda dari polisionline.com, maka situs jual beli online milik kliennya ini tepercaya.
                    Aktivitas menjadi polisi online ini sudah dia lakoni sejak Februari 2013 lalu. Sudah hampir tiga tahun dia menjalani kegiatan semacam ini. Ide itu bermula pada 2012, dia menjadi korban penipuan online saat bertransaksi dengan salah satu situs jual beli. Kala itu, sulung dari tiga bersaudara itu ingin berbisnis laptop dan mulai membeli barang yang dia inginkan di salah satu situs jual beli. ”Dapat informasi dari teman jika di situs tersebut harga laptop yang dijual cukup murah, hanya Rp 6 juta. Padahal, waktu itu di pasaran harganya sekitar Rp 15 juta,” terang dia. Namun, setelah uang yang diminta dikirimkan ke rekening si penjual, barang yang sudah dipesan tidak kunjung diterima Erwan.
                    Berbekal rasa sakit hati kepada para pencuri di dunia maya itu, akhirnya dia menciptakan website untuk memverifikasi keaslian toko online. Pada awalnya memang pemilik toko online yang ditawarinya untuk menjadi klien dan diperiksa keasliannya tokonya, banyak yang menolak. Bahkan, tidak jarang e-mail penawarannya tidak mendapatkan balasan dari para calon klien. Namun, dia tidak berputus asa, dia terus melakukan penawaran hingga mulai datang para pelanggan. Bahkan karena kreativitasnya itu dia juga pernah diundang dalam acara Kick Andy di Metro TV pada Jumat 8 Januari 2016 lalu dengan tema Kami Muda Kami Peduli.
                    Karena, aksinya ini sudah ratusan hingga ribuan kali ia bisa mengungkap toko online abal-abal. Tidak jarang dia mendapatkan teror karena pemilik toko online tersebut tidak terima jika usahanya dicap melakukan penipuan. Teror itu mulai dari SMS maupun telepon yang mengancam Erwan akan dipolisikan. ”Namun,  itu tidak terbukti. Kalau mereka lapor polisi, ya akan kena sendirilah. Wong, penipunya mereka kok,” jelas dia. (Sumber: http://kalteng.prokal.co – dengan perubahan)
                </p>
                <p></p>
                <h3 class="text-center"><b>Ida Wahyuni, S.Kom., M.Kom. </b></h3>
                <img src="{{ url('images/tokoh/ida.jpg') }}" class="img-responsive">
                <p></p>
                <p>Salah satu alumni STMIK Asia Angkatan 2010 berhasil menjadi salah satu “Best Session Presenter” di 8th International Conference on Advanced Computer Science and Information System (ICACSIS) 2016 yang dilaksanakan pada 15-16 Oktober 2016 di Widyaloka Building. Dia adalah Ida Wahyuni alumni STMIK Asia jurusan Teknik Informatika. Dalam konferensi international tersebut Ida mempresentasikan hasil penelitian yang berjudul “Error Numerical Analysis for Result of Rainfall Prediction Between Tsukamoto FIS and Hybrid Tsukamoto FIS with GA”.
                    Penelitian yang dilakukan Ida Wahyuni dikerjakan saat dia menempuh kuliah S2. Penelitian tersebut membahas mengenai perbandingan error pada prediksi curah hujan yang dihasilkan dari metode  Tsukamoto FIS dan Hybrid Tsukamoto FIS with Genetic Algorithm. Penelitian ini dilakukan Ida sebagai salah satu penelitian pendahuluan untuk tesis-nya. “Tema tesis saya adalah membuat prototype metode yang handal untuk prediksi curah hujan, dan kedua metode yang saya tulis dalam penelitian tersebut adalah salah satu metode pembanding yang saya gunakan dalam tesis saya, jelas Ida”.
                    Menjadi salah satu presenter terbaik dalam sebuah konferensi internasional memang bukanlah hal yang mudah. Apalagi peserta yang mengikuti konferensi internasional berasal dari berbagai negara dan berbagai latar belakang. ICACSIS 2016 adalah konferensi international kedua yang pernah diikuti oleh Ida. Sebelumnya Ida pernah mengikuti ICITISEE 2016 di Yogyakarta dua bulan lalu. Sebelum melakukan presentasi Ida biasanya melakukan persiapan sehari sebelum presentasi dilaksanakan, sehingga performa yang bagus akan didapat saat melakukan presentasi. “Saya sendiri tidak menyangka dapat menjadi salah satu presenter terbaik, saya hanya mempersiapkan diri dengan baik dan berdoa agar semua dapat berjalan dengan lancar. Hal terpenting yang harus dilakukan sebelum presentasi adalah jangan terlalu gugup, kita harus bisa menyeimbangkan emosi agar tetap tenang dan bisa konsentrasi, jelas Ida.” (Red: Ida)
                </p>
            </div>
        </div>
    </div>
@endsection