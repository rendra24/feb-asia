<ul>
    <li><a href="{{ url('tautan/alumni') }}">Profile</a></li>
    <li><a href="{{ url('tautan/alumni/struktur') }}">Struktur Organisasi</a></li>
    <li><a href="{{ url('tautan/alumni/tokoh') }}">Tokoh</a></li>
    <li><a href="{{ url('tautan/alumni/galeri') }}">Galeri</a></li>
    <li><a href="{{ url('tautan/alumni/sumbangan') }}">Sumbangan</a></li>
    <li><a href="{{ url('tautan/alumni/pustaka') }}">Pustaka</a></li>
</ul>