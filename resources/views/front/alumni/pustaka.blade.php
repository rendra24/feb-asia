@extends('front.layout.app')
@section('title', 'ALUMNI STMIK ASIA')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('css/alumni/custom.css') }}">
@endsection
@section('content')
    <div class="container min-700">
        <div class="row">
            <div class="col-md-12 well well-white mt-50">
                @include('front.alumni.menu')
                <div class="text-center">
                    <h1 class="title-content weight-7">ALUMNI STMIK ASIA</h1>
                </div>
                <h3><strong>PUSTAKA</strong></h3>
                <p>PUBLIKASI NASIONAL</p>
                <p></p>
                <table class="table table-bordered table-responsive">
                    <tbody>
                    <tr>
                        <td>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; text-align: center; line-height: normal;" align="center"><b><span lang="EN-US" style="font-size: 12.0pt; mso-bidi-font-family: Arial;">Tahun</span></b></p>
                        </td>
                        <td>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; text-align: center; line-height: normal;" align="center"><b><span lang="EN-US" style="font-size: 12.0pt; mso-bidi-font-family: Arial;">Judul Artikel</span></b></p>
                        </td>
                        <td>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; text-align: center; line-height: normal;" align="center"><b><span lang="EN-US" style="font-size: 12.0pt; mso-bidi-font-family: Arial;">Nama Penulis </span></b></p>
                        </td>
                        <td>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; text-align: center; line-height: normal;" align="center"><b><span lang="EN-US" style="font-size: 12.0pt; mso-bidi-font-family: Arial;">URL / Link Artikel</span></b></p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt; mso-bidi-font-family: Arial;">2006</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt; mso-bidi-font-family: Arial;">Perancangan Pengendali Model Tangan Robot Menggunakan Volume Suara Manusia </span></p>
                        </td>
                        <td>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt; mso-bidi-font-family: Arial;">Bambang Tri Wahyu Utomo </span></p>
                        </td>
                        <td>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US"><a href="http://jurnal.stmikasia.ac.id/index.php/jitika/article/view/4" target="_blank" rel="noopener noreferrer"><span style="font-size: 12.0pt; mso-bidi-font-family: Arial;">http://jurnal.stmikasia.ac.id/index.php/jitika/article/view/4</span></a></span><u></u></p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt; mso-bidi-font-family: Arial;">2007</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt; mso-bidi-font-family: Arial;">Perancangan Robot Dasar Menjadi Cerdas Memanfaatkan Teknologi Sensor</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt; mso-bidi-font-family: Arial;">Bambang Tri Wahyu Utomo,<br /> Tomi Pancoro Nugroho</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US"><a href="http://jurnal.stmikasia.ac.id/index.php/jitika/article/view/15" target="_blank" rel="noopener noreferrer"><span style="font-size: 12.0pt; mso-bidi-font-family: Arial;">http://jurnal.stmikasia.ac.id/index.php/jitika/article/view/15</span></a></span><u></u></p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt; mso-bidi-font-family: Arial;">2007</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt; mso-bidi-font-family: Arial;">Rancang Bangun Pengaman Mobil Berbasis Mikrokontroler AT89S51 Dengan Aplikasi Telepon Seluler Sebagai Indikator Alarm</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt; mso-bidi-font-family: Arial;">Bambang Tri Wahyu Utomo,<br /> Pri Hadi Wijaya</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US"><a href="http://jurnal.stmikasia.ac.id/index.php/jitika/article/view/22" target="_blank" rel="noopener noreferrer"><span style="font-size: 12.0pt; mso-bidi-font-family: Arial;">http://jurnal.stmikasia.ac.id/index.php/jitika/article/view/22</span></a></span><u></u></p>
                        </td>
                    </tr>
                   <tr>
                        <td>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt; mso-bidi-font-family: Arial;">2012</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt; mso-bidi-font-family: Arial;">Rancang Bangun Aplikasi Sistem Parkir Mobil Menggunakan Sensor Infra Red di Rumah Sakit Aminah Blitar </span></p>
                        </td>
                        <td>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt; mso-bidi-font-family: Arial;">Bambang Tri Wahyu Utomo</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US"><a href="http://jurnal.stmikasia.ac.id/index.php/jitika/article/view/90" target="_blank" rel="noopener noreferrer"><span style="font-size: 12.0pt; mso-bidi-font-family: Arial;">http://jurnal.stmikasia.ac.id/index.php/jitika/article/view/90</span></a></span><u></u></p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt; mso-bidi-font-family: Arial;">2014</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt; mso-bidi-font-family: Arial;">Prototiping Sistem Monitoring Ketinggian Air Dan Pengendalian Pintu Air Pada Jaringan Irigasi Berbasis Microkontroler Atmega16 Dengan Menggunakan Short Message Service (SMS)</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt; mso-bidi-font-family: Arial;">Bambang Tri Wahyu Utomo,<br /> Hasan Saifudi</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US"><a href="http://jurnal.stmikasia.ac.id/index.php/jitika/article/view/129" target="_blank" rel="noopener noreferrer"><span style="font-size: 12.0pt; mso-bidi-font-family: Arial;">http://jurnal.stmikasia.ac.id/index.php/jitika/article/view/129</span></a></span><u></u></p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt; mso-bidi-font-family: Arial;">2015</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt; mso-bidi-font-family: Arial;">Sistem Rekomendasi Paket Wisata Se-Malang Raya Menggunakan Metode Hybrid Content Based Dan Collaborative</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt; mso-bidi-font-family: Arial;">Bambang Tri Wahyu Utomo,<br /> Angga Anggriawan</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US"><a href="http://jurnal.stmikasia.ac.id/index.php/jitika/article/view/96" target="_blank" rel="noopener noreferrer"><span style="font-size: 12.0pt; mso-bidi-font-family: Arial;">http://jurnal.stmikasia.ac.id/index.php/jitika/article/view/96</span></a></span><u></u></p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt; mso-bidi-font-family: Arial;">2016</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt; mso-bidi-font-family: Arial;">Simulasi Sistem Pendeteksi Polusi Ruangan Menggunakan Sensor Asap Dengan Pemberitahuan Melalui SMS (Short Message Service) Dan Alarm Berbasis Arduino</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt; mso-bidi-font-family: Arial;">Bambang Tri Wahyu Utomo,<br /> Dharmawan Setya Saputra</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US"><a href="http://jurnal.stmikasia.ac.id/index.php/jitika/article/view/76" target="_blank" rel="noopener noreferrer"><span style="font-size: 12.0pt; mso-bidi-font-family: Arial;">http://jurnal.stmikasia.ac.id/index.php/jitika/article/view/76</span></a></span><u></u></p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt; mso-bidi-font-family: Arial;">2016</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal" style="line-height: 150%;"><span lang="EN-US" style="font-size: 12.0pt; line-height: 150%;">Diagnosis Penyakit Infeksi Saluran Pernapasan pada Anak Menggunakan Forward Chaining dan Certainty Factor</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12pt; color: #222222; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">Ida Wahyuni, </span></p>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12pt; color: #222222; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">Chynthia Kusumawati</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><u><span lang="EN-US" style="font-size: 12.0pt; mso-bidi-font-family: Arial; color: #1155cc;">https://www.researchgate.net/profile/Ida_Wahyuni/publication/318596448_Diagnosis_Penyakit_Infeksi_Saluran_Pernapasan_pada_Anak_Menggunakan_Forward_Chaining_dan_Certainty_Factor/links/59721432458515e26df86a86/Diagnosis-Penyakit-Infeksi-Saluran-Pernapasan-pada-Anak-Menggunakan-Forward-Chaining-dan-Certainty-Factor.pdf</span></u></p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt; mso-bidi-font-family: Arial;">2016</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal" style="line-height: 150%;"><span lang="EN-US" style="font-size: 12.0pt; line-height: 150%;">Pembobotan Penilaian Ujian Pilihan Ganda Menggunakan Algoritma Genetika</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12pt; color: #222222; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">Ida Wahyuni, </span></p>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12pt; color: #222222; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">Wayan Firdaus Mahmudy</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><u><span lang="EN-US" style="font-size: 12.0pt; mso-bidi-font-family: Arial; color: #1155cc;"><span style="text-decoration-line: none;"> </span></span></u></p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt; mso-bidi-font-family: Arial;">2016</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal" style="line-height: 150%;"><span lang="EN-US" style="font-size: 12.0pt; line-height: 150%;">Clustering Nasabah Bank Berdasarkan Tingkat Likuiditas Menggunakan Hybrid Particle Swarm Optimization dengan K-Means</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12pt; color: #222222; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">Ida Wahyuni, </span></p>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12pt; color: #222222; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">Yudha Alif Auliya, </span></p>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12pt; color: #222222; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">Asyrofa Rahmi, </span></p>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12pt; color: #222222; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">Wayan Firdaus Mahmudy</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><u><span lang="EN-US" style="font-size: 12.0pt; mso-bidi-font-family: Arial; color: #1155cc;">http://jurnal.stmikasia.ac.id/index.php/jitika/article/download/79/62</span></u></p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt; mso-bidi-font-family: Arial;">2017</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal" style="line-height: 150%;"><span lang="EN-US" style="font-size: 12.0pt; line-height: 150%;">Prediksi Produksi Biogas Tahunan Dengan Pendekatan Sistem Dinamik Untuk Optimasi Kapasitas Sampah TPAS Talangagung</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt;">Philip Faster Eka Adipraja, </span></p>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt;">Mufidatul Islamiyah,</span></p>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt;"> Ida Wahyuni</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><u><span lang="EN-US" style="font-size: 12.0pt; mso-bidi-font-family: Arial; color: #1155cc;"><span style="text-decoration-line: none;"> </span></span></u></p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt; mso-bidi-font-family: Arial;"> </span></p>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt; mso-bidi-font-family: Arial;">2010</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal" style="line-height: 150%;"><span lang="EN-US" style="font-size: 12.0pt; line-height: 150%;">Membangun Sistem Pakar untuk Mengidentifikasi Jenis Penyakit pada Tanaman Jeruk</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt;">Akhlis Munazilin, </span></p>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt;">Jaenal Arifin</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><u><span lang="EN-US" style="font-size: 12.0pt; mso-bidi-font-family: Arial; color: #1155cc;">http://jurnal.amiki.ac.id/wp-content/uploads/2015/02/Jurnal-7.pdf</span></u></p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt; mso-bidi-font-family: Arial;">2011</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal" style="line-height: 150%;"><span lang="EN-US" style="font-size: 12.0pt; line-height: 150%;">Implementasi Sistem Pakar untuk Mengetahui Bakat Anak Melalui Tes WISC (Wechsler Intelligence Scale for Children) Menggunakan Metode Forward Chaining</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal" style="line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt;">Akhlis Munazilin</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><u><span lang="EN-US" style="font-size: 12.0pt; mso-bidi-font-family: Arial; color: #1155cc;">http://jurnal.stmikasia.ac.id/index.php/jitika/article/download/64/49</span></u></p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt; mso-bidi-font-family: Arial;">2007</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal" style="line-height: 150%;"><span lang="EN-US" style="font-size: 12.0pt; line-height: 150%;">Pemanfaatan Port Paralel Komputer Sebagai Media Interface Dan Sensor Cahaya Sebagai Signal Input Untuk Mengontrol Buka Dan Tutup Tirai Secara Manual Dan Otomatis Menggunakan Bahasa Pemograman Delphi 7.0</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt;">Muhammad Rofiq, </span></p>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12.0pt;">Akhlis Munazilin</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><u><span lang="EN-US" style="font-size: 12.0pt; mso-bidi-font-family: Arial; color: #1155cc;"><span style="text-decoration-line: none;"> </span></span></u></p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p class="MsoNormal"><span lang="EN-US" style="font-size: 12.0pt; line-height: 115%; mso-bidi-font-family: Arial;">2008</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal"><span lang="EN-US" style="font-size: 12.0pt; line-height: 115%; mso-bidi-font-family: Arial;">Membangun Sistem Pakar Untuk Mengidentifikasi Jenis Penyakit Pada Tanaman Jeruk Berbasis Wireless Aplication Protocol (Wap) Dengan Wireless Markup Language (WML) Dan PHP Hypertext Preprocessor (PHP)</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal"><span lang="EN-US" style="font-size: 12.0pt; line-height: 115%; mso-bidi-font-family: Arial;">Titania Dwi Andini,<br /> Jaenal Arifin</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal"><span lang="EN-US"><a href="http://jurnal.stmikasia.ac.id/index.php/jitika/article/view/115" target="_blank" rel="noopener noreferrer"><span style="font-size: 12.0pt; line-height: 115%; mso-bidi-font-family: Arial;">http://jurnal.stmikasia.ac.id/index.php/jitika/article/view/115</span></a></span><u></u></p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p class="MsoNormal"><span lang="EN-US" style="font-size: 12.0pt; line-height: 115%; mso-bidi-font-family: Arial;">2012</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal"><span lang="EN-US" style="font-size: 12.0pt; line-height: 115%; mso-bidi-font-family: Arial;">Pengenalan pola tulisan tangan untuk mengetahui kepribadian seseorang menggunakan metode jaringan hamming</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal" style="margin-bottom: .0001pt;"><span lang="EN-US" style="font-size: 12.0pt; line-height: 115%; mso-bidi-font-family: Arial;">Jaenal Arifin,</span></p>
                            <p class="MsoNormal" style="margin-bottom: .0001pt;"><span lang="EN-US" style="font-size: 12.0pt; line-height: 115%; mso-bidi-font-family: Arial;">Eko Vidiantoro<br /> Endang Setyati</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal"><span lang="EN-US"><a href="http://jurnal.stmikasia.ac.id/index.php/jitika/article/view/87" target="_blank" rel="noopener noreferrer"><span style="font-size: 12.0pt; line-height: 115%; mso-bidi-font-family: Arial;">http://jurnal.stmikasia.ac.id/index.php/jitika/article/view/87</span></a></span><u></u></p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p class="MsoNormal"><span lang="EN-US" style="font-size: 12.0pt; line-height: 115%; mso-bidi-font-family: Arial;">2013</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal"><span lang="EN-US" style="font-size: 12.0pt; line-height: 115%; mso-bidi-font-family: Arial;">Klasifikasi Jenis Kayu dengan Gray Level Co-Occurrence Matrices (GLCMs) dan K-Nearest Neighbor</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal" style="margin-bottom: .0001pt;"><span lang="EN-US" style="font-size: 12.0pt; line-height: 115%; mso-bidi-font-family: Arial;">Jaenal Arifin,</span></p>
                            <p class="MsoNormal" style="margin-bottom: .0001pt;"><span lang="EN-US" style="font-size: 12.0pt; line-height: 115%; mso-bidi-font-family: Arial;">Yuliana Melita</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal"><span lang="EN-US"><a href="http://jurnal.stmikasia.ac.id/index.php/jitika/article/view/67" target="_blank" rel="noopener noreferrer"><span style="font-size: 12.0pt; line-height: 115%; mso-bidi-font-family: Arial;">http://jurnal.stmikasia.ac.id/index.php/jitika/article/view/67</span></a></span><u></u></p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p class="MsoNormal"><span lang="EN-US" style="font-size: 12.0pt; line-height: 115%; mso-bidi-font-family: Arial;">2015</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal"><span lang="EN-US" style="font-size: 12.0pt; line-height: 115%; mso-bidi-font-family: Arial;">Pembuatan Digital Magazine Komunitas Kawasaki KLX 150s Regional malang</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal"><span lang="EN-US" style="font-size: 12.0pt; line-height: 115%; mso-bidi-font-family: Arial; color: black;">Jaenal Arifin,<br /> Aditya Prakoso</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal"><span lang="EN-US"><a href="http://jurnal.stmikasia.ac.id/index.php/jitika/article/view/117" target="_blank" rel="noopener noreferrer"><span style="font-size: 12.0pt; line-height: 115%; mso-bidi-font-family: Arial;">http://jurnal.stmikasia.ac.id/index.php/jitika/article/view/117</span></a></span><u></u></p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p class="MsoNormal"><span lang="EN-US" style="font-size: 12.0pt; line-height: 115%; mso-bidi-font-family: Arial;">2016</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal"><span lang="EN-US" style="font-size: 12.0pt; line-height: 115%; mso-bidi-font-family: Arial;">Sistem Pakar Diagnosa Penyakit Gigi Dan Mulut Manusia Menggunakan Knowledge Base System Dan Certainty Factor</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal"><span lang="EN-US" style="font-size: 12.0pt; line-height: 115%; mso-bidi-font-family: Arial; color: black;">Jaenal Arifin,</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal"><span lang="EN-US"><a href="http://jurnal.stmikasia.ac.id/index.php/jitika/article/view/86" target="_blank" rel="noopener noreferrer"><span style="font-size: 12.0pt; line-height: 115%; mso-bidi-font-family: Arial;">http://jurnal.stmikasia.ac.id/index.php/jitika/article/view/86</span></a></span><u></u></p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p class="MsoNormal"><span lang="EN-US" style="font-size: 12.0pt; line-height: 115%; mso-bidi-font-family: Arial;">2016</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal"><span lang="EN-US" style="font-size: 12.0pt; line-height: 115%; mso-bidi-font-family: Arial;">Optimasi Jumlah Mahasiswa Asia Melalui Prediksi Masa Studi Menggunakan Metode Induksi Decision Tree</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal"><span lang="EN-US" style="font-size: 12.0pt; line-height: 115%; mso-bidi-font-family: Arial; color: black;">Jaenal Arifin,<br /> Puji Subekti,<br /> Widya Adhariyanty Rahayu</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal"><span lang="EN-US"><a href="http://ejurnal.stimata.ac.id/index.php/DINAMIKA/article/view/204" target="_blank" rel="noopener noreferrer"><span style="font-size: 12.0pt; line-height: 115%; mso-bidi-font-family: Arial;">http://ejurnal.stimata.ac.id/index.php/DINAMIKA/article/view/204</span></a></span><u></u></p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p class="MsoNormal"><span lang="EN-US" style="font-size: 12.0pt; line-height: 115%; mso-bidi-font-family: 'Times New Roman';">2008</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal"><span lang="EN-US" style="font-size: 12.0pt; line-height: 115%; mso-bidi-font-family: 'Times New Roman';">Analisis Penyelesaian Puzzle Sudoku Dengan Menerapkan Algoritma Backtracking Memanfaatkan Bahasa Pemrograman Visual Basic 6.0</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal"><span lang="EN-US" style="font-size: 12.0pt; line-height: 115%; mso-bidi-font-family: 'Times New Roman';">Rina Dewi Indahsari,</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal"><span lang="EN-US"><a href="http://jurnal.stmikasia.ac.id/index.php/jitika/article/view/30" target="_blank" rel="noopener noreferrer"><span style="font-size: 12.0pt; line-height: 115%; mso-bidi-font-family: 'Times New Roman';">http://jurnal.stmikasia.ac.id/index.php/jitika/article/view/30</span></a></span><u></u></p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p class="MsoNormal"><span lang="EN-US" style="font-size: 12.0pt; line-height: 115%; mso-bidi-font-family: 'Times New Roman';">2008</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal"><span lang="EN-US" style="font-size: 12.0pt; line-height: 115%; mso-bidi-font-family: 'Times New Roman';">Implementasi Metode Huffman Untuk Kompresi Ukuran File Citra Bitmap 8 Bit Menggunakan Borland Delphi 6.0</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal"><span lang="EN-US" style="font-size: 12.0pt; line-height: 115%; mso-bidi-font-family: 'Times New Roman';">Rina Dewi Indahsari,<br /> Nur Munawaroh</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal"><span lang="EN-US"><a href="http://jurnal.stmikasia.ac.id/index.php/jitika/article/view/116" target="_blank" rel="noopener noreferrer"><span style="font-size: 12.0pt; line-height: 115%; mso-bidi-font-family: 'Times New Roman';">http://jurnal.stmikasia.ac.id/index.php/jitika/article/view/116</span></a></span><u></u></p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p class="MsoNormal"><span lang="EN-US" style="font-size: 12.0pt; line-height: 115%; mso-bidi-font-family: 'Times New Roman';">2012</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal"><span lang="EN-US" style="font-size: 12.0pt; line-height: 115%; mso-bidi-font-family: 'Times New Roman';">Desain Simple Dan Modern Dalam Perancangan Website Penjualan</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal"><span lang="EN-US" style="font-size: 12.0pt; line-height: 115%; mso-bidi-font-family: 'Times New Roman';">Rina Dewi Indahsari,<br /> Endang Setyati</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal"><span lang="EN-US"><a href="http://jurnal.stmikasia.ac.id/index.php/jitika/article/view/85" target="_blank" rel="noopener noreferrer"><span style="font-size: 12.0pt; line-height: 115%; mso-bidi-font-family: 'Times New Roman';">http://jurnal.stmikasia.ac.id/index.php/jitika/article/view/85</span></a></span><u></u></p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p class="MsoNormal"><span lang="EN-US" style="font-size: 12.0pt; line-height: 115%; mso-bidi-font-family: 'Times New Roman';">2013</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal"><span lang="EN-US" style="font-size: 12.0pt; line-height: 115%; mso-bidi-font-family: 'Times New Roman';">Rancang Bangun Pengenalan Citra Bunga Berdasarkan Bentuk Tepi Bunga Menggunakan Metode Euclidian Distance</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal"><span lang="EN-US" style="font-size: 12.0pt; line-height: 115%; mso-bidi-font-family: 'Times New Roman';">Rina Dewi Indahsari,<br /> Yuliana Melita</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal"><span lang="EN-US"><a href="http://jurnal.stmikasia.ac.id/index.php/jitika/article/view/69" target="_blank" rel="noopener noreferrer"><span style="font-size: 12.0pt; line-height: 115%; mso-bidi-font-family: 'Times New Roman';">http://jurnal.stmikasia.ac.id/index.php/jitika/article/view/69</span></a></span><u></u></p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p class="MsoNormal"><span lang="EN-US" style="font-size: 12.0pt; line-height: 115%; mso-bidi-font-family: 'Times New Roman';">2014</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal"><span lang="EN-US" style="font-size: 12.0pt; line-height: 115%; mso-bidi-font-family: 'Times New Roman';">Penerapan Data Mining Untuk Analisa Pola Perilaku Nasabah Dalam Pengkreditan Menggunakan Metode C.45 Studi Kasus Pada KSU Insan Kamil Demak</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal"><span lang="EN-US" style="font-size: 12.0pt; line-height: 115%; mso-bidi-font-family: 'Times New Roman';">Rina Dewi Indahsari, <br /> Yuwono Sindunata</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal"><span lang="EN-US"><a href="http://jurnal.stmikasia.ac.id/index.php/jitika/article/view/175" target="_blank" rel="noopener noreferrer"><span style="font-size: 12.0pt; line-height: 115%; mso-bidi-font-family: 'Times New Roman';">http://jurnal.stmikasia.ac.id/index.php/jitika/article/view/175</span></a></span><u></u></p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p class="MsoNormal"><span lang="EN-US" style="font-size: 12.0pt; line-height: 115%; mso-bidi-font-family: 'Times New Roman';">2015</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal"><span lang="EN-US" style="font-size: 12.0pt; line-height: 115%; mso-bidi-font-family: 'Times New Roman';">Sistem Pendukung Keputusan Penempatan Kerja Bagi Calon Pencari Kerja Pada Dinas Tenaga Kerja Dan Transmigrasi Kota Mojokerto Menggunakan Metode Algoritma Genetika </span></p>
                        </td>
                        <td>
                            <p class="MsoNormal"><span lang="EN-US" style="font-size: 12.0pt; line-height: 115%; mso-bidi-font-family: 'Times New Roman';">Rina Dewi Indahsari,<br /> Aryo Prakosa</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal"><span lang="EN-US"><a href="http://jurnal.stmikasia.ac.id/index.php/jitika/article/view/103" target="_blank" rel="noopener noreferrer"><span style="font-size: 12.0pt; line-height: 115%; mso-bidi-font-family: 'Times New Roman';">http://jurnal.stmikasia.ac.id/index.php/jitika/article/view/103</span></a></span><u></u></p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p class="MsoNormal"><span lang="EN-US" style="font-size: 12.0pt; line-height: 115%; mso-bidi-font-family: 'Times New Roman';">2015</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal"><span lang="EN-US" style="font-size: 12.0pt; line-height: 115%; mso-bidi-font-family: 'Times New Roman';">Pengolahan Nilai Berbasis Database Di MTS Miftahul Ulum Wonokoyo</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal"><span lang="EN-US" style="font-size: 12.0pt; line-height: 115%; mso-bidi-font-family: 'Times New Roman'; color: black;">Setyorini,<br /> Suastika Yulia Riska,<br /> Fadhli Almu'iini Ahda,<br /> Rina Dewi Indah Sari</span></p>
                        </td>
                        <td>
                            <p class="MsoNormal"><span lang="EN-US" style="font-size: 12.0pt; line-height: 115%; mso-bidi-font-family: 'Times New Roman';"> </span></p>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <p></p>
                <p>PUBLIKASI INTERNATIONAL</p>
                <p></p>
                <table class="table-responsive">
                    <tbody>
                    <tr style="mso-yfti-irow: 0; mso-yfti-firstrow: yes; height: 15.75pt;">
                        <td style="width: 37.7pt; border: solid black 1.0pt; mso-border-alt: solid black .75pt; padding: 1.5pt 2.25pt 1.5pt 2.25pt; height: 15.75pt;" valign="top" width="50">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; text-align: center; line-height: normal;" align="center"><b><span lang="EN-US" style="font-size: 12.0pt; mso-bidi-font-family: Arial;">Tahun</span></b></p>
                        </td>
                        <td style="width: 9.0cm; border: solid black 1.0pt; border-left: none; mso-border-left-alt: solid #CCCCCC .75pt; mso-border-alt: solid black .75pt; padding: 1.5pt 2.25pt 1.5pt 2.25pt; height: 15.75pt;" valign="top" width="340">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; text-align: center; line-height: normal;" align="center"><b><span lang="EN-US" style="font-size: 12.0pt; mso-bidi-font-family: Arial;">Judul Artikel</span></b></p>
                        </td>
                        <td style="width: 179.65pt; border: solid black 1.0pt; border-left: none; mso-border-left-alt: solid #CCCCCC .75pt; mso-border-alt: solid black .75pt; padding: 1.5pt 2.25pt 1.5pt 2.25pt; height: 15.75pt;" valign="top" width="240">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; text-align: center; line-height: normal;" align="center"><b><span lang="EN-US" style="font-size: 12.0pt; mso-bidi-font-family: Arial;">Nama Penulis </span></b></p>
                        </td>
                        <td style="width: 174.75pt; border: solid black 1.0pt; border-left: none; mso-border-left-alt: solid #CCCCCC .75pt; mso-border-alt: solid black .75pt; padding: 1.5pt 2.25pt 1.5pt 2.25pt; height: 15.75pt;" valign="top" width="233">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; text-align: center; line-height: normal;" align="center"><b><span lang="EN-US" style="font-size: 12.0pt; mso-bidi-font-family: Arial;">URL / Link Artikel</span></b></p>
                        </td>
                    </tr>
                    <tr style="mso-yfti-irow: 1; height: 15.75pt;">
                        <td style="width: 37.7pt; border: solid black 1.0pt; border-top: none; mso-border-top-alt: solid black .75pt; mso-border-alt: solid black .75pt; padding: 1.5pt 2.25pt 1.5pt 2.25pt; height: 15.75pt;" valign="top" width="50">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; text-align: center; line-height: normal;" align="center"><b><span lang="EN-US" style="font-size: 12.0pt; mso-bidi-font-family: Arial;">2016</span></b></p>
                        </td>
                        <td style="width: 9.0cm; border-top: none; border-left: none; border-bottom: solid black 1.0pt; border-right: solid black 1.0pt; mso-border-top-alt: solid black .75pt; mso-border-left-alt: solid #CCCCCC .75pt; mso-border-alt: solid black .75pt; padding: 1.5pt 2.25pt 1.5pt 2.25pt; height: 15.75pt;" valign="top" width="340">
                            <p class="MsoNormal" style="line-height: 150%;"><span lang="EN-US" style="font-size: 12.0pt; line-height: 150%;">Rainfall Prediction In Tengger Region Indonesia Using Tsukamoto Fuzzy Inference System</span></p>
                        </td>
                        <td style="width: 179.65pt; border-top: none; border-left: none; border-bottom: solid black 1.0pt; border-right: solid black 1.0pt; mso-border-top-alt: solid black .75pt; mso-border-left-alt: solid #CCCCCC .75pt; mso-border-alt: solid black .75pt; padding: 1.5pt 2.25pt 1.5pt 2.25pt; height: 15.75pt;" valign="top" width="240">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12pt; color: #222222; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">Ida Wahyuni, </span></p>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12pt; color: #222222; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">Wayan Firdaus Mahmudy, </span></p>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12pt; color: #222222; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">Atiek Iriany</span></p>
                        </td>
                        <td style="width: 174.75pt; border-top: none; border-left: none; border-bottom: solid black 1.0pt; border-right: solid black 1.0pt; mso-border-top-alt: solid black .75pt; mso-border-left-alt: solid #CCCCCC .75pt; mso-border-alt: solid black .75pt; padding: 1.5pt 2.25pt 1.5pt 2.25pt; height: 15.75pt;" valign="top" width="233">
                            <p class="MsoNormal" style="line-height: 150%;"><span lang="EN-US" style="font-size: 12.0pt; line-height: 150%;">https://www.researchgate.net/profile/Ida_Wahyuni/publication/308693809_Rainfall_Prediction_in_Tengger_Region_Indonesia_using_Tsukamoto_Fuzzy_Inference_System/links/57eb6c5908aeafc4e88a631c.pdf</span></p>
                        </td>
                    </tr>
                    <tr style="mso-yfti-irow: 2; height: 15.75pt;">
                        <td style="width: 37.7pt; border: solid black 1.0pt; border-top: none; mso-border-top-alt: solid black .75pt; mso-border-alt: solid black .75pt; padding: 1.5pt 2.25pt 1.5pt 2.25pt; height: 15.75pt;" valign="top" width="50">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; text-align: center; line-height: normal;" align="center"><b><span lang="EN-US" style="font-size: 12.0pt; mso-bidi-font-family: Arial;"> </span></b></p>
                        </td>
                        <td style="width: 9.0cm; border-top: none; border-left: none; border-bottom: solid black 1.0pt; border-right: solid black 1.0pt; mso-border-top-alt: solid black .75pt; mso-border-left-alt: solid #CCCCCC .75pt; mso-border-alt: solid black .75pt; padding: 1.5pt 2.25pt 1.5pt 2.25pt; height: 15.75pt;" valign="top" width="340">
                            <p class="MsoNormal" style="line-height: 150%;"><span lang="EN-US" style="font-size: 12.0pt; line-height: 150%;">Error Numerical Analysis For Result Of Rainfall Prediction Between Tsukamoto Fis And Hybrid Tsukamoto Fis With GA</span></p>
                        </td>
                        <td style="width: 179.65pt; border-top: none; border-left: none; border-bottom: solid black 1.0pt; border-right: solid black 1.0pt; mso-border-top-alt: solid black .75pt; mso-border-left-alt: solid #CCCCCC .75pt; mso-border-alt: solid black .75pt; padding: 1.5pt 2.25pt 1.5pt 2.25pt; height: 15.75pt;" valign="top" width="240">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12pt; color: #222222; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">Ida Wahyuni, </span></p>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12pt; color: #222222; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">Fitri Utaminingrum</span></p>
                        </td>
                        <td style="width: 174.75pt; border-top: none; border-left: none; border-bottom: solid black 1.0pt; border-right: solid black 1.0pt; mso-border-top-alt: solid black .75pt; mso-border-left-alt: solid #CCCCCC .75pt; mso-border-alt: solid black .75pt; padding: 1.5pt 2.25pt 1.5pt 2.25pt; height: 15.75pt;" valign="top" width="233">
                            <p class="MsoNormal" style="line-height: 150%;"><span lang="EN-US" style="font-size: 12.0pt; line-height: 150%;">https://www.researchgate.net/profile/Ida_Wahyuni/publication/314666323_Error_numerical_analysis_for_result_of_rainfall_prediction_between_Tsukamoto_FIS_and_hybrid_Tsukamoto_FIS_with_GA/links/590eae290f7e9b2863a485e4/Error-numerical-analysis-for-result-of-rainfall-prediction-between-Tsukamoto-FIS-and-hybrid-Tsukamoto-FIS-with-GA.pdf</span></p>
                        </td>
                    </tr>
                    <tr style="mso-yfti-irow: 3; height: 15.75pt;">
                        <td style="width: 37.7pt; border: solid black 1.0pt; border-top: none; mso-border-top-alt: solid black .75pt; mso-border-alt: solid black .75pt; padding: 1.5pt 2.25pt 1.5pt 2.25pt; height: 15.75pt;" valign="top" width="50">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; text-align: center; line-height: normal;" align="center"><b><span lang="EN-US" style="font-size: 12.0pt; mso-bidi-font-family: Arial;"> </span></b></p>
                        </td>
                        <td style="width: 9.0cm; border-top: none; border-left: none; border-bottom: solid black 1.0pt; border-right: solid black 1.0pt; mso-border-top-alt: solid black .75pt; mso-border-left-alt: solid #CCCCCC .75pt; mso-border-alt: solid black .75pt; padding: 1.5pt 2.25pt 1.5pt 2.25pt; height: 15.75pt;" valign="top" width="340">
                            <p class="MsoNormal" style="line-height: 150%;"><span lang="EN-US" style="font-size: 12.0pt; line-height: 150%;">Rainfall Prediction using Hybrid Adaptive Neuro Fuzzy Inference System (ANFIS) and Genetic Algorithm</span></p>
                        </td>
                        <td style="width: 179.65pt; border-top: none; border-left: none; border-bottom: solid black 1.0pt; border-right: solid black 1.0pt; mso-border-top-alt: solid black .75pt; mso-border-left-alt: solid #CCCCCC .75pt; mso-border-alt: solid black .75pt; padding: 1.5pt 2.25pt 1.5pt 2.25pt; height: 15.75pt;" valign="top" width="240">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12pt; color: #222222; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">Ida Wahyuni, </span></p>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12pt; color: #222222; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">Wayan Firdaus Mahmudy, </span></p>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span lang="EN-US" style="font-size: 12pt; color: #222222; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">Atiek Iriany</span></p>
                        </td>
                        <td style="width: 174.75pt; border-top: none; border-left: none; border-bottom: solid black 1.0pt; border-right: solid black 1.0pt; mso-border-top-alt: solid black .75pt; mso-border-left-alt: solid #CCCCCC .75pt; mso-border-alt: solid black .75pt; padding: 1.5pt 2.25pt 1.5pt 2.25pt; height: 15.75pt;" valign="top" width="233">
                            <p class="MsoNormal" style="line-height: 150%;"><span lang="EN-US" style="font-size: 12.0pt; line-height: 150%;">http://journal.utem.edu.my/index.php/jtec/article/viewFile/2627/1699</span></p>
                        </td>
                    </tr>
                    <tr style="mso-yfti-irow: 4; mso-yfti-lastrow: yes; height: 15.75pt;">
                        <td style="width: 37.7pt; border: solid black 1.0pt; border-top: none; mso-border-top-alt: solid black .75pt; mso-border-alt: solid black .75pt; padding: 1.5pt 2.25pt 1.5pt 2.25pt; height: 15.75pt;" valign="top" width="50">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; text-align: center; line-height: normal;" align="center"><b><span lang="EN-US" style="font-size: 12.0pt; mso-bidi-font-family: Arial;"> </span></b></p>
                        </td>
                        <td style="width: 9.0cm; border-top: none; border-left: none; border-bottom: solid black 1.0pt; border-right: solid black 1.0pt; mso-border-top-alt: solid black .75pt; mso-border-left-alt: solid #CCCCCC .75pt; mso-border-alt: solid black .75pt; padding: 1.5pt 2.25pt 1.5pt 2.25pt; height: 15.75pt;" valign="top" width="340">
                            <p class="MsoNormal" style="line-height: 150%;"><span lang="EN-US" style="font-size: 12.0pt; line-height: 150%;">Rainfall Prediction in Tengger, Indonesia Using Hybrid Tsukamoto FIS and Genetic Algorithm Method</span></p>
                        </td>
                        <td style="width: 179.65pt; border-top: none; border-left: none; border-bottom: solid black 1.0pt; border-right: solid black 1.0pt; mso-border-top-alt: solid black .75pt; mso-border-left-alt: solid #CCCCCC .75pt; mso-border-alt: solid black .75pt; padding: 1.5pt 2.25pt 1.5pt 2.25pt; height: 15.75pt;" valign="top" width="240">
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 150%;"><span lang="EN-US" style="font-size: 12.0pt; line-height: 150%;">Ida Wahyuni, </span></p>
                            <p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 150%;"><span lang="EN-US" style="font-size: 12.0pt; line-height: 150%;">Wayan Firdaus Mahmudy</span></p>
                        </td>
                        <td style="width: 174.75pt; border-top: none; border-left: none; border-bottom: solid black 1.0pt; border-right: solid black 1.0pt; mso-border-top-alt: solid black .75pt; mso-border-left-alt: solid #CCCCCC .75pt; mso-border-alt: solid black .75pt; padding: 1.5pt 2.25pt 1.5pt 2.25pt; height: 15.75pt;" valign="top" width="233">
                            <p class="MsoNormal" style="line-height: 150%;"><span lang="EN-US" style="font-size: 12.0pt; line-height: 150%;">http://journals.itb.ac.id/index.php/jictra/article/viewFile/2577/2509</span></p>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <p></p>


                    </tbody>
                </table>


            </div>
        </div>
    </div>
@endsection