@extends('front.layout.app')
@section('title', 'ALUMNI STMIK ASIA')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('css/alumni/custom.css') }}">
@endsection
@section('content')
    <div class="container min-700">
        <div class="row">
            <div class="col-md-12 well well-white mt-50">
                @include('front.alumni.menu')
                <div class="text-center">
                    <h1 class="title-content weight-7">ALUMNI STMIK ASIA</h1>
                </div>
                <h3><strong>PROFIL UMUM</strong></h3>

                <b><p>PROFIL HIMPUNAN ALUMNI STMIK ASIA MALANG</p></b>
                <p>Lulusan  STMIK Asia Malang memiliki himpunan alumni yang memiliki  aktivitas dan hasil  kegiatan dari himpunan alumni untuk kemajuan  jurusan  dalam kegiatan akademik dan non akademik, meliputi sumbangan dana, sumbangan fasilitas akademik, sumbangan fasilitas non-akademik, keterlibatan dalam kegiatan, pengembangan jejaring, dan penyediaan fasilitas. Beberapa program kerja yang dilaksanakan oleh Himpunan Alumni STMIK Asia Malang antara lain adalah</p>
                <ol>
                    <li>
                        Sumbangan dana<br/>
                        Salah satu  bentuk  partisipasi alumni dalam  mendukung  jurusan,  yaitu  dengan memberikan  sumbangan  dana. Pemberian  sumbangan  ini  dilakukan  secara insidental, misalnya  pada  saat acara-acara  seminar nasional  atau acara dies natalis Organisasi/UKM di STMIK Asia Malang.
                    </li>
                    <li>
                        Sumbangan Fasilitas Akademik<br/>
                        Salah satu bentuk partisipasi alumni untuk mendukung jurusan Pemberian sumbangan fasilitas akademik oleh alumni seperti  komputer maupun  bahan-bahan referensi (buku).
                    </li>
                    <li>
                        Keterlibatan dalam kegiatan<br/>
                        Adanya keterlibatan alumni dengan pihak kampus dalam kegiatan kuliah tamu, seminar, pendampingan, dan kegiatan organisasi atau UKM yang melibatkan alumni.
                    </li>
                    <li>
                        Pengembangan Jaringan<br/>
                        Pengembangan  jejaring  ini  diwujudkan  dengan  terbentuknya  paguyuban  atau  Himpunan Alumni STMIK Asia Malang.  Jejaringnya meliputi pembuatan Group Alumni di social media, antara lain:<br/>
                        a.	Page Facebook: Alumni STMIK ASIA Malang (https://www.facebook.com/Alumni-STMIK-ASIA-Malang-1339387439404635/)<br/>
                        b.	Group Facebook: Alumni STMIK-STIE ASIA MALANG (https://www.facebook.com/groups/688559904546291/)<br/>
                        c.	Group Facebook: Info Center STMIK-STIE ASIA Malang (https://www.facebook.com/groups/1448322948790703/)<br/>
                        Selain itu diadakan pula acara yang dilaksanakan untuk temu alumni antara lain Reuni Akbar (Tahunan) yang selalu mengundang pejabat struktural fakultas dan perguruan tinggi. Selanjutnya pemanfaatan informasi dari alumni mengenai ketersediaan pekerjaan dilakukan  jurusan  secara berkala dengan memanfaatkan social media dengan selalu melakukan  updating  data alumni sehingga kontak dengan alumni selalu terjaga dengan baik. Sehingga bila instansi di mana alumni bekerja membuka lowongan pekerjaan, alumni tersebut akan memberikan informasi kepada jurusan.
                    </li>
                    <li>
                        Penyediaan fasilitas<br/>
                        Fasilitas pembelajaran diberikan kepada mahasiswa STMIK Asia Malang untuk tempat KKN-P bagi mahasiswa Juruasan Ilmu Ekonomi dan kerjasama dengan perushaan tempat alumni bekerja untuk mengadakan MOU dalam berbagai kegaitan.
                    </li>
                </ol>

                <p> <b>A. VISI</b><br/>

                    “Mewujudkan Ikatan Himpunan Alumni STMIK Asia Malang sebagai lembaga yang dapat mewadahi aspirasi para alumni STMIK Asia Malang serta dapat memberikan kontribusi terhadap perkembangan kualitas akademis dan non-akademis sebagai mitra kampus”<br/>
                    <b>B. MISI</b><br/>
                <ol>
                    <li>
                        Mempersatukan, mempererat, dan mewujudkan persaudaraan antar alumni STMIK Asia Malang(Mempersatukan, mempererat, dan mewujudkan persaudaraan antar alumni STMIK Asia Malang Mempersatukan, mempererat, dan mewujudkan persaudaraan antar alumni STMIK Asia Malang Mempersatukan, mempererat, dan mewujudkan persaudaraan antar alumni STMIK Asia Malang ).
                    </li>
                    <li>
                        Sebagai wadah aspirasi para alumni dalam berbagai hal untuk kemajuan Himpunan Alumni STMIK Asia Malang dan Kampus STMIK Asia Malang.<br/>
                    </li>
                    <li>
                        Mengkonsolidasikan dan mensinergikan aspirasi para alumni dengan pihak kampus.<br/>
                    </li>
                    <li>
                        Turut berperan aktif  bersama kampus dalam rangka peningkatan kualitas pendidikan dikampus baik di bidang akademis dan non-akademis.<br/>
                    </li>
                    <li>
                        Bekerja sama dengan pihak sekolah dalam menginformasikan peluang kerja untuk mahasiswa STMIK Asia Malang.<br/>
                    </li>
                    <li>
                        Berperan aktif dalam mengembangkan kegiatan-kegiatan dan organisasi-organisasi yang ada di STMIK Asia Malang.
                    </li>
                </ol>


                </p>



            </div>
        </div>
    </div>
@endsection