@extends('front.layout.app')
@section('title', 'LPMI STMIK ASIA')
@section('content')
    <div class="container min-700">
        <div class="row">
            <div class="col-md-12 well well-white mt-50">
                <div class="text-center">
                    <h1 class="title-content weight-7">LPMI STMIK ASIA</h1>
                </div>
                <h3><strong>PROFIL UMUM</strong></h3>
                <p>Penjaminan mutu menjadi sarana menciptakan suasana atau iklim dan&nbsp; perangkat jaminan mutu yang senantiasa mengembangkan semangat unggul menuju peningkatan mutu penyelenggaraan pendidikan sesuai dengan visi dan misi. Adapun tugas pokok LPMA STMIK ASIA Malang adalah:</p>
                <ol>
                    <li>Mengembangkan perangkat dan panduan penjaminan mutu kegiatan pengajaran, penelitian, dan pengandian pada masyarakat serta kegiatan non akademik yang bersifat umum.</li>
                    <li>Mengkoordinasi pelaksanaan kegiatan penjaminan mutu kegiatan akademik dan non akademik.</li>
                    <li>Memonitor dan mengevaluasi hasil pelaksanaan penjaminan mutu yang dilaksanakan oleh unit-unit kerja</li>
                </ol>
                <p><strong>FUNGSI Lembaga Penjaminan Mutu Akademik STMIK ASIA MALANG</strong></p>
                <ol>
                    <li>Menyusun dan menggembangkan berbagai standar dan pedoman sebagai acuan melaksanakan penjaminan mutu pendidikan, penelitian dan pengabdian kepada masyarakat&nbsp; serta administrasi akademik.</li>
                    <li>Mengembangkan standar mutu dalam bidang pendidikan, penelitian, dan pengabdian kepada masyarakat merujuk standar internasional.</li>
                    <li>Mengkoordinasikan pelaksanaan kegiatan penjaminan mutu pendidikan, penelitian dan pengabdian kepada masyarakat, serta administrasi akademik mulai tingkat perguruan tinggi sampai program studi.</li>
                    <li>Mengkaji dan melaporakan hasil pelaksanaan penjaminan mutu dalam suatu siklus pada seluruh unit dan jajarannya di STMIK ASIA MALANG.</li>
                    <li>Menyampaikan rekomendasi kepada Ketua STMIK lainnya sebagai masukan untuk peningkatan mutu secara berkelanjutan</li>
                </ol>
                <hr />
                <h3><strong>VISI &amp; MISI LPMI</strong></h3>
                <p><strong>Visi Lembaga Penjaminan Mutu Akademik STMIK ASIA MALANG</strong></p>
                <p>Menjadi salah satu institusi penjaminan mutu perguruan tinggi terbaik di tingkat nasional untuk menjadikan STMIK ASIA memiliki keunggulan di bidang pendidikan, penelitian, dan pengabdian kepada masyarakat serta layanan administrasi akademik.&nbsp;</p>
                <p><strong>Misi Lembaga Penjaminan Mutu Akademik STMIK ASIA MALANG</strong></p>
                <ol>
                    <li>Menjadikan mutu sebagai jiwa seluruh civitas akademika STMIK ASIA MALANG dalam menjalankan fungsi terbaiknya di bidang pendidikan, penelitian, dan pengabdian pada masyarakat serta layanan administrasi akademik.</li>
                    <li>Mendorong STMIK ASIA MALANG menjadi perguruan tinggi yang bermutu dan berskala internasional.</li>
                </ol>
            </div>
        </div>
    </div>
@endsection