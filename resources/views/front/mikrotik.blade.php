@extends('front.layout.app')
@section('title', 'MIKROTIK ACADEMY STMIK ASIA')
@section('content')
      <div class="container min-700">
        <div class="row">
            <div class="col-md-12 well well-white mt-50">
                 <strong><a class="" href="http://mikrotik.com/" target="_blank" ><img src="{{ URL::to('/assets/front/mikrotik.png') }}" align="left" title="MikroTik" class="img-responsive img-mikrotik"></a></strong>
                 <strong><a class="" href="http://belajarmikrotik.com/" target="_blank" ><img src="{{ URL::to('/assets/front/belajarmikrotik.png') }}" align="right" title="BelajarMikrotik" class="img-responsive img-belajarmikrotik"></a></strong>
                <br><br><br><br>
                <div class="text-center">
                    <h1 class="title-content weight-7">MikroTik Academy</h1>
                </div>
                <hr>
                <br>
                <p>Program MikroTik Akademi adalah program khusus untuk Institusi Pendidikan, seperti Universitas, STMIK, AMIK, Institut Teknik, SMK dan sekolah berbasis semester. Program ini memungkinkan sekolah untuk membuka kelas MikroTik Bersertifikasi sebagai bagian dari kurikulum pembelajaran dan membuka ujian sertifikasi bagi siswanya di akhir kelas.</p>
                <p>Sebagai bagian dari program, STMIK ASIA Malang merupakan salah satu dari Training Partner Akademi untuk MikroTik yang ditunjuk untuk melaksanakan program kelas MikroTik Akademi bagi mahasiswa kami. Program ini digabungkan ke dalam kurikulum pada semester 4 dan 5 melalui mata kuliah Administrasi Jaringan dan sebagai bagian dari praktek laboratorium untuk mata kuliah Jaringan Komputer.  Pada akhir masa pembelajaran, akan diadakan ujian sertifikasi untuk menguji kemampuan mahasiswa serta memberikan kesempatan kepada mahasiswa untuk memperoleh Sertifikat MikroTik yang bersifat internasional. </p>
                <p>Sertifikat yang dikeluarkan merupakan sertifikat bertaraf Internasional (bila mahasiswa lulus ujian dengan nilai > 60%) dan mahasiswa akan mendapatkan gelar sebagai MikroTik Certified Network Associate (MTCNA).</p>
                <hr />
                <h3><strong>STMIK ASIA Malang mengajarkan topik berikut dalam kelas MikroTik Akademi</strong></h3>
                <ol>
                    <li>Pengenalan MikroTik dan konfigurasi dasar</li>
                    <li>Manajemen jaringan layer 2 dan layer 3 (Bridging dan Routing)</li>
                    <li>Pengaturan trafik (konsep Firewall dasar)</li>
                    <li>Dasar jaringan nirkabel (Basic Wireless)</li>
                    <li>Teknologi tunneling dan VPN</li>
                    <li>Quality of Service (QoS)</li>
                    <li>User Management</li>
                </ol>
                <p>Diselenggarakan selama 2 semester (1 tahun)</p>
                <hr />
                <p>MikroTik Academy di STMIK ASIA Malang didukung sepenuhnya oleh <a href="http://www.belajarmikrotik.co.id/" target="_blank" rel="nofollow" title="BelajarMikrotik">BelajarMikroTik</a> sebagai Koordinator MikroTik Akademi di Indonesia. </p>
                <br>
                <p>Untuk informasi lebih lanjut mengenai Kelas MikroTik Akademi di STMIK ASIA Malang hubungi </p>
                
                <p><b>Fransiska Sisilia Mukti, S.T., M.T.<br>+6281216176910<br>Email : mikrotik@asia.ac.id</b>
                 <strong><a class="" href=""  ><img src="{{ URL::to('/assets/front/mikrotikacademy.png') }}" align="right" title="MikroTik Academy" class="img-responsive img-mikrotikacademy"></a></strong>
                </p>
            </div>
        </div>
    </div>
@endsection