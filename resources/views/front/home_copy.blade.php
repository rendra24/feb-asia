@extends('front.layout.home')
@push('style')
<link href="{{ URL::to('/assets/front/fullcalendar/fullcalendar.min.css') }}" rel='stylesheet' />
<link href="{{ URL::to('/assets/front/fullcalendar/fullcalendar.print.min.css') }}" rel='stylesheet' media='print' />
@endpush
@section('content')
<!-- Header -->
<header>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <div class="container">
        <div class="intro-text">
            <div class="intro-lead-in animated bounceInDown" style="font-family:&#39;Roboto&#39;;font-weight:100;">
            </div>
            <div class="intro-heading animated bounceInDown">Fakultas Ekonomi dan Bisnis</div>
            <div class="intro-heading animated bounceInDown">
                Institut Asia Malang
            </div>

            <div class="animated fadeInRight profil-stmik">{!! $prodi[0]->Profil->content !!}</div>
            <a href="{{ route('visi-misi') }}" class="page-scroll btn btn-xl animated fadeInLeft"
                style="margin-top:10px;">Lihat Selengkapnya</a>
            <div class="row" style="margin-top: 30px;">
                @foreach($prodi as $data)
                @if($data->name!='stmik')
                <div class="col-md-6 col-sm-12">
                    <a href="{{ route('prodi',$data->name) }}">
                        <div class="well well-prodi">
                            <h4>{{ $data->description }}</h4>
                            {!! $data->Profil->body !!}
                        </div>
                    </a>
                </div>
                @endif
                @endforeach
            </div>

        </div>
    </div>
</header>
<div style="background-color: #FFF;">
    <div class="container" style="padding-top: 50px;padding-bottom:  50px;">
        <div class="row">
            <div class="col-md-8 ">
                <h2 class="title-header">Pojok Berita</h2>
                <div class="row">
                    <div class="col-md-6">
                        <img src="{{ URL::to($berita[0]->image) }}" alt="" class="img-100">
                        <h3 class="title-berita" style="margin-bottom: 0px;"><a
                                href="{{ route('berita-detail', ['id' => $berita[0]->id, 'slug' => $berita[0]->slug]) }}">{{ $berita[0]->title }}</a>
                        </h3>
                        <p style="margin-top: 5px;">{{ $berita[0]->user->name }} | {{ $berita[0]->date }}</p>
                    </div>
                    <div class="col-md-6">
                        @foreach($berita as $data)
                        @if(! $loop->first)
                        <div class="media">
                            <div class="media-left media-middle">
                                <img src="{{ URL::to($data->image) }}" class="media-object" style="width:90px">
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading font-11pt"><a
                                        href="{{ route('berita-detail', ['id' => $data->id, 'slug' => $data->slug]) }}">{{ $data->title }}</a>
                                </h4>
                                <p>{{ $data->user->name }} | {{ $data->date }}</p>
                            </div>
                        </div>
                        @endif
                        @endforeach

                        <div class="text-right">
                            <a href="{{ route('berita') }}">Lihat Selengkapnya&nbsp;<i
                                    class="fa fa-angle-double-right"></i></a>
                        </div>
                    </div>
                </div>


            </div>
            <div class="col-md-4">

            </div>
            <div class="col-md-4">
                <h2 class="mb-0 title-header">Pengumuman</h2>
                <div class="list-group menu-home">
                    @if($pengumuman)
                    @foreach($pengumuman as $data)
                    <a href="{{ route('pengumuman-detail', ['id' => $data->id, 'slug' => $data->slug]) }}"
                        class="list-group-item">
                        <h6 style="font-size: 14px;color: #252688;margin-bottom: 0px;">{{ $data->user->name }} |
                            {{ $data->created }}</h6>
                        {{ $data->title }}
                    </a>
                    @endforeach
                    @else
                    <span style="color: rgba(51, 51, 51, 0.83); padding-top: 30px;">Pengumuman Tidak Ada...</span>
                    @endif
                </div>
                <div class="text-right">
                    <a href="{{ route('pengumuman') }}">Lihat Selengkapnya&nbsp;<i
                            class="fa fa-angle-double-right"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row" style="margin-top: 30px;margin-bottom: 50px;">
        <div class="col-md-12 well well-white" style="">
            <div class="col-md-7">
                <div id='calendar'></div>
            </div>
            <div class="col-md-5" style="padding-top: 30px;">
                <iframe
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4699.2591422806045!2d112.62233302710163!3d-7.937799108214037!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7882780c5b14db%3A0xa2178e9d5a420380!2sSTMIK+-+STIE+Asia+Malang!5e0!3m2!1sen!2sid!4v1495108737268"
                    width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</div>
@endsection
@include('front.kalender.calendar')