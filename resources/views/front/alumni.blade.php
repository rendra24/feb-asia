@extends('front.layout.app')
@section('title', 'ALUMNI STMIK ASIA')
@section('content')
    <div class="container min-700">
        <div class="row">
            <div class="col-md-12 well well-white mt-50">
                <div class="text-center">
                    <h1 class="title-content weight-7">ALUMNI STMIK ASIA</h1>
                </div>
                <h3><strong>PROFIL UMUM</strong></h3>
                <p>ALUMNI ASIA merupakan wadah informasi dan komunikasi antara STIE ASIA Malang dengan Alumni-alumni yang telah tersebar di seluruh Indonesia. Meski telah menjadi sarjana dan bekerja tersebar di seantero nusantara, Alumni STIE ASIA Malang memiliki ikatan kekeluargaan yang sangat kuat. Pertemuan alumni selain untuk memelihara tali silaturahmi, memantau keberadaan alumni juga sebagai wadah informasi mengenai perkembangan existensi alumni. Apabila alumni menginginkan sesuatu, sebagai misal ingin komunikasi dengan teman lama saat kuliah, maka tinggal menghubungi sekretariat alumni di kampus pusat, demikian juga komunitas alumni dalam jejaring sosial dan lainnya. Kuatnya tali silaturahmi alumni bermanfaat terhadap luasnya jaringan alumni sebagai pengejawantahan Tri Dharma Perguruan tinggi.</p>
            </div>
        </div>
    </div>
@endsection