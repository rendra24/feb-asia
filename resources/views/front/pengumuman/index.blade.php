@extends('front.layout.app')
@section('title', 'PENGUMUMAN')
@push('style')
	<style type="text/css">
		.box-pengumuman{
			padding: 15px;
			min-height: 500px;
		}
		.box-pengumuman .item{
			font-size: 13.5pt;
			border-bottom: 1px solid #d7d7d7 !important;
			padding-top: 7px;
			padding-bottom: 8px;
			color: #333346 !important
		}
		.box-pengumuman .item a{
			color: #333346 !important;
			cursor: pointer;
		}
		.box-pengumuman .item a:hover, .box-pengumuman .item a:focus{
			color: #EF5F5A !important;
		}
		.box-pengumuman .item h6{
			font-size: 14px;
			color: #EF5F5A;
			margin-top: 0px;
			margin-bottom: 0px;
		}

		.box-pengumuman .item .content {
			font-size: 14px;
			color: #555;
			margin-top: 0px;
			margin-bottom: 0px;
		}
	</style>
@endpush
@section('content')
<div class="container min-700">
	<div class="row">
		<div class="col-md-10 col-md-offset-1 well well-white mt-50">
			<div class="text-center">
				<h1 class="title-content weight-6">PENGUMUMAN</h1>
			</div>
			<div class="box-pengumuman">
				@foreach($pengumuman as $data)
					<div class="item">
						<h6 style="">{{ $data->user->name }} | {{ $data->created }}</h6>
						<a href="{{ route('pengumuman-detail', [ 'id' => $data->id, 'slug'=> $data->slug ]) }}">{{ $data->title }}</a>
						<div class="content">
							{!! $data->isi !!}
						</div>
					</div>
				@endforeach
				<div class="text-center">
					{{ $pengumuman->links() }}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection