@extends('front.layout.app')
@section('title', 'PENGUMUMAN')
@push('style')
	<style type="text/css">
		.box-pengumuman-detail{
			padding: 15px;
			min-height: 500px;
		}
		.box-pengumuman-detail .item{
			font-size: 13.5pt;
			border-bottom: 1px solid #d7d7d7 !important;
			padding-top: 7px;
			padding-bottom: 8px;
			color: #333346 !important
		}
		.box-pengumuman-detail a{
			color: #333346 !important;
			cursor: pointer;
		}
		.box-pengumuman-detail h6{
			font-size: 14px;
			color: #EF5F5A;
			margin-top: 0px;
			margin-bottom: 0px;
		}

		.box-pengumuman-detail .content {
			font-size: 14px;
			color: #555;
			margin-top: 0px;
			margin-bottom: 0px;
		}
	</style>
@endpush
@section('content')
<div class="container min-700">
	<div class="row">
		<div class="col-md-10 col-md-offset-1 well well-white mt-50 box-pengumuman-detail">
				<h6 style="">{{ $pengumuman->user->name }} | {{ $pengumuman->created }}</h6>
				<h2>{{ $pengumuman->title }}</h2>
				<div class="content">
					{!! $pengumuman->content !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection