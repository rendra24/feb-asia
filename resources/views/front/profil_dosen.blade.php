@extends('front.layout.app')
@section('title', 'Profil Dosen FEB ASIA MALANG')
@push('style')
	<style type="text/css" >
		.box-profil img{
			height: 300px;
			/*width: 230px;*/
			margin-left: auto;
			margin-right: auto;
		}
		.box-profil{
			margin-bottom: 20px;
		}
		.box-profil h4{
			font-size: 12px;
			font-weight: 600;
		}
	</style>
@endpush

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12 well well-white mt-50 min-700">
			<h2>PROFIL DOSEN FEB ASIA</h2>
			@foreach($profil as $data)
				@if($loop->first)
				<div class="row">
				@endif
					<div class="col-md-3 box-profil">
						<img src="{{ URL::to($data->foto) }}" alt="" class="img-responsive">
						<h4 class="text-center">{{ $data->nama }}</h4>
					</div>
				@if($loop->last)
					</div>
				@else
					@if($loop->iteration%4==0)
					</div>
					<div class="row">
					@endif
				@endif
			@endforeach
			<div class="text-center">
				{{ $profil->links() }}
			</div>
		</div>
	</div>
</div>
@endsection