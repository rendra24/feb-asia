@extends('front.layout.app')
@section('title', 'JADWAL UJIAN '.$ujian->jenis->description.' SEMESTER '.$ujian->semester.' TA. '.$ujian->tahun_ajaran)
@section('content')
<div class="container ">
	<div class="row">
		<div class="col-md-12 well well-white mt-50 min-700">
			<div class="row">
				<div class="col-sm-9">
					<h1 class="title-content weight-7">JADWAL UJIAN <span class="uppercase">{{ $ujian->jenis->description }}</span> SEMESTER <span class="uppercase">{{ $ujian->semester }}</span> TA {{ $ujian->tahun_ajaran }}</h1>
				</div>
				<div class="col-sm-3">
					<form action="{{ route('ujian-search', $ujian->jenis->name) }}" method="get">
						<div class="input-group">
						      <input type="text" class="form-control" placeholder="Pencarian..." name="q" value="{{ Request::has('q')?Request::input('q'):'' }}">
						      <span class="input-group-btn">
						        <button class="btn btn-default" type="submit">
						        		<i class="fa fa-search"></i>
						        </button>
						      </span>
						</div>
					</form>
				</div>
			</div>
			@if(count($details)>0)
				<div class="table-responsive mt-30">
					<table class="table table-bordered table-striped">
						<thead >
							<tr class="default">
								<th class="text-center">NO</th>
								<th class="text-center">JUDUL</th>
								<th class="text-center">HARI</th>
								<th class="text-center">TANGGAL</th>
								<th class="text-center">JAM</th>
								<th class="text-center">RUANG</th>
								<th class="text-center">NIM</th>
								<th class="text-center">NAMA MAHASISWA</th>
							</tr>
						</thead>
						<tbody>
							@foreach($details as $key => $data)
							<tr>
								<td class="text-center">{{ ($key+1) }}</td>
								<td>{{ $data->judul }}</td>
								<td class="text-center">{{ $data->hari }}</td>
								<td class="text-center">{{ $data->tanggal }}</td>
								<td class="text-center">{{ $data->jam }}</td>
								<td class="text-center">{{ $data->ruang }}</td>
								<td class="text-center">{{ $data->nim }}</td>
								<td>{{ $data->nama_mahasiswa }}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			@else 
				<div class="row">
					<div class="col-sm-8 col-sm-offset-2">
						<div class="well text-center" style="margin-top: 75px;">
							<h3 style="color: #EF5F5A;font-size: 21px;">Oops... hasil pencarian Anda tidak dapat ditemukan.</h3>
							<h6>Silakan melakukan pencarian kembali dengan menggunakan kata kunci lain.</h6>
						</div>
					</div>
				</div>
						
			@endif
		</div>
	</div>
</div>
@endsection